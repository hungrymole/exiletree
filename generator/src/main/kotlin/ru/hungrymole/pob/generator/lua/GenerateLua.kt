package ru.hungrymole.pob.generator.lua
import com.squareup.wire.Message
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaValue
import org.luaj.vm2.LuajOptimizations
import org.luaj.vm2.lib.ResourceFinder
import org.luaj.vm2.lib.jse.JsePlatform
import ru.hungrymole.pob.Constants
import ru.hungrymole.pob.generator.Generated
import ru.hungrymole.pob.generator.GeneratedLoaders
import ru.hungrymole.pob.generator.loadPobEnviroment
import ru.hungrymole.pob.lua.call
import ru.hungrymole.pob.lua.luaValue
import ru.hungrymole.pob.lua.pobColors
import java.io.File
import java.io.FileInputStream

/**
 * Created by Dinar Khusainov on 09.07.2017.
 */

val generatorOutputDir = File(File("").absoluteFile.parentFile, "assets/${Generated.OUTPUT}").also { it.mkdirs() }

fun <M : Message<*,*>> Generated<M>.saveGenerated(what: M) {
    write(generatorOutputDir, what)
}

val lua: Globals by lazy {
    JsePlatform.standardGlobals().apply {
        finder = ResourceFinder(::FileInputStream)
        loadPobEnviroment(generator = true)
    }
}
val version = Constants.VERSION
val colors by lazy(lua::pobColors)

fun main(args: Array<String>) {
    Loadables(
        generateBases(),
        generateMinions(),
        generateSkills(),
        generateModCache()
    ).let(GeneratedLoaders::saveGenerated)

    val tree = generateTree(lua)
    lua["main"]["tree"][version] = tree

    lua["initBuild"].call("_".luaValue(), LuaValue.NIL)
    generateDb()
}