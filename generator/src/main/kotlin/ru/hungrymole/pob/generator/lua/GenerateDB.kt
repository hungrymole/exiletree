package ru.hungrymole.pob.generator.lua

import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.generator.db.DB
import ru.hungrymole.pob.lua.*
import java.io.File
import java.sql.DriverManager

/**
 * Created by Dinar Khusainov on 15.01.2018.
 */
private val db by lazy {
    val dbFile = File(generatorOutputDir, DB.filename)
    dbFile.delete()
    DriverManager.getConnection("jdbc:sqlite:${dbFile.absolutePath}")
}

private fun createTables() {
    DB.tables().forEach {
        db.prepareStatement(it.createStatement()).execute()
    }
}

fun generateDb() {
    lua.pobColors()

    createTables()

    val itemsTab = lua.getBy("build.itemsTab")
    val data = lua.getBy("data.$version")
    val uniques = lua["main"]["uniqueDB"][version]["list"]
        .entries()

    //item types
    val extraTypeOneHanded = "One Handed Melee"
    val extraTypeTwoHanded = "Two Handed Melee"
    val types = uniques
        .map { it.value["type"].checkjstring() }
        .distinct()
        .sorted()
        .toMutableList()
        .apply {
            add(0, extraTypeTwoHanded)
            add(0, extraTypeOneHanded)
            add(0, "Any type")
            forEachIndexed { id, label ->
                DB.ItemTypes.insert(id, label)
            }
        }

    //unique types
    val leagueAnyLeague = "Any league"
    val leagues = uniques
        .map { it.value["league"].optjstring(null) }
        .filterNotNull()
        .flatMap {
            it.split(",").map { it.trim() }
        }
        .distinct()
        .sorted()
        .toMutableList()
        .apply {
            add(0, leagueAnyLeague)
            add(0, "Not specified")
            forEachIndexed { id, label ->
                DB.Leagues.insert(id, label)
            }
        }

    //item bases and base categories
    val baseCategories = data["itemBaseTypeList"]
        .strings()
        .apply {
            forEachIndexed { id, category ->
                DB.BaseCategories.insert(id, category)
            }
        }

    class BaseInfo(
        val id: Int,
        val name: String,
        val label: String,
        val baseCategoryId: Int
    )

    var baseIdCounter = 1
    val basesByName = data["itemBaseLists"]
        .entries()
        .flatMap { (category, bases) ->
            val categoryId = baseCategories.indexOf(category.checkjstring())
            bases.arrayValues().map { base ->
                val name = base["name"].checkjstring()
                val label = base["label"].checkjstring()
                BaseInfo(baseIdCounter++, name, label, categoryId)
            }
        }
        .onEach {
            it.apply {
                DB.Bases.insert(id, name, label, baseCategoryId)
            }
        }
        .associateBy { it.name }

    //uniques
    val weaponTypeInfo = lua.getBy("data.$version.weaponTypeInfo")
    val luaTooltip = lua.getBy("new").call("Tooltip")
    val makeTooltip = { item: LuaValue ->
        luaTooltip.selfCall("Clear", log = false)
        itemsTab.selfCall("generateTooltip", luaTooltip, item, log = false)
        luaTooltip
    }
    val orderedUniques = uniques
        .mapKeys { it.key.checkjstring() }
        .toSortedMap()
    var uniqueIdCounter = 1
    orderedUniques.forEach { (uniqueName, unique) ->
        println("Saving $uniqueName")
        val uniqueId = uniqueIdCounter++

        unique["league"]
            .optjstring("")
            .split(",")
            .map { it.trim() }
            .filter { it.isNotEmpty() }
            .onEach { league ->
                val leagueId = leagues.indexOf(league)
                DB.UniquesLeagues.insert(uniqueId, leagueId)
            }
            .takeIf { it.isNotEmpty() }
            ?.run {
                DB.UniquesLeagues.insert(uniqueId, leagues.indexOf(leagueAnyLeague))
            }

        val uniqueType = unique["type"].checkjstring()
        val uniqueTypeId = types.indexOf(uniqueType)
        weaponTypeInfo[uniqueType].opttable(null)?.also { typeInfo ->
            val meleeOneHand = typeInfo["melee"].checkboolean() && typeInfo["oneHand"].checkboolean()
            val meleeTwoHand = typeInfo["melee"].checkboolean() && !typeInfo["oneHand"].checkboolean()
            when {
                meleeOneHand -> types.indexOf(extraTypeOneHanded)
                meleeTwoHand -> types.indexOf(extraTypeTwoHanded)
                else         -> null
            }?.let { extraWeaponType ->
                DB.UniquesTypes.insert(uniqueId, extraWeaponType)
            }
        }
        DB.UniquesTypes.insert(uniqueId, uniqueTypeId)

        val (tooltip, tooltipMods, tooltipOffset, tooltipUniqueName) = makeTooltip(unique).let(::processTooltip)
        val uniqueRaw = unique["raw"].checkjstring()
        val baseId = resolveBaseName(unique)
            .let(basesByName::get)
            ?.id

        DB.Uniques.insert(uniqueId, baseId, uniqueRaw, tooltip.encode(), tooltipOffset)
        DB.UnuqueFTSSearch.insert(tooltipUniqueName, tooltipMods, uniqueId)
    }
    lua["main"]["uniqueDB"] = LuaValue.NIL
}

private fun processTooltip(tooltip: LuaValue): DisassembledTooltip {
    val parsed = Tooltip(tooltip).serialize()
    val lines = parsed.text.split("\n")
    val searchableMods = when {
        lines.any { it.startsWith("Requires Level") } -> lines.takeLastWhile { !it.startsWith("Requires Level") }
        lines.any { it.startsWith("Sockets:") }       -> lines.takeLastWhile { !it.startsWith("Sockets:") }
        lines.any { it.startsWith("Variant:") }       -> lines.takeLastWhile { !it.startsWith("Variant:") }
        lines[1].contains("Jewel")                    -> lines.drop(2)
        lines[0] in lvl1Jewelry                       -> lines.drop(2)
        else                                          -> error(parsed.text)
    }

    val uniqueName = lines.take(2).joinToString("\n")

    val marks = listOf("Elder", "Shaper")
    val extraName = lines[2]
      .takeIf { supposedMark ->
          marks.any {
              supposedMark.contains(it, ignoreCase = true)
          }
      }
      ?.let { "\n$it" }
      ?: ""

    val modsString = searchableMods.joinToString(separator = "\n", prefix = "\n")
    val modsOffset = parsed.text.indexOf(modsString)// + 1
    if (modsOffset == -1) error(modsString)
    return DisassembledTooltip(parsed, modsString, modsOffset, uniqueName + extraName)
}

private data class DisassembledTooltip(
    val tooltip: TooltipInfo,
    val modsOnly: String,
    val modsOffset: Int,
    val uniqueName: String
)

private fun resolveBaseName(unique: LuaValue): String {
    val base = unique["base"].checktable()
    val baseName = unique["baseName"].checkjstring()
    val isTwoStoneRing = base["tags"]["twostonering"].optboolean(false)
    return when {
        isTwoStoneRing -> {
            val implicit = base["implicit"].checkjstring()
            when {
                implicit.contains("Fire and Lightning") -> "Fire/Lightning"
                implicit.contains("Cold and Lightning") -> "Cold/Lightning"
                implicit.contains("Fire and Cold")      -> "Fire/Cold"
                else                                    -> error(baseName)
            }.let { "Two-Stone Ring ($it)" }
        }
        else           -> baseName
    }
}


private fun DB.Table.insert(vararg values: Any?) {
    val sql = "INSERT INTO $tableName VALUES (${values.map { "?" }.joinToString()})"
    val stm = db.prepareStatement(sql)
    values.forEachIndexed { index, value ->
        val i = index + 1
        when (value) {
            null         -> stm.setNull(i, i)
            is String    -> stm.setString(i, value)
            is Int       -> stm.setInt(i, value)
            is Long      -> stm.setLong(i, value)
            is ByteArray -> stm.setBytes(i, value)
            else         -> error("Incorrect value type $value ${value::class.java}")
        }
    }

    stm.execute()
}


private val lvl1Jewelry = listOf(
    "Araku Tiki",
    "Blackheart",
    "Doedre's Damning",
    "Kaom's Sign"
)

//
//val slots = listOf(
//    "Any slot",
//    "Weapon 1",
//    "Weapon 2",
//    "Helmet",
//    "Body Armour",
//    "Gloves",
//    "Boots",
//    "Amulet",
//    "Ring",
//    "Belt",
//    "Jewel"
//)