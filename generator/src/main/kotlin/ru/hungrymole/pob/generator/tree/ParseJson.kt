package ru.hungrymole.pob.generator.tree

import com.github.kittinunf.fuel.httpGet
import com.squareup.moshi.Moshi
import ru.hungrymole.pob.common.androidColor
import ru.hungrymole.pob.generator.tree.TreeConstants.RESIZE
import java.io.File
import java.util.*

fun loadAndParseJson(): JsonOutput {
    var (json, ascJson) = loadJson()
//    json = File("treeAssets/SkillTree.txt").readText()
    val treeJson = moshi.adapter(Any::class.java).fromJson(json) as JsonObj

    val const = treeJson.to("constants")

    val getDimmen = { name: String -> treeJson.float(name).let(Math::abs).toInt() }
    val rect = Rect(
            getDimmen("min_x"),
            getDimmen("min_y"),
            getDimmen("max_x"),
            getDimmen("max_y"))

    val orbits = const.array<Int>("orbitRadii")
            .map { it * RESIZE }
            .zip(const.array<Int>("skillsPerOrbit"))
            .mapIndexed { index, info ->
                val (radius, skillsPer) = info
                Orbit(index, radius, skillsPer)
            }

    val nodes = treeJson.to("nodes").map {(_,v) ->
        v as JsonObj
        v.run {
            val id = int("id")
            val name = str("dn")
            val isNotable = bool("not")
            val icon = str("icon")
            val groupId = int("g")
            val orbit = int("o")
            val orbitIndex = int("oidx")
            val nodes = array<Double>("out").map { it.toInt() }
            val ascName = try { str("ascendancyName") } catch (e: Exception) { null }
            val isAscendancyStart = try { bool("isAscendancyStart") } catch (e: Exception) { false }
            val classId = array<Double>("spc").firstOrNull()?.toInt()
            val type = if (!ascName.isNullOrEmpty()) {
                when {
                    isNotable && name.startsWith("Path of") -> NODE_TYPE.ASC_SCION_PATH_OF_X
                    isNotable && bool("isMultipleChoice")-> NODE_TYPE.ASC_SCION_MULTI_CHOICE
                    isNotable -> NODE_TYPE.ASC_NOTABLE
                    isAscendancyStart -> NODE_TYPE.ASC_START
                    bool("isMultipleChoiceOption") -> NODE_TYPE.ASC_SCION_MULTI_CHOICE_OPTION
                    else -> NODE_TYPE.ASC_NORMAL
                }
            } else {
                when {
                    isNotable -> NODE_TYPE.NOTABLE
                    name == "Jewel Socket" -> NODE_TYPE.JEWEL
                    bool("m") -> NODE_TYPE.MASTERY
                    bool("ks") -> NODE_TYPE.KEYSTONE
                    classId != null -> NODE_TYPE.CLASS
                    else -> NODE_TYPE.NORMAL
                }
            }
            JsonTreeNode(id, type, orbits[orbit], orbitIndex, icon, groupId, nodes, ascName, classId)
        }
    }

    val groups = treeJson.to("groups").map { (id, obj) ->
        val group = obj.asJsonObj()
        val x = group.float("x")
        val y = group.float("y")
        val groupNodes = group
                .array<Double>("n")
                .map { it.toInt() }
        JsonGroup(id.toInt(), x, y, groupNodes)
    }

    val ascs = ascJson.let(moshi.adapter(Any::class.java)::fromJson).asJsonObj().flatMap {
        val classId = it.key.toInt()
        it.value.asJsonObj().to("classes").map { (_,v) ->
            val asc = v.asJsonObj()
            val name = asc.str("name")
            val fText = asc.str("flavourText")
            val (fleft, ftop, fright, fbottom) = asc.str("flavourTextRect")
                    .split(",")
                    .map { it.toInt() }
            val (r, g, b) = asc.str("flavourTextColour")
                    .split(",")
                    .map { it.toInt() }
            val fColor = androidColor("$r$g$b")
            JsonAscendancy(classId, name, fText, fColor, Rect(fleft, ftop, fright, fbottom))
        }
    }

    val sprites = treeJson.to("skillSprites").map { (iconType, sprites) ->
        val info = (sprites as ArrayList<JsonObj>).last()
        val filename = info.str("filename")
        val icons = info.to("coords").map { (k, v) ->
            val r = v.asJsonObj()
            val rect = Rect(
                    r.int("x"),
                    r.int("y"),
                    r.int("w"),
                    r.int("h"))
            k to rect
        }.toMap()
        iconType to Sprite(filename, icons)
    }.toMap()

    return JsonOutput(nodes, groups, orbits, rect, ascs, sprites)
}

private fun loadJson(): Pair<String, String> {
    val html = "https://www.pathofexile.com/passive-skill-tree"
            .httpGet()
            .header("User-Agent" to UUID.randomUUID())//403 he owner of this website (www.pathofexile.com) has banned your access based on your browser's signature.
            .responseString()
            .third
            .component1()!!

    fun readLine(start: String): String {
        return html.split("\n")
                .map { it.trim() }
                .first { it.startsWith(start) }
                .replace(start, "")
    }

    return readLine("var passiveSkillTreeData = ") to readLine("ascClasses: ")
}

val moshi = Moshi.Builder().build()
inline fun <reified T> String.fromJson(): T {
    return moshi.adapter(T::class.java).fromJson(this)!!
}

inline fun <reified T> Any.toJson(): String {
    return moshi.adapter(T::class.java).toJson(this as T)
}

private typealias JsonObj = Map<String, *>
private fun Any?.asJsonObj(): JsonObj {
    return this as JsonObj
}

private fun JsonObj.to(k: String): JsonObj {
    return get(k) as JsonObj
}

private fun JsonObj.str(k: String): String {
    return get(k) as String
}

private fun JsonObj.int(k: String): Int {
    return (get(k) as Double).toInt()
}

private fun JsonObj.float(k: String): Float {
    return (get(k)!! as Double).toFloat()
}

private fun JsonObj.bool(k: String): Boolean {
    return get(k) as Boolean
}

private fun <T : Any> JsonObj.array(k: String): ArrayList<T> {
    return get(k) as ArrayList<T>
}


data class JsonOutput(
        val nodes: List<JsonTreeNode>,
        val groups: List<JsonGroup>,
        val orbits: List<Orbit>,
        val rect: Rect,
        val ascs: List<JsonAscendancy>,
        val sprites: Map<String, Sprite>
)
data class Sprite(
    val fileName: String,
    val icons: Map<String, Rect> = mutableMapOf())

data class Rect(
        val left: Int,
        val top: Int,
        val right: Int,
        val bottom: Int)

class JsonTreeNode(
        val id: Int,
        val type: NODE_TYPE,
        val orbit: Orbit,
        val orbitPosition: Int,
        val icon: String,
        val groupId: Int,
        val linked: List<Int>,
        val ascName: String?,
        val classId: Int?)

class JsonGroup(
        val id: Int,
        val x: Float,
        val y: Float,
        val nodes: List<Int>)

class Orbit(
        val index: Int,
        val radius: Float,
        val nodeCount: Int)

class JsonAscendancy(
        val classId: Int,
        val name: String,
        val fText: String,
        val fColor: Int,
        val fRect: Rect)

enum class NODE_TYPE {
    CLASS,
    NORMAL,
    NOTABLE,
    MASTERY,
    JEWEL,
    KEYSTONE,
    ASC_START,
    ASC_NORMAL,
    ASC_NOTABLE,
    ASC_SCION_MULTI_CHOICE,
    ASC_SCION_MULTI_CHOICE_OPTION,
    ASC_SCION_PATH_OF_X
}