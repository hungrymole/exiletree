package ru.hungrymole.pob.generator.tree

import com.github.kittinunf.fuel.httpGet
import ru.hungrymole.pob.generator.tree.NODE_TYPE.*
import ru.hungrymole.pob.generator.tree.TreeConstants.ASC_ALLOC_BUTTON_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.ASC_START_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.ASC_UNALLOC_BUTTON_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.CLASS_UNALLOCATED_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.GROUP_ORBIT_1_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.GROUP_ORBIT_2_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.GROUP_ORBIT_3_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.JEWEL_ALLOC_ABYSS_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.JEWEL_ALLOC_COBALT_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.JEWEL_ALLOC_CRIMSON_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.JEWEL_ALLOC_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.JEWEL_ALLOC_PRISMATIC_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.JEWEL_ALLOC_VIRIDIAN_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.JEWEL_UNALLOC_ICON_ID
import java.awt.Image
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import java.io.File
import java.util.*
import javax.imageio.ImageIO

fun generateIcons(nodes: List<JsonTreeNode>,
                  sprites: Map<String, Sprite>): Pair<Icons, NodeIconKeys> {
    val fileToImage = sprites.values.map { it.fileName }.toSet().map { filename ->
//        /*
        val img = "http://web.poecdn.com/image//build-gen/passive-skill-sprite/$filename".httpGet().response().third.component1()!!
        filename to img.inputStream().let(ImageIO::read)
//        */
        /*
        filename to filename.split("?").first().loadImage()
        */
    }.toMap()

    var keyCounter = 20
    val hashToKey = mutableMapOf<Int, Int>()
    fun create(iconType: String,
               iconName: String,
               frameFileName: String): Int {
        val hash = ImageKey(iconType, iconName, frameFileName).hashCode()
        val key = hashToKey.getOrPut(hash) { keyCounter++ }
        icons.getOrPut(key) {
            val source = sprites[iconType]!!.fileName.let(fileToImage::get)!!
            val sprite = sprites[iconType]!!.icons[iconName]!!
            val icon = source.exract(sprite)
            val frame = assets.load(frameFileName)
            merge(frame, icon)
        }
        return key
    }

    val jewelAllocFrame = assets.load(JEWEL_FRAME_ALLOC)

    fun makeJewelTypeIcon(key: Int, fileName: String) {
        icons[key] = merge(fileName.loadImage(), jewelAllocFrame)
    }
    makeJewelTypeIcon(JEWEL_ALLOC_COBALT_ICON_ID, COBALT_JEWEL_ALLOCATED)
    makeJewelTypeIcon(JEWEL_ALLOC_CRIMSON_ICON_ID, CRIMSON_JEWEL_ALLOCATED)
    makeJewelTypeIcon(JEWEL_ALLOC_VIRIDIAN_ICON_ID, VIRIDIAN_JEWEL_ALLOCATED)
    makeJewelTypeIcon(JEWEL_ALLOC_PRISMATIC_ICON_ID, PRISMATIC_JEWEL_ALLOCATED)
    makeJewelTypeIcon(JEWEL_ALLOC_ABYSS_ICON_ID, ABYSS_JEWEL_ALLOCATED)


    icons[JEWEL_UNALLOC_ICON_ID] = assets.load(JEWEL_FRAME_UNALLOC).toBytes()
    icons[JEWEL_ALLOC_ICON_ID] = jewelAllocFrame.toBytes()

    icons[ASC_START_ICON_ID] = assets.load(ASC_CENTER).toBytes()

    icons[CLASS_UNALLOCATED_ICON_ID] = assets.load(CLASS_NODE_UNALLOCATED).toBytes()

    icons[GROUP_ORBIT_1_ICON_ID] = assets.load(SMALL_GROUP_BG).toBytes()
    icons[GROUP_ORBIT_2_ICON_ID] = assets.load(MEDIUM_GROUP_BG).toBytes()
    icons[GROUP_ORBIT_3_ICON_ID] = assets.load(LARGE_GROUP_BG).toBytes()

    icons[ASC_UNALLOC_BUTTON_ICON_ID] = assets.load(ASC_BUTTON).toBytes()
    icons[ASC_ALLOC_BUTTON_ICON_ID] = assets.load(ASC_BUTTON_PRESSED).toBytes()

    nodes.forEach { node ->
        val ascNotables = listOf(ASC_NOTABLE, ASC_SCION_MULTI_CHOICE, ASC_SCION_PATH_OF_X)
        val ascNodes = listOf(ASC_NORMAL, ASC_SCION_MULTI_CHOICE_OPTION)
        val (unallocKey, allocKey) = when (node.type) {
            NORMAL         -> create(NODE_UNALLOC, node.icon, NODE_FRAME_UNALLOC) to create(NODE_ALLOC, node.icon, NODE_FRAME_ALLOC)
            NOTABLE        -> create(NOTABLE_UNALLOC, node.icon, NOTABLE_FRAME_UNALLOC) to create(NOTABLE_ALLOC, node.icon, NOTABLE_FRAME_ALLOC)
            KEYSTONE       -> create(KEYSTONE_UNALLOC, node.icon, KEYSTONE_FRAME_UNALLOC) to create(KEYSTONE_ALLOC, node.icon, KEYSTONE_FRAME_ALLOC)
            in ascNodes    -> create(NODE_UNALLOC, node.icon, ASC_NODE_FRAME_UNALLOC) to create(NODE_ALLOC, node.icon, ASC_NODE_FRAME_ALLOC)
            in ascNotables -> create(NOTABLE_UNALLOC, node.icon, ASC_NOTABLE_FRAME_UNALLOC) to create(NOTABLE_ALLOC, node.icon, ASC_NOTABLE_FRAME_ALLOC)
            JEWEL          -> JEWEL_UNALLOC_ICON_ID to JEWEL_ALLOC_ICON_ID
            ASC_START      -> ASC_START_ICON_ID to ASC_START_ICON_ID
            CLASS          -> CLASS_UNALLOCATED_ICON_ID to node.id
            MASTERY        -> {
                val hash = ImageKey("", node.icon, "").hashCode()
                val key = hashToKey.getOrPut(hash) { keyCounter++ }
                icons.computeIfAbsent(key) {
                    val source = sprites[MASTERY_ALL]!!.fileName.let(fileToImage::get)!!
                    val sprite = sprites[MASTERY_ALL]!!.icons[node.icon]!!
                    source.exract(sprite).toBytes()
                }
                key to key
            }
            else           -> error("")
        }
        nodeKeys[node] = unallocKey to allocKey
    }

    TreeConstants.classNodeIdToClassId.forEach { (nodeId, classId) ->
        icons[nodeId] = assets.load("$classId.png").toBytes()
    }

    return icons to nodeKeys
}

private data class ImageKey(
        val iconType: String,
        val key: String,
        val frame: String)

private val assets = mutableMapOf<String, BufferedImage>()
private val nodeKeys: NodeIconKeys = mutableMapOf()
private val icons: Icons = TreeMap()

private typealias Icons = MutableMap<Int, ByteArray>
private typealias NodeIconKeys = MutableMap<JsonTreeNode, Pair<Int, Int>>

private fun MutableMap<String, BufferedImage>.load(k: String): BufferedImage {
    return getOrPut(k, k::loadImage)
}

private fun String.loadImage(): BufferedImage {
    return File("treeAssets/$this").let(ImageIO::read)
}

private fun BufferedImage.exract(r: Rect): BufferedImage = getSubimage(r.left, r.top, r.right, r.bottom)

private fun BufferedImage.toBytes(): ByteArray {
    return ByteArrayOutputStream().run {
        ImageIO.write(this@toBytes, "png", this)
        flush()
        close()
        toByteArray()
    }
}

private fun merge(frame: BufferedImage, icon: BufferedImage): ByteArray {
    val result = BufferedImage(frame.width, frame.height, BufferedImage.TYPE_INT_ARGB)
    val iconY = ((result.width.toFloat() - icon.width) / 2).toInt()
    val iconX = ((result.height.toFloat() - icon.height) / 2).toInt()
    result.graphics.drawImage(icon, iconY, iconX) { image: Image, i: Int, i1: Int, i2: Int, i3: Int, i4: Int -> true }
    result.graphics.drawImage(frame, 0, 0)  { image: Image, i: Int, i1: Int, i2: Int, i3: Int, i4: Int -> true }
    return result.toBytes()
}

private val SMALL_GROUP_BG = "PSGroupBackground1.png"
private val MEDIUM_GROUP_BG = "PSGroupBackground2.png"
private val LARGE_GROUP_BG = "PSGroupBackground3.png"

//icon types
private val NODE_ALLOC = "normalActive"
private val NODE_UNALLOC = "normalInactive"
private val NOTABLE_ALLOC = "notableActive"
private val NOTABLE_UNALLOC = "notableInactive"
private val KEYSTONE_ALLOC = "keystoneActive"
private val KEYSTONE_UNALLOC = "keystoneInactive"
private val MASTERY_ALL = "mastery"

private val ASC_CENTER = "PassiveSkillScreenAscendancyMiddle.png"
private val ASC_BUTTON = "PassiveSkillScreenAscendancyButton.png"
private val ASC_BUTTON_PRESSED = "PassiveSkillScreenAscendancyButtonPressed.png"
private val ASC_NODE_FRAME_ALLOC = "PassiveSkillScreenAscendancyFrameSmallAllocated.png"
private val ASC_NODE_FRAME_UNALLOC = "PassiveSkillScreenAscendancyFrameSmallNormal.png"
private val ASC_NOTABLE_FRAME_ALLOC = "PassiveSkillScreenAscendancyFrameLargeAllocated.png"
private val ASC_NOTABLE_FRAME_UNALLOC = "PassiveSkillScreenAscendancyFrameLargeNormal.png"

//frames
private val NODE_FRAME_UNALLOC = "PSSkillFrame.png"
private val NODE_FRAME_ALLOC = "PSSkillFrameActive.png"
private val NOTABLE_FRAME_UNALLOC = "NotableFrameUnallocated.png"
private val NOTABLE_FRAME_ALLOC = "NotableFrameAllocated.png"
private val KEYSTONE_FRAME_UNALLOC = "KeystoneFrameUnallocated.png"
private val KEYSTONE_FRAME_ALLOC = "KeystoneFrameAllocated.png"
private val JEWEL_FRAME_UNALLOC = "JewelFrameUnallocated.png"
private val JEWEL_FRAME_ALLOC = "JewelFrameAllocated.png"
private val CLASS_NODE_UNALLOCATED = "PSStartNodeBackgroundInactive.png"
private val CRIMSON_JEWEL_ALLOCATED = "JewelSocketActiveRed.png"
private val VIRIDIAN_JEWEL_ALLOCATED = "JewelSocketActiveGreen.png"
private val COBALT_JEWEL_ALLOCATED = "JewelSocketActiveBlue.png"
private val PRISMATIC_JEWEL_ALLOCATED = "JewelSocketActivePrismatic.png"
private val ABYSS_JEWEL_ALLOCATED = "JewelSocketActiveAbyss.png"