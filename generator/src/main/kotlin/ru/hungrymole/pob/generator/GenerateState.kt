package ru.hungrymole.pob.generator

import org.luaj.vm2.LuaValue
import org.luaj.vm2.lib.ResourceFinder
import org.luaj.vm2.lib.jse.JsePlatform
import ru.hungrymole.pob.Constants
import ru.hungrymole.pob.generator.lua.generateTree
import ru.hungrymole.pob.generator.lua.saveGenerated
import ru.hungrymole.pob.lua.serialization.LuaTableSerializer
import ru.hungrymole.pob.lua.values
import java.io.FileInputStream

/**`
 * Created by Dinar Khusainov on 01.04.2018.
 */
fun main(args: Array<String>) {
    val lua = JsePlatform.standardGlobals().apply {
        finder = ResourceFinder(::FileInputStream)
        loadPobEnviroment(generator = false)
    }

    generateTree(lua)
        .let(LuaTableSerializer.Companion::serializeTable)
        .let(GeneratedLuaTree::saveGenerated)
}