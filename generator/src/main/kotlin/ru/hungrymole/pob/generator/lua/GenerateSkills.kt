package ru.hungrymole.pob.generator.lua
import java.io.File

/**
 * Created by khusainov.d on 15.09.2017.
 */
fun generateSkills(): MutableList<LoadableSkill> {
    val skillRegex = "\"([A-z'0-9]*)\"\\] = ([\\s\\S]+?)(?=\nskills|$)".toRegex()
    val nameRegex = "name = \"([A-z ']*)\"".toRegex()
    val skills = mutableListOf<LoadableSkill>()
    File("Data/$version/Skills").listFiles().forEach { file ->
        val fileName = file.name
        val s = file.readText()
        s.split("skills[").drop(1).forEach {
            val g = skillRegex.matchEntire(it)?.groups?.mapNotNull { it?.value }!!
            val key = g[1]//key for table (ArcticBreath, SupportUniqueCosprisMaliceColdSpellsCastOnMeleeCriticalStrike)
            val luaTable = g[2]
            val nameSpec = nameRegex.find(luaTable)?.groupValues?.getOrNull(1)!!
            val start = s.indexOf(luaTable)
            val length = luaTable.length
            val color = when {
                file.name.contains("str") -> colors["MARAUDER"]
                file.name.contains("dex") -> colors["RANGER"]
                file.name.contains("int") -> colors["WITCH"]
                else -> null
            }
            skills += LoadableSkill(fileName, key, start, length, nameSpec, color)
            println(nameSpec)
        }
    }
    return skills
}