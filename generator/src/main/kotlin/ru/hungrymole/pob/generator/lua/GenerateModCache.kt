package ru.hungrymole.pob.generator.lua

import java.io.File

/**
 * Created by khusainov.d on 20.09.2017.
 */

fun generateModCache(): List<LoadableMod> {
    val splitRegex = "([ A-z0-9:{},.'%+()-]{1,500})\\\"\\]=(.*)".toRegex()
    val raw = File("Data/$version/ModCache.lua").readText()
    return raw.split("c[\"").drop(1).map {
        val g = splitRegex.matchEntire(it)?.groups?.mapNotNull { it?.value }!!
        val modLine = g[1]
        val rawMod = g[2]
        val start = raw.indexOf(rawMod)
//        println("$modLine -- ${modLine.hashCode()} -- ${modLine.toByteArray().joinToString("|")}")
        println(modLine)

        LoadableMod(start, rawMod.length, modLine.hashCode())
    }.sortedBy { it.lineHash }
}