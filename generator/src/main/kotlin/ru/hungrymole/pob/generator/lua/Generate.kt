package ru.hungrymole.pob.generator.lua

import java.io.File

/**
 * Created by khusainov.d on 27.09.2017.
 */
fun generateBases(): List<Loadable> {
    val bases = mutableListOf<Loadable>()
    File("Data/$version/Bases").listFiles().forEach {
        val file = it.name
        val text = it.readText()
        text.split("itemBases[\"").drop(1).forEach {
            val rawBase = it.substringAfter('=').trim()
            val name = it.split("\"").first()
            val start = text.indexOf(rawBase)
            val length = rawBase.length
            bases += Loadable(file, name, start, length)
            println(name)
        }
    }
    return bases
}

fun generateMinions(): List<Loadable> {
    val minions = mutableListOf<Loadable>()
    val text = File("Data/$version/Minions.lua").readText()
    text.split("minions[\"").drop(1).forEach {
        val rawMinion = it.substringAfter('=').trim()
        val name = it.split("\"").first()
        val start = text.indexOf(rawMinion)
        val length = rawMinion.length
        minions += Loadable("Minions.lua", name, start, length)
        println(name)
    }
    return minions
}