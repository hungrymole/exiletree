package ru.hungrymole.pob.generator.lua

import com.sun.xml.internal.messaging.saaj.util.ByteOutputStream
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.lua.call
import java.io.File
import java.util.zip.ZipInputStream

/**
 * Created by Dinar on 10.01.2018.
 */
internal fun generateTree(lua: Globals): LuaTable {
    val rawTree = getFileFromZip(File("tree.zip"), "TreeData/$version/tree.lua")
    val treeTable = lua.load(rawTree).call()
    lua["LoadModule"].call("Classes/PassiveTree", LuaTable(), LuaTable())
    val tree = lua["new"].call("PassiveTree", version, treeTable)
    val sanitizedTreeTable = tree["dump"].call(tree).checktable()
    sanitizedTreeTable.setmetatable(null)
    lua["common"]["classes"].set("PassiveTree", LuaValue.NIL)
    return sanitizedTreeTable
}

private fun getFileFromZip(zipFile: File,
                           name: String): String {

    var result: String? = null
    val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
    val zip = ZipInputStream(zipFile.inputStream())
    var zipEntry = zip.nextEntry
    while (zipEntry != null) {
        if (zipEntry.name != name) {
            zipEntry = zip.nextEntry
            continue
        }
        val out = ByteOutputStream()
        var len = zip.read(buffer)
        while (len > 0) {
            out.write(buffer, 0, len)
            len = zip.read(buffer)
        }
        out.close()
        result = String(out.bytes)
        break
    }
    zip.closeEntry()
    zip.close()
    return result ?: error("File $name not found")
}