package ru.hungrymole.pob.generator.tree

import okio.ByteString
import ru.hungrymole.pob.generator.GeneratedJavaTree
import ru.hungrymole.pob.generator.tree.TreeConstants.GROUP_ORBIT_1_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.GROUP_ORBIT_2_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.GROUP_ORBIT_3_ICON_ID
import ru.hungrymole.pob.generator.tree.TreeConstants.RESIZE
import ru.hungrymole.pob.generator.tree.TreeConstants.classNodeIdToClassId
import java.io.File

fun main(args: Array<String>) {
    val (nodes, groups, orbits, rect, ascs, sprites) = loadAndParseJson()
    val (minX, minY, maxX, maxY) = rect
    val right = (maxX + minX) * RESIZE
    val bottom = (maxY + minY) * RESIZE
    val nodeById = nodes.associateBy { it.id }
    val (images, nodeIconKeys) = generateIcons(nodes, sprites)

    val outGroups = groups.map {
        val maxOrbit = nodes
                .filter { node -> node.groupId == it.id }
                .map { it.orbit.index }
                .max()
        val background = when (maxOrbit) {
            1 -> GROUP_ORBIT_1_ICON_ID
            2 -> GROUP_ORBIT_2_ICON_ID
            3 -> GROUP_ORBIT_3_ICON_ID
            else -> 0
        }
        Group(((it.x + minX) * RESIZE).toInt(), ((it.y + minY) * RESIZE).toInt(), it.id, background, it.nodes)
    }.toMutableList()


    val coordinatesOfClass = { id: Int ->
        val nodeId = classNodeIdToClassId.first { it.second == id }.first
        val node = nodeById[nodeId]!!
        val group = outGroups.first { nodeId in it.nodes  }
        node.calculatePosition(group)
    }
    val (scionX, scionY) = coordinatesOfClass(0)
    val ascendancies = ascs.map { asc ->
        val (startX, startY) = coordinatesOfClass(asc.classId)
        val ascGroups = nodes.filter { it.ascName == asc.name }
                .map { it.groupId }
                .toSet()
                .let { ids -> outGroups.filter { it.id in ids }  }

        val distanceFromStartNodeCenter = 270 * RESIZE;
        val isScion = scionX == startX
        val angleRad = when {
            !isScion -> Math.atan2(startX - scionX.toDouble(), -(startY - scionY.toDouble()))
            else -> Math.PI * 2
        }.toFloat()

        val buttonX = startX + distanceFromStartNodeCenter * Math.cos(angleRad + Math.PI / 2).toFloat()
        val buttonY = startY + distanceFromStartNodeCenter * Math.sin(angleRad + Math.PI / 2).toFloat()

        val bgRadius = (if (!isScion) 500 else 589) / 2f
        val posX = startX + (distanceFromStartNodeCenter + bgRadius) * Math.cos(angleRad + Math.PI / 2)
        val posY = startY + (distanceFromStartNodeCenter + bgRadius) * Math.sin(angleRad + Math.PI / 2)

        val centerGroup = nodes
                .first { it.type == NODE_TYPE.ASC_START && it.ascName == asc.name }
                .id
                .let { id -> outGroups.first { id in it.nodes } }

        val dx = posX.toFloat() - centerGroup.x
        val dy = posY.toFloat() - centerGroup.y
        val shiftedAscGroups = ascGroups.map {
            val newX = it.x + dx
            val newY = it.y + dy
            it.newBuilder()
                    .x(newX.toInt())
                    .y(newY.toInt())
                    .build()
        }

        outGroups.removeAll(ascGroups)
        outGroups.addAll(shiftedAscGroups)

        Ascendancy.Builder().apply {
            this.groups = shiftedAscGroups.map { it.id }
            name = asc.name
            x = (centerGroup.x + dx).toInt()
            y = (centerGroup.y + dy).toInt()
            flavor = AscendancyText(asc.fText, asc.fColor, asc.fRect.left, asc.fRect.top, asc.fRect.right, asc.fRect.bottom)
            button = AscendancyButton(buttonX.toInt(), buttonY.toInt(), Math.toDegrees(angleRad.toDouble()).toFloat())
        }.build()
    }

    class EdgeFilter(val id1: Int, val id2: Int) {
        override fun hashCode() = id1 + id2
        override fun equals(other: Any?): Boolean {
            other as EdgeFilter
            return (setOf(id1, id2) + setOf(other.id1, other.id2)).size == 2
        }
    }

    val lines = mutableListOf<Line>()
    val arcs = mutableListOf<Arc>()
    nodes.flatMap { node -> node.linked.map { id -> EdgeFilter(node.id, id) } }
            .toSet().map { edge ->
        val n1 = nodeById[edge.id1]!!
        val n2 = nodeById[edge.id2]!!
        when {
            n1.groupId == n2.groupId && n1.orbit == n2.orbit -> {
                val group = outGroups.first { it.id == n1.groupId }
                val (start, sweep) = n1.calcArcToOther(n2)
                arcs += Arc(edge.id1, edge.id2, group.x, group.y, start, sweep, n1.orbit.radius)
            }
            else -> {
                lines += Line(edge.id1, edge.id2)
            }
        }
    }

    val nodeOutput = Nodes.Builder().also {
        it.count = nodes.size
    }
    nodes.forEach { node ->
        val (x, y) = outGroups
                .first { group -> node.id in group.nodes }
                .let(node::calculatePosition)
        val typeList = when (node.type) {
            NODE_TYPE.CLASS                         -> nodeOutput.CLASS
            NODE_TYPE.NORMAL                        -> nodeOutput.NORMAL
            NODE_TYPE.NOTABLE                       -> nodeOutput.NOTABLE
            NODE_TYPE.MASTERY                       -> nodeOutput.MASTERY
            NODE_TYPE.JEWEL                         -> nodeOutput.JEWEL
            NODE_TYPE.KEYSTONE                      -> nodeOutput.KEYSTONE
            NODE_TYPE.ASC_START                     -> nodeOutput.ASC_START
            NODE_TYPE.ASC_NORMAL                    -> nodeOutput.ASC_NORMAL
            NODE_TYPE.ASC_NOTABLE                   -> nodeOutput.ASC_NOTABLE
            NODE_TYPE.ASC_SCION_MULTI_CHOICE        -> nodeOutput.ASC_SCION_MULTI_CHOICE
            NODE_TYPE.ASC_SCION_MULTI_CHOICE_OPTION -> nodeOutput.ASC_SCION_MULTI_CHOICE_OPTION
            NODE_TYPE.ASC_SCION_PATH_OF_X           -> nodeOutput.ASC_SCION_PATH_OF_X
        }
        val (unalloc, alloc) = nodeIconKeys[node]!!
        typeList += Node(x.toInt(), y.toInt(), node.id, unalloc, alloc, node.linked)
        typeList.sortBy { it.id }
    }

    val result = ParsedTree.Builder().also {
        it.nodes = nodeOutput.build()
        it.bottom = bottom.toInt()
        it.right = right.toInt()
        it.groups = outGroups
        it.arcs = arcs
        it.lines = lines
        it.ascendanceis = ascendancies
        it.icons = images.mapValues {  ByteString.of(it.value, 0, it.value.size)  }
    }.build()

    GeneratedJavaTree.write(File(args.first()), result)
}

private fun JsonTreeNode.calcArcToOther(other: JsonTreeNode): Pair<Float, Float> {
    val angle1 = (getClockAngle() - 90).let {
        if (it < 0 ) 360 + it else it
    }
    val angle2 = (other.getClockAngle() - 90).let {
        if (it < 0 ) 360 + it else it
    }
    var sweep = Math.abs(angle1 - angle2)
    var start = Math.min(angle1, angle2)
    if (sweep > 180f) {
        sweep = 360f - sweep
        start = Math.max(angle1, angle2)
    }
    return start to sweep
}

private fun JsonTreeNode.getClockAngle(): Float {
    val degreePerNode = 360F / orbit.nodeCount
    val angle = orbitPosition * degreePerNode
    return angle
}

private fun JsonTreeNode.calculatePosition(group: Group): Pair<Float, Float> {
    val radius = orbit.radius
    val angle = getClockAngle().toDouble().let(Math::toRadians)
    val x = (group.x + radius * Math.sin(angle)).toFloat()
    val y = (group.y - radius * Math.cos(angle)).toFloat()
    return x to y
}





