package ru.hungrymole.pob.kapt

import com.squareup.kotlinpoet.*
import org.jetbrains.annotations.Nullable
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.lua.Color
import ru.hungrymole.pob.lua.HasTableKey
import ru.hungrymole.pob.lua.KeyName
import ru.hungrymole.pob.lua.Table
import java.io.File
import javax.annotation.processing.AbstractProcessor
import javax.annotation.processing.ProcessingEnvironment
import javax.annotation.processing.RoundEnvironment
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement
import javax.lang.model.element.VariableElement
import javax.lang.model.type.TypeKind.BOOLEAN
import javax.lang.model.type.TypeKind.DOUBLE
import javax.lang.model.type.TypeKind.FLOAT
import javax.lang.model.type.TypeKind.INT
import javax.lang.model.type.TypeMirror

/**
 * Created by Dinar Khusainov on 24.12.2017.
 */

internal class GenerateLuaDeserializers : AbstractProcessor() {

    val elements by lazy { processingEnv.elementUtils }
    val pkg = "ru.hungrymole.pob.lua"
    val ANDROID_BLACK = -0x1000000
    val target = Table::class.java.canonicalName
    lateinit var out: File
    override fun getSupportedAnnotationTypes() = mutableSetOf(target)

//    val log = File("C:\\Users\\mole\\Desktop\\log.txt")

    override fun init(processingEnv: ProcessingEnvironment) {
        super.init(processingEnv)
        out = processingEnv.options
            .getValue("kapt.kotlin.generated")
            .replace("kaptKotlin", "kapt")
            .let(::File)
    }

    override fun process(annotations: MutableSet<out TypeElement>, env: RoundEnvironment): Boolean {
        val classes = annotations
            .firstOrNull { it.toString() == target }
            ?.let(env::getElementsAnnotatedWith)
        if (classes == null || classes.isEmpty()) return false

        val LT = LuaTable::class.asTypeName()
        val HAS_TABLE_KEY = HasTableKey::class.asClassName()
        val LUA_TABLE = ParameterSpec.builder("t", LuaTable::class.asTypeName()).build()
        val T_ANY = TypeVariableName("T", Any::class)
        val CLASS_T = ParameterizedTypeName.get(Class::class.asClassName(), T_ANY)
        val LIST_T = ParameterizedTypeName.get(List::class.asClassName(), T_ANY)
        val MAP_STRING_INT = ParameterizedTypeName.get(HashMap::class, String::class, Int::class)
        val CLS_T = ParameterSpec.builder("cls", CLASS_T).build()
        val LT_TO_T = LambdaTypeName.get(
            parameters = listOf(LT),
            returnType = T_ANY
        )

        val rootObj = TypeSpec.objectBuilder("LuaDeserializer")

        //color cache
        PropertySpec.builder("prefix", Regex::class, KModifier.PRIVATE)
            .initializer("\"[\\\\^x]\".toRegex()")
            .build()
            .let(rootObj::addProperty)

        PropertySpec.builder("colorCache", MAP_STRING_INT, KModifier.PRIVATE)
            .initializer("HashMap()")
            .build()
            .let(rootObj::addProperty)

        FunSpec.builder("getColor")
            .addModifiers(KModifier.PRIVATE)
            .receiver(LuaValue::class)
            .returns(Int::class)
            .addStatement("val luaColor = checkjstring()")
            .addStatement("return colorCache.getOrPut(luaColor) {")
            .addStatement("    val rgb = luaColor.replace(prefix, \"\")")
            .addStatement("    try { (java.lang.Long.parseLong(rgb, 16) or -0x1000000).toInt() } catch (e: Exception) { $ANDROID_BLACK }")
            .addStatement("}")
            .build()
            .let(rootObj::addFunction)

        //primitive lists

        FunSpec.builder("ints")
            .addModifiers(KModifier.PRIVATE)
            .receiver(LuaTable::class)
            .returns(ParameterizedTypeName.get(List::class.asClassName(), Int::class.asTypeName()))
            .addStatement("val t = checktable()")
            .addStatement("val keys = t.keys()")
            .addStatement("return keys.mapTo(ArrayList<Int>(keys.size)) { t.get(it).checkint() }")
            .build()
            .let(rootObj::addFunction)

        FunSpec.builder("strings")
            .addModifiers(KModifier.PRIVATE)
            .receiver(LuaTable::class)
            .returns(ParameterizedTypeName.get(List::class.asClassName(), String::class.asTypeName()))
            .addStatement("val t = checktable()")
            .addStatement("val keys = t.keys()")
            .addStatement("return keys.mapTo(ArrayList<String>(keys.size)) { t.get(it).checkjstring() }")
            .build()
            .let(rootObj::addFunction)


        val names = mutableListOf<String>()
        classes.forEach { cls ->
            val name = cls.simpleName.toString().also { names.add(it) }
            val constructor = cls.enclosedElements
                .filterIsInstance<ExecutableElement>()
                .first { it.simpleName.contentEquals("<init>") }
//            log.appendText(cls.asType().asTypeName().toString())
//            log.appendText("\n")
            val params = constructor
                .parameters
                .map(::extractParamFromTable)
                .joinToString(prefix = "\n    ", separator = ",\n    ", postfix = "\n")
            FunSpec.builder("parse$name")
                .addModifiers(KModifier.PRIVATE)
                .returns(cls.asType().asTypeName())
                .addStatement("return $name($params)")
                .addParameter(LUA_TABLE)
                .build()
                .let(rootObj::addFunction)
        }

        FunSpec.builder("getMapper")
            .addTypeVariable(T_ANY)
            .addParameter(CLS_T)
            .returns(LT_TO_T)
            .addModifiers(KModifier.PRIVATE)
            .addStatement(
"""return when (cls) {
    ${names.joinToString("\n    ") { "$it::class.java -> ::parse$it" }}
    else -> error("Type not supported ${"$"}cls")
} as (LuaTable) -> T""".trim())
            .build()
            .let(rootObj::addFunction)

        FunSpec.builder("tableToObject")
            .addTypeVariable(T_ANY)
            .addParameter(LUA_TABLE)
            .addParameter(CLS_T)
            .returns(T_ANY)
            .addStatement("return getMapper(cls)(t)")
            .build()
            .let(rootObj::addFunction)

        FunSpec.builder("tableToList")
            .addTypeVariable(T_ANY)
            .addParameter(LUA_TABLE)
            .addParameter(CLS_T)
            .returns(LIST_T)
            .addStatement("val mapper = getMapper(cls)")
            .addStatement("return t.keys().mapTo(ArrayList<T>(t.keyCount())) { key ->")
            .addStatement("    mapper(t.get(key).checktable()).also {")
            .addStatement("        if (it is ${HAS_TABLE_KEY.canonicalName}) it.key = key.checkint()")
            .addStatement("    }")
            .addStatement("}")
            .build()
            .let(rootObj::addFunction)

        FileSpec.get(pkg, rootObj.build())
            .toBuilder()
            .indent("    ")
            .build()
            .writeTo(out)
        return true
    }

    fun extractParamFromTable(param: VariableElement): String {
        val type = param.asType()
        val simpleName = type.asTypeName().toString()
        val nullable = param.getAnnotation(Nullable::class.java) != null
        val replaceKey = param.getAnnotation(KeyName::class.java)?.name
        val isColor = param.getAnnotation(Color::class.java) != null
        val name = "\"${replaceKey ?: param.simpleName}\""

        fun extractValue(transform: String): String = when {
            nullable -> "t.get($name).takeUnless { it.isnil() }?.$transform"
            else -> "t.get($name).$transform"
        }

//        log.appendText(type.asTypeName().toString())
//        log.appendText("\n")
        return when {
            simpleName.contains("java.util.List") -> {
                val listType = simpleName
                    .split("<")
                    .last()
                    .dropLast(1)
                    .replace("out ", "")
                    .let(elements::getTypeElement)
                    .simpleName
                    .toString()
                when (listType) {
                    "Integer" -> extractValue("checktable().ints()")
                    "String" -> extractValue("checktable().strings()")
                    else -> "tableToList(t.get($name).checktable(), $listType::class.java)"
                }
            }
            isColor -> extractValue("getColor()")
            else -> extractPrimitiveFromValue(type)?.let(::extractValue) ?: error("Invalid type $type")
        }
    }

    fun extractPrimitiveFromValue(param: TypeMirror): String? {
        val kind = param.kind
        val type = param.asTypeName().toString()
        return when {
            type == "java.lang.Integer" || kind == INT -> "checkint()"
            type == "java.lang.Boolean" || kind == BOOLEAN -> "checkboolean()"
            type == "java.lang.Double" || kind == DOUBLE -> "checkdouble()"
            type == "java.lang.Float" || kind == FLOAT -> "checkdouble().toFloat()"
            type == "java.lang.String" -> "checkjstring()"
            else -> null
        }
    }
}
