package kdbush

class FloatDeque(
    initialCapacity: Int = 2048
) {
    private var deque = FloatArray(initialCapacity)
    private var nextFree = 0
    fun getDeque() = deque
    fun getCount() = nextFree

    fun push(value: Float) {
        if (nextFree == deque.size) {
            val newDeque = FloatArray(nextFree * 2)
            System.arraycopy(deque, 0, newDeque, 0, nextFree)
            deque = newDeque
        }

        deque[nextFree++] = value
    }

    fun clear() {
        nextFree = 0
    }
}
