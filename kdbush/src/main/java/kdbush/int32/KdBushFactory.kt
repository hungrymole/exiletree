@file:Suppress("NOTHING_TO_INLINE")

package kdbush.int32

object KDBushIntFactory {

    var nodeSize = 64

    fun <T : PointInt> fromPoints(points: List<T>): KDBushInt<T> {
        val ids = IntArray(points.size)
        val coords = IntArray(points.size * 2)
        points.forEachIndexed { i, point ->
            ids[i] = i
            coords[i * 2] = point.x
            coords[i * 2 + 1] = point.y
        }
        sortKD(ids, coords, nodeSize, 0, ids.size - 1, 0)
        return KDBushInt(ids, coords, points, nodeSize)
    }
}

private fun sortKD(ids: IntArray,
                   coords: IntArray,
                   nodeSize: Int,
                   left: Int,
                   right: Int,
                   depth: Int) {
    if (right - left <= nodeSize) return

    val m = Math.floor((left + right) / 2.0).toInt()

    select(ids, coords, m, left, right, depth % 2)

    sortKD(ids, coords, nodeSize, left, m - 1, depth + 1)
    sortKD(ids, coords, nodeSize, m + 1, right, depth + 1)
}

private fun select(ids: IntArray,
                   coords: IntArray,
                   k: Int,
                   _left: Int,
                   _right: Int,
                   inc: Int) {
    var left = _left
    var right = _right

    while (right > left) {
        if (right - left > 600) {
            val n = right - left + 1
            val m = k - left + 1
            val z = Math.log(n.toDouble())
            val s = 0.5 * Math.exp(2 * z / 3)
            val sd = 0.5 * Math.sqrt(z * s * (n - s) / n) * (if (m - n / 2 < 0) -1 else 1)
            val newLeft = Math.max(left, Math.floor(k - m * s / n + sd).toInt())
            val newRight = Math.min(right, Math.floor(k + (n - m) * s / n + sd).toInt())
            select(ids, coords, k, newLeft, newRight, inc)
        }

        val t = coords[2 * k + inc]
        var i = left
        var j = right

        swapInts(ids, coords, left, k)
        if (coords[2 * right + inc] > t) swapInts(ids, coords, left, right)

        while (i < j) {
            swapInts(ids, coords, i, j)
            i++
            j--
            while (coords[2 * i + inc] < t) i++
            while (coords[2 * j + inc] > t) j--
        }

        if (coords[2 * left + inc] == t) swapInts(ids, coords, left, j)
        else {
            j++
            swapInts(ids, coords, j, right)
        }

        if (j <= k) left = j + 1
        if (k <= j) right = j - 1
    }
}

private inline fun swapInts(ids: IntArray,
                            coords: IntArray,
                            i: Int,
                            j: Int) {
    swapInt(ids, i, j)
    swapInt(coords, 2 * i, 2 * j)
    swapInt(coords, 2 * i + 1, 2 * j + 1)
}

private inline fun swapInt(arr: IntArray, i: Int, j: Int) {
    val tmp = arr[i]
    arr[i] = arr[j]
    arr[j] = tmp
}