@file:Suppress("NOTHING_TO_INLINE")

package kdbush.int32

import kdbush.IntDeque

class KDBushInt<T : PointInt> internal constructor(
    private val ids: IntArray,
    private val coords: IntArray,
    private val points: List<T>,
    private val nodeSize: Int
) {

    fun range(left: Int,
              top: Int,
              right: Int,
              bottom: Int): List<T> {
        val deque = rangeInternal(left, top, right, bottom)
        val end = deque.getCount()
        val arr = deque.getDeque()
        val result = ArrayList<T>(end)
        for (i in 0 until end) {
            result.add(points[arr[i]])
        }
        return result
    }

    /**
    private fun withinInternal(qx: Int,
                               qy: Int,
                               r: Int): IntArray {

        val stack = IntDeque().apply {
            push(0)
            push(ids.size - 1)
            push(0)
        }
        val result = IntDeque()
        val r2 = r * r

        while (stack.isNotEmpty) {
            val axis = stack.pop()
            val right = stack.pop()
            val left = stack.pop()

            if (right - left <= nodeSize) {
                for (i in left..right) {
                    if (sqDist(coords[2 * i], coords[2 * i + 1], qx, qy) <= r2) result.push(ids[i])
                }
                continue
            }

            val m = Math.floor((left + right) / 2.0).toInt()

            val x = coords[2 * m]
            val y = coords[2 * m + 1]

            if (sqDist(x, y, qx, qy) <= r2) result.push(ids[m])

            val nextAxis = (axis + 1) % 2

            if (axis == 0 && qx - r <= x || qy - r <= y) {
                stack.push(left)
                stack.push(m - 1)
                stack.push(nextAxis)
            }
            if (axis == 0 && qx + r >= x || qy + r >= y) {
                stack.push(m + 1)
                stack.push(right)
                stack.push(nextAxis)
            }
        }

        return result.elements
    }

    private inline fun sqDist(ax: Int,
                              ay: Int,
                              bx: Int,
                              by: Int): Int {
        val dx = ax - bx
        val dy = ay - by
        return dx * dx + dy * dy
    }
     */


    private val stack = IntDeque()
    private val result = IntDeque()
    private fun rangeInternal(minX: Int,
                              minY: Int,
                              maxX: Int,
                              maxY: Int): IntDeque {
        result.clear()
        stack.apply {
            clear()
            push(0)
            push(ids.size - 1)
            push(0)
        }

        var x: Int
        var y: Int

        while (stack.isNotEmpty) {
            val axis = stack.pop()
            val right = stack.pop()
            val left = stack.pop()

            if (right - left <= nodeSize) {
                for (i in left..right) {
                    x = coords[2 * i]
                    y = coords[2 * i + 1]
                    if (x >= minX && x <= maxX && y >= minY && y <= maxY) result.push(ids[i])
                }
                continue
            }

            val m = Math.floor((left + right) / 2.0).toInt()

            x = coords[2 * m]
            y = coords[2 * m + 1]

            if (x >= minX && x <= maxX && y >= minY && y <= maxY) result.push(ids[m])

            val nextAxis = (axis + 1) % 2

            if (axis == 0 && minX <= x || minY <= y) {
                stack.push(left)
                stack.push(m - 1)
                stack.push(nextAxis)
            }
            if (axis == 0 && maxX >= x || maxY >= y) {
                stack.push(m + 1)
                stack.push(right)
                stack.push(nextAxis)
            }
        }
        return result
    }
}
