package kdbush.int32

interface PointInt {
    val x: Int
    val y: Int
}