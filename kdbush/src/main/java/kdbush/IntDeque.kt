package kdbush

internal class IntDeque(
    initialCapacity: Int = 2048
) {
    private var deque = IntArray(initialCapacity)
    private var nextFree = 0

    val isNotEmpty: Boolean get() = nextFree != 0

    fun getDeque() = deque
    fun getCount() = nextFree

    fun push(value: Int) {
        if (nextFree == deque.size) {
            val newDeque = IntArray(nextFree * 2)
            System.arraycopy(deque, 0, newDeque, 0, nextFree)
            deque = newDeque
        }

        deque[nextFree++] = value
    }

    fun pop(): Int {
        return deque[--nextFree]
    }

    fun clear() {
        nextFree = 0
    }
}
