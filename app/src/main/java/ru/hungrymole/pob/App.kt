package ru.hungrymole.pob

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.core.CrashlyticsCore
import io.fabric.sdk.android.Fabric
import io.reactivex.Single
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import ru.hungrymole.pob.components.tree.model.tree.Graph
import ru.hungrymole.pob.debug.CrashlyticsTree
import ru.hungrymole.pob.di.DaggerMainComponent
import ru.hungrymole.pob.di.MainComponent
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.generator.POBColors
import ru.hungrymole.pob.lua.LuaModule
import ru.hungrymole.pob.lua.luaLogger
import timber.log.Timber
import javax.inject.Inject


/**
 * Created by khusainov.d on 30.05.2017.
 */

class App : Application() {

  @Inject lateinit var graph: Graph
  @Inject lateinit var loaders: LuaModule.Loaders
  @Inject lateinit var c: POBColors//todo

  companion object {
    val onLoaded = BehaviorSubject.create<Unit>()
  }

  lateinit var injector: MainComponent
  override fun onCreate() {
    super.onCreate()

    injector = DaggerMainComponent
      .builder()
      .ctx(this)
      .build()

    setupLogging()

    Single
      .fromCallable { injector.inject(this) }
      .luaSubscribe()
      .subscribeBy(
        onError = Timber::e,
        onSuccess = onLoaded::onNext
      )
  }

  private fun setupLogging() {
    when {
      BuildConfig.DEBUG -> Timber.plant(Timber.DebugTree())
      else              -> setupFabric()
    }

    luaLogger = { s: String ->
      var message = s
      if (BuildConfig.DEBUG) {
        message = "${Thread.currentThread().name} - $s"
      }
      Timber.tag("Lua").d(message)
    }
  }

  private fun setupFabric() {
    val core = CrashlyticsCore.Builder()
      .disabled(BuildConfig.DEBUG)
      .build()
    val kit = Crashlytics.Builder()
      .core(core)
      .answers(Answers())
      .build()
    Fabric.with(this, kit)

    Timber.plant(CrashlyticsTree(core))
  }
}