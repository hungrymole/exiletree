package ru.hungrymole.pob.lua.interloop.loaders

import android.content.Context
import android.support.annotation.Keep
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.Constants

/**
 * Created by khusainov.d on 27.09.2017.
 */
@Keep
class MinionLoader(
    items: List<Loadable>,
    ctx: Context,
    lua: Globals,
    private val mod: LuaValue
) : Loader<Loadable>(items, ctx, lua) {

    override val src = "Data/${Constants.VERSION}"
    override val cacheExpirationSeconds: Int get() = 30

    private val prefix = "mod=...return "


    private val loadPermanently = setOf(
        "SummonedIceGolem",
        "SummonedLightningGolem",
        "SummonedChaosGolem",
        "SummonedStoneGolem",
        "SummonedFlameGolem",
        "SummonedPhantasm"
    )
    private val permCache = mutableMapOf<String, LuaValue>()

    override fun loadTable(name: String): LuaValue {
        return when {
            name in loadPermanently -> permCache.getOrPut(name) { loadMinion(name) }
            else                    -> loadMinion(name)
        }
    }

    private fun loadMinion(name: String): LuaValue {
        val entity = itemsMap[name]
        val raw = entity?.read() ?: return nil
        val table = lua.load("$prefix$raw").call(mod)
        return table
    }
}