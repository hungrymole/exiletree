package ru.hungrymole.pob.lua.interloop

import android.support.annotation.Keep
import okhttp3.OkHttpClient
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.lua.call
import ru.hungrymole.pob.lua.component1
import ru.hungrymole.pob.lua.component2
import ru.hungrymole.pob.lua.component3
import ru.hungrymole.pob.network.HttpException
import ru.hungrymole.pob.network.NetworkModule
import ru.hungrymole.pob.network.get
import java.io.IOException
import javax.inject.Inject
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Dinar Khusainov on 03.02.2018.
 */
@Keep
@Singleton
class HttpDelegate @Inject constructor(
    @Named(NetworkModule.OKHTTP_DEFAULT) private val client: OkHttpClient
) {

    private val nil = LuaValue.NIL

    fun DownloadPage(input: LuaTable) {
        val (url, callback, cookies) = input
        try {
            val response = client
                .get(url.checkjstring(), "Cookie" to (cookies?.optjstring("") ?: ""))
                .blockingGet()
            val body = response.body()?.string()?.let(LuaValue::valueOf)
            val errorMsg = "Response code: ${response.code()}"
            when {
                response.isSuccessful && body != null -> callback.call(body)
                else                                  -> callback.call(nil, errorMsg)
            }
            response.close()
        } catch (e: Throwable) {
            val actual = e.cause//Rx2 wraps exception
            when (actual) {
                is HttpException -> callback.call(nil, "Response code: ${actual.code}")
                is IOException   -> callback.call(nil, "Network error")
                else             -> throw e
            }
        }
    }
}