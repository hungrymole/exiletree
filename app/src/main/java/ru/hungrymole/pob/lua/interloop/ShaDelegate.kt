package ru.hungrymole.pob.lua.interloop

import android.support.annotation.Keep
import org.luaj.vm2.LuaString
import java.security.MessageDigest

@Keep
object ShaDelegate {

  fun sha1(input: String): LuaString =
    MessageDigest
      .getInstance("SHA-1")
      .also { it.update(input.toByteArray(), 0, input.length) }
      .digest()
      ?.contentToString()
      ?.let(LuaString::valueOf)
      ?: error("SHA-1 error")
}