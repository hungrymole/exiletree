package ru.hungrymole.pob.lua.interloop.loaders

import ru.hungrymole.pob.generator.ProtoLoadable
import ru.hungrymole.pob.generator.lua.LoadableMod
import ru.hungrymole.pob.generator.lua.LoadableSkill

/**
 * Created by khusainov.d on 14.09.2017.
 */
open class Loadable(val start: Int,
                    val length: Int,
                    var file: String,
                    val name: String) {

    constructor(data: ProtoLoadable) : this(data.start, data.length, data.file, data.name)

    override fun toString() = name

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is Loadable) return false

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}

class BaseInfo(data: ProtoLoadable) : Loadable(data)

class SkillInfo(data: LoadableSkill) : Loadable(data.start, data.length, data.file, data.name) {
    val nameSpec: String = data.nameSpec//actual name (Arctic Breath, Cospri's Malice)
    val color: Int? = data.color
}

class ModInfo(
    file: String,
    name: String,
    data: LoadableMod
) : Loadable(data.start, data.length, file, name) {
    val lineHash: Int = data.lineHash
}