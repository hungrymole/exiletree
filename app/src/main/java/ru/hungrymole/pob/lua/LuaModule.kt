package ru.hungrymole.pob.lua

import android.content.Context
import dagger.Module
import dagger.Provides
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaTable
import org.luaj.vm2.lib.ResourceFinder
import org.luaj.vm2.lib.jse.CoerceJavaToLua
import org.luaj.vm2.lib.jse.JsePlatform
import ru.hungrymole.pob.Constants
import ru.hungrymole.pob.components.loadGenerated
import ru.hungrymole.pob.components.skills.group.search.GemRepository
import ru.hungrymole.pob.components.skills.group.search.LuaGemRepository
import ru.hungrymole.pob.extensions.measure
import ru.hungrymole.pob.generator.GeneratedLoaders
import ru.hungrymole.pob.generator.GeneratedLuaTree
import ru.hungrymole.pob.generator.POBColors
import ru.hungrymole.pob.generator.loadPobEnviroment
import ru.hungrymole.pob.lua.interloop.*
import ru.hungrymole.pob.lua.interloop.loaders.*
import ru.hungrymole.pob.lua.serialization.LuaTableSerializer
import javax.inject.Singleton

/**
 * Created by khusainov.d on 19.09.2017.
 */
@Module
class LuaModule {

    @Provides
    @Singleton
    fun lua(ctx: Context,
            http: HttpDelegate,
            eventBus: EventBusDelegate): Globals = measure("AndroidMain") {
        return JsePlatform.standardGlobals().also {
            it.finder = ResourceFinder(ctx.assets::open)
            loadLua(ctx, it, http, eventBus)
        }
    }

    private fun loadLua(ctx: Context,
                        lua: Globals,
                        http: HttpDelegate,
                        eventBus: EventBusDelegate) {
        //load main env
        measure("LoadLuaCompile") {
            lua.loadPobEnviroment(generator = false)
        }

        //load tree first
        val luaTree = measure("Deserialize lua tree") {
            GeneratedLuaTree
              .loadGenerated(ctx)
              .let { LuaTableSerializer.deserializeTable(it, lua) }
        }

        lua["main"]["tree"].set(Constants.VERSION, luaTree)

        //setup android interloop
        measure("AndroidMain") {
          lua.loadfile("Android/AndroidMain.lua").call()
        }
        lua["itemsTabDelegate"] = CoerceJavaToLua.coerce(ItemsTabDelegate())
        lua["httpDelegate"] = CoerceJavaToLua.coerce(http)
        lua["eventBusDelegate"] = CoerceJavaToLua.coerce(eventBus)
        lua["debugDelegate"] = CoerceJavaToLua.coerce(DebugDelegate)
        lua["sha1Delegate"] = CoerceJavaToLua.coerce(ShaDelegate)
    }

    @Provides
    @Singleton
    fun colors(lua: Globals): POBColors = lua.pobColors()

    class Loaders(
        val base: BaseLoader,
        val minions: MinionLoader,
        val mod: ModLoader,
        val skill: SkillLoader
    )

    @Provides
    @Singleton
    fun loaders(ctx: Context,
                lua: Globals): Loaders = measure("Loaders") {
        val data = GeneratedLoaders.loadGenerated(ctx)
        val name = ""
        val file = "ModCache.lua"

        val skilLoader = SkillLoader(data.skills.map(::SkillInfo), ctx, lua)
        val loaders =  Loaders(
            BaseLoader(data.bases.map(::BaseInfo), ctx, lua),
            MinionLoader(data.minions.map(::Loadable), ctx, lua, skilLoader.mod),
            ModLoader(data.mods.map { ModInfo(file, name, it) }, ctx, lua),
            skilLoader
        )
        val loadersTable = LuaTable().apply {
            set("mod", CoerceJavaToLua.coerce(loaders.mod))
            set("skill", CoerceJavaToLua.coerce(loaders.skill))
            set("base", CoerceJavaToLua.coerce(loaders.base))
            set("minions", CoerceJavaToLua.coerce(loaders.minions))
        }
        lua["setupLoaders"].call(loadersTable)

        loaders
    }

    @Provides
    @Singleton
    fun skillRepo(impl: LuaGemRepository): GemRepository = impl

    @Provides @Singleton fun base(loaders: Loaders) = loaders.base
    @Provides @Singleton fun mod(loaders: Loaders) = loaders.mod
    @Provides @Singleton fun skill(loaders: Loaders) = loaders.skill
}