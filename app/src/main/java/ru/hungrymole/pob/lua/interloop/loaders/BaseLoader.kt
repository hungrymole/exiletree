package ru.hungrymole.pob.lua.interloop.loaders

import android.content.Context
import android.support.annotation.Keep
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.Constants

/**
 * Created by khusainov.d on 27.09.2017.
 */
@Keep
class BaseLoader(
    items: List<BaseInfo>,
    ctx: Context,
    lua: Globals
) : Loader<BaseInfo>(items, ctx, lua) {

    override val src = "Data/${Constants.VERSION}/Bases"
    override val cacheExpirationSeconds: Int get() = 5

    fun all() = all
    private val all by lazy {
        LuaTable().apply {
            items.forEach {
                LuaValue.valueOf(it.name).let { set(it, it) }
            }
        }
    }
}