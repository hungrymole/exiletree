package ru.hungrymole.pob.lua.interloop

import android.support.annotation.Keep
import ru.hungrymole.pob.build.toolbar.progress.ProgressEvent
import ru.hungrymole.pob.components.EventBus
import javax.inject.Inject
import javax.inject.Singleton

@Keep
@Singleton
class EventBusDelegate @Inject constructor(
    private val eventBus: EventBus
) {

    fun progressEvent(progress: Double) {
        eventBus.onNext(ProgressEvent(progress.toInt()))
    }
}