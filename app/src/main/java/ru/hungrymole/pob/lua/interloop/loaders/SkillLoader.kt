package ru.hungrymole.pob.lua.interloop.loaders

import android.content.Context
import android.support.annotation.Keep
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.Constants
import ru.hungrymole.pob.extensions.fileAsString
import ru.hungrymole.pob.extensions.getOrCreate
import ru.hungrymole.pob.lua.call
import ru.hungrymole.pob.lua.toMap

/**
 * Created by khusainov.d on 15.09.2017.
 */
@Keep
class SkillLoader(
    items: List<SkillInfo>,
    ctx: Context,
    lua: Globals
) : Loader<SkillInfo>(items, ctx, lua) {

    override val src = "Data/${Constants.VERSION}/Skills"

    private val prefix = "mod,flag,skill=...return "
    val mod: LuaValue
    private val flag: LuaValue
    private val skill: LuaValue

    override val cacheExpirationSeconds: Int get() = 3 * 60

    private val loadPermanently = setOf(
        "Melee",
        "IceElementalIceCyclone",
        "IceElementalSpearSummoned",
        "LightningGolemArcSummoned",
        "MonsterProjectileSpellLightningGolemSummoned",
        "LightningGolemWrath",
        "ChaosElementalCascadeSummoned",
        "SandstormChaosElementalSummoned",
        "RockGolemSlam",
        "RockGolemWhirlingBlades",
        "FireElementalFlameRedSummoned",
        "FireElementalMortarSummoned",
        "FireElementalConeSummoned"
    )
    private val permCache = mutableMapOf<String, LuaValue>()

    private val dataTable = lua["data"][Constants.VERSION]
    private val gemTable = dataTable["gems"].checktable()
    private val skillTable = dataTable["skills"].checktable()
    private val gemsNames = gemTable.toMap().map { it.value["name"].checkjstring() }

    val searchable = items.filter { it.nameSpec in gemsNames }
    private val gems = searchable.associateBy { it.nameSpec }

    init {
        val f = ctx.assets.fileAsString("skillLoaderFunc.lua")
            .let(lua::load)
            .call()
        mod = f["mod"]
        flag = f["flag"]
        skill = f["skill"]
    }

    override fun get(key: String?): LuaValue {
        if (key == null) return LuaValue.NIL
        return super.get(gems[key]?.name ?: key)
    }

    override fun loadTable(name: String): LuaValue {
        val gemSkill = skillTable.rawget(name)
        return when {
            name in loadPermanently -> permCache.getOrCreate(name, ::loadSkill)
            !gemSkill.isnil()       -> gemSkill
            else                    -> loadSkill(name)
        }
    }

    private fun loadSkill(name: String): LuaValue {
        val entity = itemsMap[name]
        val raw = entity?.read() ?: return nil
        val table = lua.load("$prefix$raw").call(mod, flag, skill)
        lua.get("processLoadedSkill").call(entity.name, table)
        return table
    }
}