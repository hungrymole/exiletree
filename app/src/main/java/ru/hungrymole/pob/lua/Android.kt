package ru.hungrymole.pob.lua

import io.reactivex.internal.schedulers.NonBlockingThread
import io.reactivex.internal.schedulers.SingleScheduler
import org.luaj.vm2.LuajOptimizations
import ru.hungrymole.pob.extensions.logD
import java.util.concurrent.ThreadFactory

/**
 * Created by Dinar Khusainov on 18.01.2018.
 */
val luaScheduler = SingleScheduler(CustomThreadSingleFactory())

//mirror RxThreadFactory
private class CustomThreadSingleFactory : ThreadFactory {
  override fun newThread(r: Runnable?): Thread {
    val p = "rx2.single-priority"//SingleScheduler.KEY_SINGLE_PRIORITY
    val priority = Math.max(Thread.MIN_PRIORITY, Math.min(Thread.MAX_PRIORITY, Integer.getInteger(p, Thread.NORM_PRIORITY)!!))
    val group = ThreadGroup("LuaThreadGroup")
    val t = object : Thread(group, r, "LuaThread"), NonBlockingThread {}
    LuajOptimizations.luaThread = t
    t.priority = priority
    t.isDaemon = true
    return t
  }
}