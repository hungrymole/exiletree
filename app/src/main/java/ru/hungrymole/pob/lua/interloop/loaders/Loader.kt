package ru.hungrymole.pob.lua.interloop.loaders

import android.content.Context
import net.jodah.expiringmap.ExpirationPolicy
import net.jodah.expiringmap.ExpiringMap
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.BuildConfig
import ru.hungrymole.pob.common.utf8String
import ru.hungrymole.pob.extensions.fileAsString
import ru.hungrymole.pob.extensions.measure
import java.io.File
import java.io.RandomAccessFile
import java.util.concurrent.TimeUnit


/**
 * Created by khusainov.d on 15.09.2017.
 */
abstract class Loader<T : Loadable>(items: List<T>,
                                    val ctx: Context,
                                    val lua: Globals) {

   companion object {
       @JvmStatic protected val nil = LuaValue.NIL
       private const val prefix = "return "
       const val LOADER_FILES_DIR = "loaders"
   }

    protected abstract val src: String
    protected open val cacheExpirationSeconds: Int = 40
    protected open val log: Boolean = BuildConfig.DEBUG

    protected val version by lazy { lua["liveTargetVersion"].checkjstring() }
    protected val itemsMap = mutableMapOf<String, T>()
    val items by lazy { itemsMap.values }

    protected val cache: ExpiringMap<String, LuaValue> = ExpiringMap.builder()
        .expiration(cacheExpirationSeconds.toLong(), TimeUnit.SECONDS)
        .expirationPolicy(ExpirationPolicy.ACCESSED)
        .entryLoader<String, LuaValue>(::loadTable)
        .build<String, LuaValue>()

    init {
        val files = mutableSetOf<String>()
        items.forEach { entity ->
            //deduplicate strings
            files += entity.file
            entity.file = files.first { it == entity.file }
            itemsMap[entity.name] = entity
        }
    }

    open fun get(key: String?): LuaValue {
        if (key == null) return LuaValue.NIL
        return cache[key] ?: error("Failed to lazy load loadable '$key' ")
    }

    open fun load(name: String): String? = itemsMap[name]?.read()
    open fun loadTable(name: String): LuaValue = load(name).toTable()

    protected fun String?.toTable(): LuaValue {
        this ?: return nil
        return lua.load("$prefix$this").call()
    }

    protected fun T.read() = getOrSetupBaseFile(file).use { raf ->
        if (log) {
            measure("${this@Loader::class.java.simpleName} - $name") {
                raf.read(this)
            }
        } else {
            raf.read(this)
        }
    }

    open fun clear() {
        cache.clear()
    }

    private fun RandomAccessFile.read(entity: T): String {
        seek(entity.start.toLong())
        return ByteArray(entity.length)
            .also { read(it) }
            .utf8String()
    }

    //lazy copy files to internal dir for RandomAccessFile
    private val dir =
      ctx
        .packageManager
        .getPackageInfo(ctx.packageName, 0)
        .versionName
        .let { "/$LOADER_FILES_DIR/$it/" }
        .let { File(ctx.cacheDir, it) }
        .apply { mkdirs() }

    private val files = mutableMapOf<String, File>()
    private fun getOrSetupBaseFile(filename: String): RandomAccessFile {
      var file = files[filename]
      if (file == null || !file.exists()) {
        file = File(dir, filename)
        ctx
          .assets
          .fileAsString("$src/$filename")
          .let { file.writeText(it) }
      }
      return RandomAccessFile(file, "r")
    }
}