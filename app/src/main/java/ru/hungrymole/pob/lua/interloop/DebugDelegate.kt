package ru.hungrymole.pob.lua.interloop

import android.support.annotation.Keep
import org.luaj.vm2.LuaValue

@Keep
object DebugDelegate {

    fun d0() = Unit
    fun d1(v1: LuaValue) = Unit
    fun d2(v1: LuaValue, v2: LuaValue) = Unit
    fun d3(v1: LuaValue, v2: LuaValue, v3: LuaValue) = Unit
}