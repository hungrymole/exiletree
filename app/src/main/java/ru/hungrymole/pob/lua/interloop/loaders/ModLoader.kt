package ru.hungrymole.pob.lua.interloop.loaders

import android.content.Context
import android.support.annotation.Keep
import android.util.SparseArray
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.Constants
import ru.hungrymole.pob.lua.clear
import ru.hungrymole.pob.lua.getBy

/**
 * Created by khusainov.d on 20.09.2017.
 */

@Keep
class ModLoader(
    items: List<ModInfo>,
    ctx: Context,
    lua: Globals
) : Loader<ModInfo>(items, ctx, lua) {

    override val src = "Data/${Constants.VERSION}"
    override val log = false

    val map = SparseArray<ModInfo>(items.size).apply {
        items.forEach { append(it.lineHash, it) }
    }

    //called from ModParser
    fun loadByLine(modLine: String): LuaValue {
        val mod = map[modLine.hashCode()]//hack to fix lower uppsre/lower diff in this type of mod
            ?: map[modLine.replace("Grants level", "Grants Level").hashCode()]
            ?: map[modLine.replace("Grants Level", "Grants level").hashCode()]
        return mod?.read().toTable()
    }

    override fun loadTable(name: String): LuaValue {
        error("Mods cached in lua ModParser")
    }

    override fun clear() {
        super.clear()
        lua.getBy("modLib.parseModCache.$version").clear()
    }

}