package ru.hungrymole.pob.lua.interloop

import android.support.annotation.Keep
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.POEData
import ru.hungrymole.pob.lua.*
import timber.log.Timber

/**
 * Created by khusainov.d on 28.09.2017.
 */
@Keep
@Deprecated("Too much work")
class ItemsTabDelegate {

    val weaponItems = listOf("Weapon", "Bow", "Quiver", "Shield")
    val weaponSlots = listOf(
            LuaValue.valueOf("Weapon 1"),
            LuaValue.valueOf("Weapon 2"),
            LuaValue.valueOf("Weapon 1 Swap"),
            LuaValue.valueOf("Weapon 2 Swap")
    )
    val weapons = listOf("Weapon 1","Weapon 2")
    val flasks = listOf("Flask 1", "Flask 2", "Flask 3", "Flask 4", "Flask 5")
    val rings = listOf("Ring 1","Ring 2")

    val oneToOne = listOf(
            "Helmet",
            "Body Armour",
            "Gloves",
            "Boots",
            "Amulet",
            "Belt")

    fun PopulateSlots(itemsTab: LuaValue): Unit {
        itemsTab.selfCall("UpdateSockets")
        val slots = itemsTab["slots"].entries()

        val activeSockets = itemsTab["sockets"]
            .values()
            .filter { !it["inactive"].checkboolean() }
            .map { it["slotName"].checkjstring() }

        val activeAbyssalSockets = slots
            .filterKeys { it.checkjstring().contains("Abyssal Socket") }
            .filter { it.value["inactive"].checkboolean() }
            .map { it.value["slotName"].checkjstring() }

        val itemToSlots = itemsTab["items"].entries().map { (_, item) ->
            val type = item["type"].checkjstring()
            val baseName = item["baseName"].checkjstring()
            item to when {
                type in oneToOne -> listOf(type)
                type[0] == 'F' -> flasks
                type[0] == 'R' -> rings
                type in weaponItems || !item["weaponData"].isnil() -> weaponSlots
                        .filter { itemsTab.isItemValidForSlot(item, it) }
                        .map { it.checkjstring() }
                baseName in POEData.abyssJewelTypes -> activeSockets + activeAbyssalSockets
                type == "Jewel" -> activeSockets
                else -> emptyList()
            }
        }.toMap()
        slots.forEach { (_, slot) ->
            val slotName = slot["slotName"].checkjstring()
            val newSlotItems = itemToSlots.filter { slotName in it.value }.keys
            val newSlotItemsIds = newSlotItems.map { it["id"].checkint() }
            val currentSlotItemsIds = slot["items"].ints().drop(1)
            val delete = currentSlotItemsIds - newSlotItemsIds
            val add = newSlotItemsIds - currentSlotItemsIds
            slot["items"].onEach { k, v ->
                if (v.checkint() in delete) {
                    slot["items"][k] = LuaValue.NIL
                    slot["list"][k] = LuaValue.NIL
                }
                newSlotItems
                        .filter { it["id"].checkint() in add }
                        .forEach { slot.selfCall("AddItem", it) }
            }
            if (slot["abyssalSocketList"].checktable().keyCount() != 0) {
                slot.selfCall("ResolveAbyssJewels")
            }
        }
    }

    fun LuaValue.isItemValidForSlot(item: LuaValue, slot: LuaValue): Boolean {
        return try {
            return selfCall("IsItemValidForSlot", item, slot).checkboolean()
        } catch (e: Throwable) {
            Timber.i("IsItemValidForSlot: name=${item["name"].checkjstring()}")
            false
        }
    }
}