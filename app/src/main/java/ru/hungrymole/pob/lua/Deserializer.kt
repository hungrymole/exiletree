package ru.hungrymole.pob.lua

import org.luaj.vm2.LuaValue

/**
 * Created by Dinar Khusainov on 18.01.2018.
 */
inline fun <reified T : Any> LuaValue.toObject(): T = try {
    LuaDeserializer.tableToObject(this.checktable(), T::class.java)
} catch (e: Exception) {
    val info = checktable().keys().joinToString { it.toString() }
    throw RuntimeException("Failed to deserialize table: $info", e)
}

inline fun <reified T> LuaValue.toList(): List<T> {
    return LuaDeserializer.tableToList(checktable(), T::class.java)
}