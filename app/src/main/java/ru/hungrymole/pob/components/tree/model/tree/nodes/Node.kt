package ru.hungrymole.pob.components.tree.model.tree.nodes

import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 29.12.14.
 */
open class Node(data: TreeNodeData) : AbstractNode(data)
