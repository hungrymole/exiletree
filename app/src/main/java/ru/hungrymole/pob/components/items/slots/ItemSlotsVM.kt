package ru.hungrymole.pob.components.items.slots

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaInteger
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.build.BuildRefreshEvent
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.*
import ru.hungrymole.pob.ui.list.ListViewModel
import javax.inject.Inject

/**
 * Created by khusainov.d on 11.08.2017.
 */
class ItemSlotsVM @Inject constructor(
  private val lua: Globals,
  private val eventBus: EventBus,
  ctx: Context
) : ListViewModel<ItemSlot>(ctx) {

    private val swapEnabled get() = lua.getBy("build.itemsTab.activeItemSet.useSecondWeaponSet").optboolean(false)

    val flaskEnabledChanges = PublishSubject.create<Pair<String, Boolean>>()
    val weaponSwap = PublishSubject.create<Unit>()
    val slotChanges = PublishSubject.create<Pair<String, Int>>()

    override fun getState() = Single.fromCallable {
        val swapEnabled = swapEnabled
        lua.getBy("build.itemsTab").selfCall("UpdateSockets")
        lua.getBy("build.itemsTab.orderedSlots").arrayValues()
                .filterNot {
                    val name = it["slotName"].checkjstring()
                    val label = it["label"].checkjstring()
                    val inactiveJewel = label == "Socket" || label.startsWith("Abyssal") && it["inactive"].checkboolean()
                    inactiveJewel ||
                            !swapEnabled && name.contains("Swap") ||
                            swapEnabled && name in listOf("Weapon 1", "Weapon 2")

                }
                .map { it.toObject<ItemSlot>() }
    }.luaSubscribe()

    override fun subscribeOnStart(subscriptions: CompositeDisposable) {
        subscriptions += flaskEnabledChanges.lua().doOnNext { (slotName, activated) ->
            lua.getBy("build.itemsTab.slots.$slotName.controls.activate.changeFunc").call(activated)
        }.subscribe()

        subscriptions += slotChanges.lua().doOnNext { (slotName, i) ->
            lua.getBy("build.itemsTab.slots.$slotName.selFunc").call(i + 1)
        }.subscribe()

        subscriptions += weaponSwap.lua().doOnNext {
            lua.getBy("build.itemsTab.controls.weaponSwap${if (swapEnabled) "1" else "2"}.onClick()")
        }.subscribe()

        subscriptions += eventBus
            .ofType<BuildRefreshEvent>()
            .lua()
            .flatMapSingle { reloadState() }
            .subscribe()
    }

  fun getSlotItemTooltip(slot: ItemSlot, pos: Int) = Single.fromCallable<Tooltip> {
    val luaTooltip = lua.getBy("new").call("Tooltip")
    lua
      .getBy("build.itemsTab.slots.${slot.slotName}.selFunc")
      .call(luaTooltip, LuaValue.NIL, LuaInteger.valueOf(pos))
    Tooltip(luaTooltip)
  }.luaSubscribe()
}