package ru.hungrymole.pob.components.skills

import ru.hungrymole.pob.lua.Color
import ru.hungrymole.pob.lua.HasTableKey
import ru.hungrymole.pob.lua.Table
import ru.hungrymole.pob.lua.Tooltip

/**
 * Created by khusainov.d on 18.07.2017.
 */

@Table
class GroupItemSlot(val label: String, val slotName: String?) {
    override fun toString() = label
}

@Table
class SearchableSkill(val name: String,//Arctic Breath
                      val gemId: String,//Metadata/Items/Gems/SkillGemArcticBreath
                      @Color val color: Int,
                      @Color val signColor: Int = 0,
                      val okSign: Boolean = false,
                      val plusSign: Boolean = false) : HasTableKey() {
    override fun toString() = name

    var tooltip: Tooltip? = null
}

@Table
class SkillGroup(var label: String?,
                 val displayLabel: String?,
                 val source: String?,//skills from items
                 var slot: String?, //slotName
                 var enabled: Boolean,
                 val gemList: List<Skill>) : HasTableKey() {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        other as SkillGroup

        if (displayLabel != other.displayLabel) return false
        if (key != other.key) return false

        return true
    }

    override fun hashCode(): Int {
        var result = displayLabel?.hashCode() ?: 0
        result = 31 * result + key.hashCode()
        return result
    }
}

@Table
class Skill(var nameSpec: String, //name Explosive Arrow
            @Color var color: Int,
            var level: Int,
            var quality: Int,
            var enabled: Boolean) : HasTableKey() {

    override fun toString() = nameSpec
    override fun equals(other: Any?): Boolean {
        if (this === other) return true

        other as Skill

        if (nameSpec != other.nameSpec) return false
        if (key != other.key) return false

        return true
    }

    override fun hashCode(): Int {
        var result = nameSpec.hashCode()
        result = 31 * result + key.hashCode()
        return result
    }
}