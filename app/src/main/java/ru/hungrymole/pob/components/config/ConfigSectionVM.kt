package ru.hungrymole.pob.components.config

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import org.luaj.vm2.Globals
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.call
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.lua.selfCall
import ru.hungrymole.pob.lua.toObject
import ru.hungrymole.pob.ui.list.ListViewModel
import javax.inject.Inject

/**
 * Created by khusainov.d on 07.08.2017.
 */
class ConfigSectionVM @Inject constructor(
  private val lua: Globals,
  ctx: Context
) : ListViewModel<ConfigItem>(ctx) {

    lateinit var section: String

    val spinnerChanges = PublishSubject.create<Pair<String, Int>>()
    val checkboxChanges = PublishSubject.create<Pair<String, Boolean>>()
    val  numberChanges = PublishSubject.create<Pair<String, Int>>()

    val sectionItems get() =
        lua.getBy("build.configTab")
                .selfCall("GetSectionConfigs", section)
                .toObject<SectionItems>()
                .run { checkboxes + spinners + numbers + labels }
                .sortedBy { it.key }

    override fun getState() = Single.fromCallable {
        val values = lua.getBy("build.configTab.input").checktable()
        sectionItems.onEach { config ->
            if (config !is ConfigItemWithValue<*>) return@onEach
            val value = values[config.id]
            when (config) {
                is ConfigItemCheckbox -> config.value = value.takeIf { it.isboolean() }?.checkboolean() ?: false
                is ConfigItemNumber -> config.value = value.takeIf { it.isint() }?.checkint() ?: 0
                is ConfigItemSpinner -> {
                    config.value = when {
                        value.isint() -> config.list.first { it.value == value.tostring().checkjstring() }
                        value.isstring() -> config.list.first { it.value == value.checkjstring() }
                        else -> config.list.first()
                    }
                }
                else -> {}
            }
        }
    }.luaSubscribe()

    override fun subscribeOnStart(subscriptions: CompositeDisposable) {
        subscriptions += checkboxChanges.lua().doOnNext { (id, value) ->
            lua.getBy("build.configTab.varControls.$id.changeFunc").call(value)
        }.subscribe()

        subscriptions += spinnerChanges.lua().doOnNext { (id, i)  ->
            lua.getBy("build.configTab.varControls.$id").selfCall("SetSel", i + 1)
        }.subscribe()

        subscriptions += numberChanges.lua().doOnNext { (id, value)  ->
            lua.getBy("build.configTab.varControls.$id").selfCall("SetText", value.toString(), true)
        }.subscribe()
    }

    fun getConfigType(item: ConfigItem) = when (item) {
        is ConfigItemCheckbox -> CONFIG_CHECKBOX
        is ConfigItemSpinner -> CONFIG_SPINNER
        is ConfigItemNumber -> CONFIG_NUMBER
        else -> CONFIG_EMPTY
    }
}