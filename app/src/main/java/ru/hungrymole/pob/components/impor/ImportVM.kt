package ru.hungrymole.pob.components.impor

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import okhttp3.OkHttpClient
import org.luaj.vm2.Globals
import org.luaj.vm2.lib.jse.CoerceJavaToLua
import ru.hungrymole.pob.R
import ru.hungrymole.pob.common.substring
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.dialogs.BaseConfigEntry
import ru.hungrymole.pob.dialogs.ConfigResult
import ru.hungrymole.pob.dialogs.TextConfigEntry
import ru.hungrymole.pob.dialogs.byLabel
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.Tooltip.Companion.COLOR_REGEX
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.lua.luaValue
import ru.hungrymole.pob.lua.selfCall
import ru.hungrymole.pob.network.NetworkModule.Companion.OKHTTP_POE
import ru.hungrymole.pob.network.PoeCookieJar
import ru.hungrymole.pob.network.get
import ru.hungrymole.pob.network.postBody
import ru.hungrymole.pob.ui.mvvm.ViewModel
import javax.inject.Inject
import javax.inject.Named

/**
 * Created by Dinar Khusainov on 03.02.2018.
 */
class ImportVM @Inject constructor(
  ctx: Context,
  private val lua: Globals,
  private val poeCookies: PoeCookieJar,
  private val eventBus: EventBus,
  @Named(OKHTTP_POE) private var poeClient: OkHttpClient
) : ViewModel<ImportState>(ctx) {

    val startImport = PublishSubject.create<SubmitImport>()

    private val loading = listOf("IMPORTING", "DOWNLOADCHARLIST")
    override fun getState() = Single.fromCallable {
        val mode = lua.getBy("build.importTab.charImportMode").checkjstring()
        val accountName = lua.getBy("build.importTab.controls.accountName.buf").optjstring("")
        val status = lua
            .getBy("build.importTab.charImportStatus")
            .checkjstring()
            .replace(COLOR_REGEX.toRegex(), "")

        val needAuth = mode == "GETSESSIONID"
        if (needAuth) {
            setSession(accountName, null)
        }
        return@fromCallable ImportState(
            accountName = accountName,
            status = status,
            loading = mode in loading,
            needAuth = needAuth
        )
    }.luaSubscribe()

    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {
      subscriptions += Single
        .fromCallable {
          val importTab = lua.getBy("build.importTab")
          importTab["ImportEvent"] = { eventBus.onNext(ImportEvent) }.let(CoerceJavaToLua::coerce)
        }
        .luaSubscribe()
        .subscribe()

        subscriptions += startImport
            .lua()
            .subscribe { (accountName, session) ->
                val actualSession = when {
                    session == null -> getSession(accountName).luaValue()
                    else -> {
                        setSession(accountName, session)
                        session.luaValue()
                    }
                }
                lua.getBy("build.importTab.controls.accountName").selfCall("SetText", accountName)
                lua.getBy("build.importTab.controls.sessionInput").selfCall("SetText", actualSession)
                lua.getBy("build.importTab.controls.accountNameGo.onClick()")
            }

        subscriptions += eventBus
            .ofType<ImportEvent>()
            .flatMapSingle { reloadState() }
            .subscribe()

    }

    fun setInput(input: ImportInput) {
        if (input.accountName != null) {
            startImport.onNext(SubmitImport(input.accountName, null))
        }
    }


    fun getConfigs(): List<BaseConfigEntry> {
        return listOf(
            TextConfigEntry("login"),
            TextConfigEntry("password")
        )
    }

    fun applyConfig(r: ConfigResult): Single<String> {
        poeCookies.clear()
        val login = r.byLabel<TextConfigEntry>("login").text
        val password = r.byLabel<TextConfigEntry>("password").text
        if (login.isBlank() && password.isBlank()) return Single.just("Empty input")

        val loginUrl = ctx.getString(R.string.poe_login_url)
        val loginRequest = poeClient
            .get(loginUrl)
            .map {
                it.body()
                    ?.string()
                    ?.substring(after = "name=\"hash\" value=\"", until = "\"")
                    ?: ""
            }
            .flatMap { hash ->
                poeClient.postBody(
                    loginUrl,
                    "login_email" to login,
                    "login_password" to password,
                    "hash" to hash,
                    "remember_me" to 0,
                    "login" to "Login"
                )
            }

        return reloadState()
            .doOnSubscribe { lua.getBy("build.importTab")["charImportMode"] = "IMPORTING" }
            .flatMap { loginRequest }
            .map {
                val session = poeCookies
                    .getPOESESSIDs()
                    .getOrNull(1)
                when {
                    it.code() == 200 && session != null -> {
                        val accountName = lua.getBy("build.importTab.controls.accountName.buf").optjstring("")
                        startImport.onNext(SubmitImport(accountName, session))
                        "Login successful"
                    }
                    else                                -> "Login failed"
                }
            }
    }

    override fun subscribeOnStart(subscriptions: CompositeDisposable) {}

    private val sessionStorage = ctx.getSharedPreferences("poe_sessiosn", Context.MODE_PRIVATE)
    private fun setSession(accountName: String, session: String?) {
        sessionStorage.edit().putString(accountName, session).apply()
    }

    private fun getSession(accountName: String): String? {
        return sessionStorage.getString(accountName, null)
    }
}