package ru.hungrymole.pob.components.config

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.Spinner
import com.jakewharton.rxbinding2.widget.checkedChanges
import com.jakewharton.rxbinding2.widget.itemSelections
import io.reactivex.Observable
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.item_config_checkbox.view.*
import kotlinx.android.synthetic.main.item_config_number.view.*
import kotlinx.android.synthetic.main.item_config_spinner.view.*
import kotlinx.android.synthetic.main.layout_config_label.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.inflate
import ru.hungrymole.pob.ui.adapters.MutableArrayAdapter
import ru.hungrymole.pob.ui.adapters.mutableAdapter
import ru.hungrymole.pob.ui.list.RecyclerFragment
import ru.hungrymole.pob.ui.list.RecyclerHolder
import ru.hungrymole.pob.ui.view.NumberPicker
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by khusainov.d on 07.08.2017.
 */
class ConfigSectionFragment : RecyclerFragment<ConfigItem, ConfigSectionVM, ConfigSectionFragment.BaseConfigHolder>() {

    @Inject override lateinit var vm: ConfigSectionVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
        vm.section = arguments?.getString("name") ?: error("Section not found in arguments")
    }

    override fun getViewType(pos: Int) = vm.getConfigType(items[pos])

    private val holders = mapOf(
        CONFIG_CHECKBOX to (R.layout.item_config_checkbox to ::CheckboxConfigHolder),
    CONFIG_SPINNER to (R.layout.item_config_spinner to ::SpinnerConfigHolder),
    CONFIG_NUMBER to (R.layout.item_config_number to ::NumberConfigHolder),
    CONFIG_EMPTY to (R.layout.item_config_empty to ::BaseConfigHolder)
    )

    override fun createView(viewType: Int, parent: ViewGroup): View {
        return inflate(holders.getValue(viewType).first, parent).apply {
            if (viewType == CONFIG_SPINNER) {
                config_spinner.adapter = act.mutableAdapter<ConfigSpinnerItem>()
            }
        }
    }

    override fun createHolder(viewType: Int, view: View): BaseConfigHolder {
        return holders.getValue(viewType).second(view)
    }

    override fun BaseConfigHolder.subscribeHolder() {

        fun <T> Observable<T>.filterAddId(): Observable<Pair<String, T>> =
            filterOnlyUserInput()
            .map { (item as ConfigItemWithValue<*>).id to it }

        subscriptions += when (this) {
            is CheckboxConfigHolder -> checkbox.checkedChanges()
                .filterAddId()
                .subscribe(vm.checkboxChanges::onNext)
            is NumberConfigHolder -> number.changes
                .filterAddId()
                .debounce(500, TimeUnit.MILLISECONDS)
                .subscribe(vm.numberChanges::onNext)
            is SpinnerConfigHolder -> spinner.itemSelections()
                .filter { it != -1 && (item as ConfigItemSpinner).selected != it }
                .filterAddId()
                .doOnNext { (item as ConfigItemSpinner).selected = it.second }
                .subscribe(vm.spinnerChanges::onNext)
            else -> Disposables.empty()
        }
    }

    override fun areContentsTheSame(old: ConfigItem, new: ConfigItem): Boolean {
        return when {
            old is ConfigItemWithValue<*> && new is ConfigItemWithValue<*> -> old.value == new.value
            else -> true
        }
    }

    override fun BaseConfigHolder.bind(item: ConfigItem) {
        label.text = item.label
        when  {
            item is ConfigItemCheckbox && this is CheckboxConfigHolder-> checkbox.isChecked = item.value
            item is ConfigItemSpinner && this is SpinnerConfigHolder -> {
                adapter.setItems(item.list)
                spinner.setSelection(item.selected)
            }
            item is ConfigItemNumber && this is NumberConfigHolder -> number.value = item.value
            else -> {}//label only
        }
    }

    open class BaseConfigHolder(view: View) : RecyclerHolder(view) {
        val label = view.config_label
    }

    class CheckboxConfigHolder(view: View) : BaseConfigHolder(view) {
        val checkbox: CheckBox = view.config_checkbox
    }

    class SpinnerConfigHolder(view: View) : BaseConfigHolder(view) {
        val spinner: Spinner = view.config_spinner
        val adapter by lazy { spinner.adapter as MutableArrayAdapter<ConfigSpinnerItem> }
    }

    class NumberConfigHolder(view: View) : BaseConfigHolder(view) {
        val number: NumberPicker = view.config_number
    }
}