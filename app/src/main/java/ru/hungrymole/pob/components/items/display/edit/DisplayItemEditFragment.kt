package ru.hungrymole.pob.components.items.display.edit

import android.os.Bundle
import io.reactivex.disposables.CompositeDisposable
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.ui.mvvm.ViewFragment
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 03.09.2017.
 */
class DisplayItemEditFragment : ViewFragment<EditState, DisplayItemEditVM>() {

    @Inject override lateinit var vm: DisplayItemEditVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
    }

    override val layout = R.layout.fragment_display_item_edit

    override fun onStart(subscriptions: CompositeDisposable) {
    }

    override fun updateState(state: EditState) {
    }
}