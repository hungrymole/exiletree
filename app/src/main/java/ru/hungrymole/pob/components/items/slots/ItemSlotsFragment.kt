package ru.hungrymole.pob.components.items.slots

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.checkedChanges
import com.jakewharton.rxbinding2.widget.itemSelections
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.item_item_slot.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.TooltipManager
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.components.TooltipElementAdapter2
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.getActivity
import ru.hungrymole.pob.extensions.inflate
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.ui.list.RecyclerFragment
import ru.hungrymole.pob.ui.list.RecyclerHolder
import ru.hungrymole.pob.ui.rx.plusAssign
import javax.inject.Inject

/**
 * Created by khusainov.d on 11.08.2017.
 */
class ItemSlotsFragment : RecyclerFragment<ItemSlot, ItemSlotsVM, ItemSlotsFragment.ItemSlotHolder>() {

    @Inject override lateinit var vm: ItemSlotsVM
    @Inject lateinit var tooltipManager: TooltipManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
        setHasOptionsMenu(true)
    }

    override fun createHolder(viewType: Int, view: View) = ItemSlotHolder(view)

    override fun createView(viewType: Int, parent: ViewGroup): View {
        return inflate(R.layout.item_item_slot, parent)
    }

  override fun ItemSlotHolder.subscribeHolder() {
    subscriptions += list
      .itemSelections()
      .filterOnlyUserInput()
      .filter { it != -1 }
      .filter { item.index != it }
      .subscribe { newPos ->
        vm.slotChanges.onNext(item.slotName to newPos)
      }

    subscriptions += checkbox
      .checkedChanges()
      .filterOnlyUserInput()
      .subscribe { vm.flaskEnabledChanges.onNext(item.slotName to it) }
  }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_item_slots, menu)

        this += menu.findItem(R.id.act_swap_weapons).clicks().subscribe(vm.weaponSwap::onNext)
    }

    override fun areContentsTheSame(old: ItemSlot, new: ItemSlot): Boolean {
        return old.value == new.value && old.list.toSet() == new.list.toSet()
    }

    override fun ItemSlotHolder.bind(item: ItemSlot) {
        label.text = item.label
        adapter.setItems(item.tooltips)
        list.setSelection(item.index)
        checkbox.visible = item.label.contains("Flask")
        checkbox.isChecked = item.active ?: false
    }

    class ItemSlotHolder(v: View) : RecyclerHolder(v) {
        val label = v.item_slot_label
        val checkbox = v.item_slot_enabled
        val list = v.item_slot_list
        val adapter by lazy { TooltipElementAdapter2(v.getActivity(), emptyList()).also(list::setAdapter) }
    }
}