package ru.hungrymole.pob.components.items.items

import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jakewharton.rxbinding2.support.v7.widget.itemClicks
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.longClicks
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import ru.hungrymole.pob.build.Router
import ru.hungrymole.pob.build.TooltipManager
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.components.items.BuildItem
import ru.hungrymole.pob.components.setSingleLineTooltip
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.inflate
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.ui.list.RecyclerFragment
import ru.hungrymole.pob.ui.list.RecyclerHolder
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by khusainov.d on 11.08.2017.
 */
class BulidItemsFragment : RecyclerFragment<BuildItem, BuildItemsVM, BulidItemsFragment.BuildItemHolder>() {

    @Inject lateinit var router: Router
    @Inject lateinit var tooltipManager: TooltipManager
    @Inject override lateinit var vm: BuildItemsVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
    }

    override fun createHolder(viewType: Int, view: View) = BuildItemHolder(view)
    override fun createView(viewType: Int, parent: ViewGroup): View {
        return inflate(android.R.layout.simple_list_item_1, parent)
    }

    private val VIEW = "View"
    private val DELETE = "Delete"

    override fun BuildItemHolder.subscribeHolder() {
        subscriptions += view
          .longClicks()
          .flatMapSingle { vm.getItemTooltip(item) }
          .main()
          .subscribeBy(
            onError = Timber::e,
            onNext = tooltipManager::showToolTip
          )

        subscriptions += view
          .clicks()
          .flatMap {
              PopupMenu(act, view).run {
//                  menu.add(VIEW)//todo edit Display item
                  menu.add(DELETE)
                  show()
                  itemClicks()
              }
          }
          .flatMapSingle {
              val title = it.title
              when (title) {
                  /*
                  VIEW   -> vm
                    .setDisplayItem(item)
                    .main()
                    .doOnSuccess { router.showDisplayItem() }
                    */
                  DELETE -> vm.deleteItem(item)
                  else   -> error("Invalid menu items $title")
              }
          }
          .subscribe()
    }

    override fun areContentsTheSame(old: BuildItem, new: BuildItem): Boolean {
        return true
    }

    override fun BuildItemHolder.bind(item: BuildItem) {
      item
        .tooltip
        ?.let(label::setSingleLineTooltip)
        ?: label.setText(item.name)
    }

    class BuildItemHolder(v: View) : RecyclerHolder(v) {
        val label = v as TextView
    }
}