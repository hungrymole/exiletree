package ru.hungrymole.pob.components.tree.ui

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import kotlinx.android.synthetic.main.item_ascendancy_avatar.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.components.ascendancyOrClassAvatar

/**
 * Created by mole on 28.04.16.
 */
class AscendancyAdapter(val act : Activity, val classes: List<String>) : BaseAdapter() {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View? {
        val view = convertView ?: act.layoutInflater.inflate(R.layout.item_ascendancy_avatar, null)
        view.asc_icon.setImageResource(act.ascendancyOrClassAvatar(classes.get(position)))
        return view
    }

    override fun getItem(p0: Int) = classes[p0]
    override fun getItemId(p0: Int) = -0L
    override fun getCount() = classes.size
}