package ru.hungrymole.pob.components.items.display.edit

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import ru.hungrymole.pob.ui.mvvm.ViewModel
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 03.09.2017.
 */
class DisplayItemEditVM @Inject constructor(
  ctx: Context
) : ViewModel<EditState>(ctx) {
  override fun getState() = Single.fromCallable {
        EditState()
    }

    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {
    }

    override fun subscribeOnStart(subscriptions: CompositeDisposable) {
    }
}