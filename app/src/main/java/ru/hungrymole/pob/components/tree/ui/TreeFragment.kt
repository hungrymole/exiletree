package ru.hungrymole.pob.components.tree.ui

import android.os.Bundle
import android.support.v7.widget.ListPopupWindow
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.fragment_tree.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.TooltipManager
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.components.TooltipElementAdapter2
import ru.hungrymole.pob.components.tree.TreeState
import ru.hungrymole.pob.components.tree.TreeVM
import ru.hungrymole.pob.components.tree.model.tree.Graph
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.SEARCH_HIGHLIGHT_COLOR
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.ClassNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.JewelNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.ascendancy.AscendancyButton
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.generator.lua.SpanInfo
import ru.hungrymole.pob.ui.mvvm.ViewFragment
import ru.hungrymole.pob.ui.rx.plusAssign
import ru.hungrymole.pob.ui.view.SearchManager
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 29.01.15.
 */
class TreeFragment : ViewFragment<TreeState, TreeVM>() {

    @Inject lateinit var graph: Graph
    @Inject lateinit var tooltipManager: TooltipManager
    @Inject lateinit var search: SearchManager
    @Inject override lateinit var vm: TreeVM

    override val layout = R.layout.fragment_tree

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        act.di().inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tree_view.graph = graph
    }

    override fun onStart(subscriptions: CompositeDisposable) {
        subscriptions += tree_view.nodeClicks.subscribe {
            when {
                it is AscendancyButton -> {
                    it.isAllocated = !it.isAllocated
                    tree_view.invalidate()
                }
//                it is AscendancyStart -> {}
                it is ClassNode -> showSelectAscendancyPopup(it)
                it is JewelNode && it.isAllocated -> showJewelPopup(it)
                else -> vm.nodeClicks.onNext(it)
            }
        }

        subscriptions += tree_view.nodeLongPresses
            .filter(AbstractNode::hasTooltip)
            .flatMapSingle(vm::getNodeTooltip)
            .main()
            .subscribe { tooltip ->
                val query = search.query
                val start = tooltip.text.indexOf(query, ignoreCase = true)
                if (query.isNotBlank() && start != -1) {
                    tooltip.highlights += SpanInfo(SEARCH_HIGHLIGHT_COLOR, start, start + query.length)
                }
                tooltipManager.showToolTip(tooltip)
            }

        subscriptions += vm.refresh
                .main()
                .subscribe { tree_view.invalidate() }

        subscriptions += search
            .queryChanges
            .debounce(320, TimeUnit.MILLISECONDS)
            .flatMapSingle(vm::searchNodes)
            .subscribe()

        subscriptions += Disposables.fromAction(search::hide)
    }

    override fun updateState(state: TreeState) {
        graph.setState(state)
        state.searchStr?.let(search::setQuery)
        tree_view.invalidate()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_tree, menu)
        this += menu.findItem(R.id.act_search_node)
            .clicks()
            .subscribe { search.show() }
    }

    private fun showSelectAscendancyPopup(clickedClass: ClassNode) {
        val clickedActiveClass = graph.exile == clickedClass
        val currentAsc = graph.ascendancy.name
        val clickedScion = clickedClass.classType == ClassNode.CLASS.SCION

        val showDialog = fun() = with(ListPopupWindow(act)) {
            setAdapter(AscendancyAdapter(act, clickedClass.classType.ascendancies))
            anchorView = tree_view
            height = act.resources.getDimensionPixelSize(R.dimen.asc_list_height)
            width = act.resources.getDimensionPixelSize(R.dimen.asc_list_width)
            horizontalOffset = tree_view.width / 2 - width / 2
            verticalOffset = -(tree_view.height - height) * 2

            setOnItemClickListener { parent, _, position, _ ->
                val asc = parent.getItemAtPosition(position).toString()
                if (currentAsc != asc) {
                    vm.ascSelects.onNext(clickedClass to asc)
                }
                dismiss()
            }
            show()
        }

        when {
            clickedActiveClass && clickedScion -> return
            clickedScion -> vm.ascSelects.onNext(clickedClass to "Ascendant")
            else -> showDialog()
        }
    }

    private fun showJewelPopup(node: JewelNode) = with(ListPopupWindow(act)) {
        val availableJewels = vm.getSocketJewels(node)
        setAdapter(TooltipElementAdapter2(act, availableJewels.tooltips))
        setSelection(availableJewels.index)
        anchorView = tree_view
        height = act.resources.getDimensionPixelSize(R.dimen.jewel_list_height)
        width = act.resources.getDimensionPixelSize(R.dimen.jewel_list_width)
        horizontalOffset = tree_view.width / 2 - width / 2
        verticalOffset = -(tree_view.height - height) * 2

        setOnItemClickListener { _, _, position, _ ->
            vm.jewelSelections.onNext(position to node)
            dismiss()
        }
        show()
    }
}
