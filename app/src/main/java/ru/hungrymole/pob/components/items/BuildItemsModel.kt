package ru.hungrymole.pob.components.items

import ru.hungrymole.pob.components.HasSingleLineTooltip
import ru.hungrymole.pob.lua.Table
import ru.hungrymole.pob.lua.Tooltip

/**
 * Created by khusainov.d on 15.08.2017.
 */

@Table
class BuildItem(
  val name: String,
  val id: Int
) : HasSingleLineTooltip {

  override var tooltip: Tooltip? = null

  override fun equals(other: Any?): Boolean {
    if (this === other) return true
    if (javaClass != other?.javaClass) return false

    other as BuildItem

    if (id != other.id) return false

    return true
  }

  override fun hashCode(): Int {
    return id
  }
}