package ru.hungrymole.pob.components.tree.model.tree.nodes

import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 04.12.14.
 */
class ClassNode(data: TreeNodeData, classId: Int) : AbstractNode(data) {

    val classType = CLASS.getById(classId)
    override val isClass = true
    override val hasTooltip = false

    enum class CLASS(var id: Int, var ascendancies: List<String>) {
        SCION(0, listOf("Ascendant")),
        MARAUDER(1, listOf("Juggernaut", "Berserker", "Chieftain")),
        RANGER(2, listOf("Raider", "Deadeye", "Pathfinder")),
        WITCH(3, listOf("Occultist", "Elementalist", "Necromancer")),
        DUELIST(4, listOf("Slayer", "Gladiator", "Champion")),
        TEMPLAR(5, listOf("Inquisitor", "Hierophant", "Guardian")),
        SHADOW(6, listOf("Assassin", "Trickster", "Saboteur"));

        companion object {
            fun getById(id: Int): CLASS = CLASS.values().first { it.id == id }
            fun getByAscendancy(name: String): CLASS =
                    CLASS.values().first { it.ascendancies.contains(name) }
        }
    }

    override fun contains(pressedX: Int, pressedY: Int, scale: Float): Boolean {
        return x >= pressedX - iconHalfWidth &&
                x <= pressedX + iconHalfWidth &&
                y >= pressedY - iconHalfHeight &&
                y <= pressedY + iconHalfHeight
    }
}
