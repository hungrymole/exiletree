package ru.hungrymole.pob.components.tree.model.tree.drawing;

import android.graphics.Point;
import android.graphics.Rect;

//https://github.com/donkike/Computer-Graphics/blob/master/LineClipping/LineClippingPanel.java
public class LiangBarsky {

    private static int[] p = new int[4];
    private static int[] q = new int[4];

    public static void clip(int x0,
                            int y0,
                            int x1,
                            int y1,
                            Rect rect,
                            Point out) {
        double u1 = 0;
        double u2 = 1;
        int dx = x1 - x0;
        int dy = y1 - y0;

        p[0] = -dx;
        p[1] = dx;
        p[2] = -dy;
        p[3] = dy;

        q[0] = x0 - rect.left;
        q[1] = rect.right - x0;
        q[2] = y0 - rect.top;
        q[3] = rect.bottom - y0;

        for (int i = 0; i < 4; i++) {
            if (p[i] == 0) {
                if (q[i] < 0) {
                    return;
                }
            } else {
                double u = (double) q[i] / p[i];
                if (p[i] < 0) {
                    u1 = Math.max(u, u1);
                } else {
                    u2 = Math.min(u, u2);
                }
            }
        }
        if (u1 > u2) {
            return;
        }
        int a = (int) (y0 + u1 * dy);
        out.x = (int) (x0 + u2 * dx);
        out.y = (int) (y0 + u2 * dy);
    }
}