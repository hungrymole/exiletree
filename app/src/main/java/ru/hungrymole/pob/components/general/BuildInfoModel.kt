package ru.hungrymole.pob.components.general

import ru.hungrymole.pob.dialogs.SpinnerConfigEntry
import ru.hungrymole.pob.lua.KeyName
import ru.hungrymole.pob.lua.Table

/**
 * Created by Dinar Khusainov on 02.08.2017.
 */

typealias BuildAttrs = List<Pair<String, String>>

class BuildInfoState(val name: String,
                     val ascendancy: String,
                     val points: BuildPoints,
                     val attrs: BuildAttrs)
@Table
class BuildPoints(val used: Int,
                  val allocatedAsc: Int,
                  val usedMax: Int) {
    override fun toString() = "${used} / ${usedMax}"
}
@Table
class Bandit(val label: String,
             val banditId: String) {

    override fun toString() = label
}

@Table
class DropdownValue(val label: String,
                    @KeyName("val") val id: Int) {

    override fun toString() = label
}

@Table
class BanditDropdownConfig(list: List<Bandit>,
                   selIndex: Int) : SpinnerConfigEntry<Bandit>(list, selIndex)

@Table
class DropdownConfig(list: List<DropdownValue>,
                    selIndex: Int) : SpinnerConfigEntry<DropdownValue>(list, selIndex)