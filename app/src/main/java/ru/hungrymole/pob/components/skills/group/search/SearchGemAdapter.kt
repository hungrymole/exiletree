package ru.hungrymole.pob.components.skills.group.search

import android.content.Context
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import kotlinx.android.synthetic.main.item_skill_search.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.components.setTooltip
import ru.hungrymole.pob.components.skills.SearchableSkill
import ru.hungrymole.pob.extensions.color
import ru.hungrymole.pob.extensions.visible

/**
 * Created by khusainov.d on 18.07.2017.
 */
class SearchGemAdapter(ctx: Context) : ArrayAdapter<SearchableSkill>(ctx, R.layout.item_skill_search,
        R.id.skill_search_name, mutableListOf<SearchableSkill>()) {

    val textColor = ctx.color(R.color.black)

    var items: List<SearchableSkill> = emptyList()
        set(value) {
            field = value
            clear()
            addAll(value)
        }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        return super.getView(position, convertView, parent).apply {
            val item = items[position]
            val img = if (item.plusSign) R.drawable.ic_add_white_24dp else R.drawable.ic_done_white_24dp
            (skill_search_name as TextView).setTextColor(item.color ?: textColor)
            skill_search_icon.setImageResource(img)
            skill_search_icon.setColorFilter(item.signColor)
            skill_search_icon.visible = item.okSign || item.plusSign

            skill_search_tooltip.visible = item.tooltip != null
            item.tooltip?.let { skill_search_tooltip.setTooltip(it) }
        }
    }

    override fun getFilter() = object : Filter() {
        //filtering in GemRepository
        override fun performFiltering(constraint: CharSequence) =  FilterResults().apply {
            count = items.count()
            values = items
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            notifyDataSetChanged()
        }
    }
}