package ru.hungrymole.pob.components.general

import android.os.Bundle
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.fragment_general_info.*
import kotlinx.android.synthetic.main.item_build.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.components.ascendancyOrClassAvatar
import ru.hungrymole.pob.dialogs.ConfigDialog
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.ctx
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.ui.FabFragment
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 01.08.2017.
 */
class BuildInfoFragment : FabFragment<BuildInfoState, BuildInfoVM>() {

    @Inject override lateinit var vm: BuildInfoVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
    }

    override val layout = R.layout.fragment_general_info
    override val fabImage = R.drawable.ic_settings_white_24dp

    override fun onStart(subscriptions: CompositeDisposable) {
        subscriptions += fab.clicks()
            .flatMapSingle { vm.getBuildConfig() }
            .main()
            .flatMapMaybe { ConfigDialog(act, "General settings", it) }
            .flatMapSingle { vm.updateBuildConfig(it) }
            .subscribe()

    }

    override fun updateState(state: BuildInfoState) {
        build_name.text = state.name
        build_avatar.setImageResource(ctx.ascendancyOrClassAvatar(state.ascendancy))
        build_points.text = state.points.toString()

        general_info_attributes.text = state.attrs.joinToString("\n") { it.first }
        general_info_attribute_values.text = state.attrs.joinToString("\n") { it.second }
    }
}