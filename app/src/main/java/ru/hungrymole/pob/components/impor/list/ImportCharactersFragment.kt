package ru.hungrymole.pob.components.impor.list

import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.fragment_import.*
import kotlinx.android.synthetic.main.item_build.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.components.ascendancyOrClassAvatar
import ru.hungrymole.pob.components.impor.PoeCharacter
import ru.hungrymole.pob.dialogs.rxConfirmDialog
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.ctx
import ru.hungrymole.pob.extensions.inflate
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.extensions.toast
import ru.hungrymole.pob.ui.list.RecyclerFragment
import ru.hungrymole.pob.ui.list.RecyclerHolder
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 04.02.2018.
 */
class ImportCharactersFragment : RecyclerFragment<PoeCharacter, ImportCharactersVM, ImportCharactersFragment.CharacterHolder>() {

    @Inject override lateinit var vm: ImportCharactersVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
    }

    override fun createView(viewType: Int, parent: ViewGroup): View {
        return inflate(R.layout.item_build, parent)
    }

    override fun createHolder(viewType: Int, view: View): CharacterHolder {
        return CharacterHolder(view)
    }

    override fun CharacterHolder.bind(item: PoeCharacter) {
        name.text = item.name
        level.text = "${item.league}, ${item.level}"
        avatar.setImageResource(ctx.ascendancyOrClassAvatar(item.className))
    }

    override fun CharacterHolder.subscribeHolder() {
        subscriptions += view.clicks()
            .flatMapMaybe { act.rxConfirmDialog("Import this char?") }
            .filter { it }
            .flatMapSingle {
                val char = item
                Timber.i("Importing accountName=${act.import_account_name?.text} char=${char.name}")
                vm.import(char)
            }
            .main()
            .subscribe(act::toast)
    }

    override fun areContentsTheSame(old: PoeCharacter, new: PoeCharacter): Boolean {
        return old.className == new.className
    }

    class CharacterHolder(v: View) : RecyclerHolder(v) {
        val name = view.build_name
        val level = view.build_points
        var avatar = view.build_avatar
    }
}