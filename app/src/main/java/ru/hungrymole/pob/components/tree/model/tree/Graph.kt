package ru.hungrymole.pob.components.tree.model.tree

import android.graphics.Bitmap
import android.graphics.Rect
import android.graphics.RectF
import android.util.SparseArray
import android.view.View
import kdbush.int32.KDBushIntFactory
import ru.hungrymole.pob.components.tree.INTUITIVE_LEAP
import ru.hungrymole.pob.components.tree.SocketedJewel
import ru.hungrymole.pob.components.tree.TreeState
import ru.hungrymole.pob.components.tree.model.tree.drawing.GraphDrawer
import ru.hungrymole.pob.components.tree.model.tree.drawing.JewelDrawer
import ru.hungrymole.pob.components.tree.model.tree.drawing.toRect
import ru.hungrymole.pob.components.tree.model.tree.drawing.toRectF
import ru.hungrymole.pob.components.tree.model.tree.edges.Edge
import ru.hungrymole.pob.components.tree.model.tree.edges.Orbit
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.ClassNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.JewelNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.ascendancy.AscendancyGraph
import ru.hungrymole.pob.components.tree.model.tree.nodes.ascendancy.ScionPathOfX
import ru.hungrymole.pob.extensions.toSparseArray
import java.util.*

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 18.12.14.
 */
class Graph(val nodeMap: SparseArray<AbstractNode>,
            val nodes: List<AbstractNode>,
            val edges: List<Edge>,
            val groups: List<Group>,
            private val ascendancyMap: Map<String, AscendancyGraph>,
            private val _treeSize: Rect,
            val ascImageLoad: (String) -> Bitmap) {

    var exile: ClassNode
    var ascendancy: AscendancyGraph
    var jewels: Map<Int, SocketedJewel> = emptyMap()
        set(value) {
            field.forEach { nodeMap[it.key].controller?.detach() }
            field = value
            field.forEach {
                val socket = nodeMap[it.key]
                val affectedNodes = it.value.affectedNodes.map(nodeMap::get).filterNotNull()
                socket.controller = JewelDrawer(it.value, socket, affectedNodes)
            }
        }

    val ascendancies = ascendancyMap.values.toList()
    val scendanciesNodes = ascendancies.flatMap { it.nodes }

    val drawer = GraphDrawer(this)

    private val fTreeSize: RectF

    init {
        _treeSize.inset(-300, -750)
        fTreeSize = _treeSize.toRectF()
        ascendancy = ascendancyMap["Ascendant"]!!
        ascendancy.attach(ascImageLoad("Ascendant"))
        exile = getClassById(ClassNode.CLASS.getByAscendancy("Ascendant").id)
        exile.isAllocated = true
    }

    fun getTreeSize(): RectF = fTreeSize

    fun setState(state: TreeState): Unit {
        exile = getClassById(state.classId)
        setAscendancy(state.ascClass)
        (ascendancy.nodes + nodes).forEach { it.isAllocated = it.id in state.allocated }
        jewels = state.jewels
    }

    fun setAscendancy(name: String) {
        val actual = if(name == "None") "Ascendant" else name
        val newAsc = ascendancyMap[actual]!!
        if (newAsc != ascendancy) {
            ascendancy.detach()
            ascendancy = newAsc
            ascendancy.attach(ascImageLoad(actual))
        }
    }

    fun selectClass(id: Int) {
        val node = getClassById(id)
        if (!node.isAllocated) {
            node.isAllocated = true
            exile.isAllocated = false
            exile = node
            for (edge in node.connections) {
                if (edge.getSecondNode(node).isAllocated) {
                    return
                }
            }
            reset()
            exile.isAllocated = true
        }
    }

    fun getClassById(id: Int) = nodes
            .filterIsInstance<ClassNode>()
            .first { it.classType.id == id }

    fun getPressedNode(x: Float, y: Float, view: View, regionF: RectF): AbstractNode? {
        val region = regionF.toRect()
        val scale = view.height.toFloat() / region.height()
        val xPosTemp = x / view.width.toFloat() * region.width()
        val yPosTemp = y / view.height.toFloat() * region.height()
        val xPos = xPosTemp.toInt() + region.left
        val yPos = yPosTemp.toInt() + region.top
        return ascendancy.press(xPos, yPos, scale) ?:
                nodes.firstOrNull { region.contains(it.x, it.y) && it.contains(xPos, yPos, scale) }
    }

    fun getNodesIsland(source: AbstractNode): Set<Edge>? {
        if (!source.isAllocated) {
            return null
        }
        val visited = HashSet<AbstractNode>()
        val queue = LinkedList<AbstractNode>()
        val result = HashSet(source.connections)

        queue.offer(source)
        while (!queue.isEmpty()) {
            val node = queue.poll()

            if (node === exile) {
                return null
            }

            visited.add(node)

            for (edge in node.connections) {
                val connection = edge.getSecondNode(node)
                if (!visited.contains(connection) && connection.isAllocated && !queue.contains(connection)) {
                    result.add(edge)
                    queue.offer(connection)
                }

            }
        }
        return result
    }

    fun getShortestPathForAllocate(destination: AbstractNode): List<Edge> {
        val paths = LinkedHashMap<AbstractNode, AbstractNode>()
        val edges = LinkedHashMap<AbstractNode, Edge>()
        val visited = HashSet<AbstractNode>()
        val queue = LinkedList<AbstractNode>()
        var src: AbstractNode? = null

        queue.offer(destination)
        while (!queue.isEmpty()) {
            val node = queue.poll()
            if (destination !is ScionPathOfX && node is ScionPathOfX && !node.isAllocated) {
                continue
            }

            if (node.isAllocated && node.controller?.jewel?.title != INTUITIVE_LEAP) {
                src = node
                break
            }

            visited.add(node)

            for (edge in node.connections) {
                val connection = edge.getSecondNode(node)
                if (!visited.contains(connection) && !paths.containsKey(connection) && !node.isClass) {
                    paths.put(connection, node)
                    edges.put(connection, edge)
                    queue.offer(connection)
                }

            }
        }

        val result = LinkedList<Edge>()
        while (src != null && src !== destination) {
            result.add(edges[src]!!)
            src = paths[src]
        }
        return result
    }

    fun reset() {
        nodes.forEach { it.isAllocated = false }
        ascendancy.reset()
    }
}