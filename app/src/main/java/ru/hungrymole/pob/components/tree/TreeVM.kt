package ru.hungrymole.pob.components.tree

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.build.PhoneScreenTouchedEvent
import ru.hungrymole.pob.common.castOrNull
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.components.items.slots.ItemSlot
import ru.hungrymole.pob.components.tree.model.tree.Graph
import ru.hungrymole.pob.components.tree.model.tree.edges.Edge
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.ClassNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.JewelNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.Mastery
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.generator.tree.TreeConstants
import ru.hungrymole.pob.lua.*
import ru.hungrymole.pob.ui.mvvm.ViewModel
import javax.inject.Inject

/**
 * Created by khusainov.d on 01.08.2017.
 */
class TreeVM @Inject constructor(
  private val lua: Globals,
  private val graph: Graph,
  private val eventBus: EventBus,
  ctx: Context
) : ViewModel<TreeState>(ctx) {

    val jewelRadiuses: Map<Int, Int> by lazy {
        lua.getBy("data.jewelRadius").toList<JewelRadiusInfo>().associate { it.key to it.rad }
    }

    val nodeClicks = PublishSubject.create<AbstractNode>()
    val jewelSelections = PublishSubject.create<Pair<Int, JewelNode>>()
    val ascSelects = PublishSubject.create<Pair<ClassNode?, String>>()
    val refresh = PublishSubject.create<Any?>()

    override fun getState() = Single.fromCallable {
        val spec = lua.getBy("build.spec").checktable()
        val allocated = spec["allocNodes"].checktable().keys().map { it.checkint() }
        val cls = spec["curClassId"].checkint()
        val asc = spec["curAscendClassName"].checkjstring()
        val jewels = lua.getBy("build.spec.jewels").checktable()
        val socketedJewels = jewels.keys()
                .map { k -> k.checkint() to jewels[k].checkint() }
                .filter { it.second != 0 && !spec["allocNodes"][it.first].isnil() }
                .toMap()

        val actualJewels = socketedJewels.mapValues { (nodeId, jewelId) ->
            lua.getBy("build.itemsTab.items.[$jewelId]").toObject<SocketedJewel>().apply {
                if (jewelRadiusIndex != null && jewelRadiusIndex != 0) {
                    radius = jewelRadiuses[jewelRadiusIndex]!! * TreeConstants.RESIZE
                    val nodes = lua.getBy("build.spec.nodes.[$nodeId].nodesInRadius.[$jewelRadiusIndex]").toList<NodeInfo>()
                    val (affected, color) = getAffectedNodesAndColor(this, nodes)
                    affectedNodes = affected.map { it.key }
                    affectedNodeColor = color
                }
            }
        }
        val searchStr = lua.getBy("build.treeTab.viewer.searchStr").optjstring("")
        TreeState(allocated, cls, asc, actualJewels, searchStr)
    }.luaSubscribe()

    private fun saveState(): Boolean {
        val spec = lua.getBy("build.spec")
        val name = graph.ascendancy.name
        spec["curClassId"] = graph.exile.classType.id
        spec["curAscendClassName"] = name
        spec["curAscendClassId"] = spec.checktable().getBy("tree.ascendNameMap.$name.ascendClassId", false)

        val newNodes = (graph.nodes + (graph.ascendancy.nodes)).filter { it.isAllocated }
        val newIds = newNodes.map { it.id }
        val currentIds = spec["allocNodes"].checktable().keys().map { it.checkint() }
        (newIds - currentIds).forEach { spec["allocNodes"][it] = spec["nodes"][it] }
        (currentIds - newIds).forEach { spec["allocNodes"][it] = LuaValue.NIL }
        lua["build"]["buildFlag"] = true
        val allocatedJewel = newNodes.firstOrNull { it.isJewel }
        allocatedJewel?.let { lua.getBy("build.itemsTab.sockets.[${it.id}]").selfCall("Populate") }
        return allocatedJewel != null
    }

    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {
        fun resetHighlightedEdges() {
            graph.edges.forEach(Edge::unhighlight)
            graph.ascendancy.edges.forEach(Edge::unhighlight)
        }

        subscriptions += eventBus
            .ofType<PhoneScreenTouchedEvent>()
            .doOnNext { resetHighlightedEdges() }
            .doOnDispose { resetHighlightedEdges() }
            .subscribe(refresh::onNext)
    }

    override fun subscribeOnStart(subscriptions: CompositeDisposable) {
        subscriptions += nodeClicks
                .lua()
                .doOnNext(this::clickNode)
                .doOnNext(refresh::onNext)
                .lua()
                .map { saveState() }
                .flatMapSingle { if(it) reloadState() else Single.just(Unit) }
                .subscribe()

        subscriptions += ascSelects
                .lua()
                .doOnNext { (cls, asc) ->
                    cls?.let { graph.selectClass(it.classType.id) }
                    graph.setAscendancy(asc)
                }
                .doOnNext { saveState() }
                .subscribe(refresh::onNext)

        subscriptions += jewelSelections
                .lua()
                .doOnNext { (i, socket) ->
                    lua.getBy("build.itemsTab.sockets.[${socket.id}]").selfCall("SetSel", i + 1)
                    //deallocated intuitive leap, clear nodes
                    val controller = socket.controller
                    if (controller?.jewel?.title == INTUITIVE_LEAP &&
                            !getSocketJewels(socket).value.contains(INTUITIVE_LEAP)) {
                        controller.affectedNodes
                                .mapNotNull(graph::getNodesIsland)
                                .flatMap { it }
                                .forEach { it.allocate(false) }
                        saveState()
                    }
                }
                .flatMapSingle { reloadState() }
                .subscribe()
    }

    fun searchNodes(query: String) = Single.fromCallable {
        val allNodes = graph.nodes + graph.scendanciesNodes
        val luaNodes = lua.getBy("build.spec.nodes").checktable()
        val passiveTreeView = lua.getBy("build.treeTab.viewer")
        passiveTreeView["searchStr"] = query

        if (query.isBlank() || query.trim().length < 3) {
            allNodes.forEach { it.matchesSearch = false }
            return@fromCallable
        }

        allNodes
            .asSequence()
            .filterNot { it is Mastery || it is ClassNode }
            .forEach { node ->
                val luaNode = luaNodes[node.id].checktable()
                node.matchesSearch = passiveTreeView
                    .selfCall("DoesNodeMatchSearchStr", luaNode)
                    .optboolean(false)
            }
    }
      .luaSubscribe()
      .doOnSuccess { refresh.onNext(Unit) }

    fun getSocketJewels(socket: JewelNode): ItemSlot =
      lua
        .getBy("build.itemsTab.sockets.[${socket.id}]")
        .toObject()

    fun getNodeTooltip(node: AbstractNode) = Single.fromCallable<Tooltip> {
        val luaNodes = lua.getBy("build.spec.nodes").checktable()
        val pressed = luaNodes[node.id]
        val passiveTreeView = lua.getBy("build.treeTab.viewer")

        val path = if (node.isAllocated) {
            //todo ...
            val jewel = node.castOrNull<JewelNode>()
            val nodeController = jewel?.controller
            node.isAllocated = false
            val deallocEdges = node.getActiveNodeIslands()
            node.isAllocated = true
            jewel?.controller = nodeController

            deallocEdges
                .filter(Edge::isAllocated)
                .onEach(Edge::highlight)
                .flatMap(Edge::nodes)
        } else {
            graph
                .getShortestPathForAllocate(node)
                .onEach(Edge::highlight)
                .flatMap(Edge::nodes)
                .filterNot(AbstractNode::isAllocated)
        }.distinct()

        pressed["alloc"] = node.isAllocated
        passiveTreeView["tracePath"] = path
            .map { luaNodes[it.id] }
            .toArrayTable()
        val luaTooltip = passiveTreeView["tooltip"]
        luaTooltip.selfCall("Clear")
        passiveTreeView.selfCall("AddNodeTooltip", pressed, lua.getBy("build"))
        Tooltip(luaTooltip)
    }
        .luaSubscribe()
        .doOnSuccess(refresh::onNext)

    private fun clickNode(node: AbstractNode): Unit {

        val deallocateIntuitiveLeap = fun(socket: AbstractNode) {
            val controller = socket.controller ?: return
            val jewelNode = controller.jewelNode
            node.isAllocated = false
            val deallocateIL = graph.getNodesIsland(jewelNode)
            if (deallocateIL != null) {
                controller.affectedNodes
                        .plus(node.connections.map { it.getSecondNode(node) })
                        .mapNotNull(graph::getNodesIsland)
                        .flatMap { it }
                        .forEach { it.allocate(false) }
            }
        }
        fun AbstractNode.isIntuitiveLeap() = controller?.jewel?.title == INTUITIVE_LEAP

        when {
            node.isIntuitiveLeap() && !node.isAllocated -> node.isAllocated = true
            node.isIntuitiveLeap() -> deallocateIntuitiveLeap(node)
            node.isAllocated -> {
                node.isAllocated = false
                node.getActiveNodeIslands()
                        .forEach {
                            when {
                                it.node1.isIntuitiveLeap() -> deallocateIntuitiveLeap(it.node1)
                                it.node2.isIntuitiveLeap() -> deallocateIntuitiveLeap(it.node2)
                            }
                            it.allocate(false)
                        }
            }
            else -> graph.getShortestPathForAllocate(node).forEach { it.allocate(true) }
        }
    }

    private fun AbstractNode.getActiveNodeIslands(): List<Edge> {
        return connections
                .map { it.getSecondNode(this) }
                .filter { it.isAllocated }
                .mapNotNull(graph::getNodesIsland)
                .flatMap { it }
    }
}