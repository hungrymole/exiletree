package ru.hungrymole.pob.components.tree

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Rect
import android.util.SparseArray
import dagger.Module
import dagger.Provides
import okio.byteArray
import ru.hungrymole.pob.POEData
import ru.hungrymole.pob.components.loadGenerated
import ru.hungrymole.pob.components.tree.model.tree.Graph
import ru.hungrymole.pob.components.tree.model.tree.Group
import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData
import ru.hungrymole.pob.components.tree.model.tree.edges.Edge
import ru.hungrymole.pob.components.tree.model.tree.edges.Line
import ru.hungrymole.pob.components.tree.model.tree.edges.Orbit
import ru.hungrymole.pob.components.tree.model.tree.nodes.*
import ru.hungrymole.pob.components.tree.model.tree.nodes.ascendancy.*
import ru.hungrymole.pob.extensions.measure
import ru.hungrymole.pob.extensions.values
import ru.hungrymole.pob.generator.GeneratedJavaTree
import ru.hungrymole.pob.generator.ProtoNode
import ru.hungrymole.pob.generator.tree.TreeConstants
import java.util.*
import javax.inject.Singleton

/**
 * Created by Dinar Khusainov on 23.10.2016.
 */
@Module
class TreeModule {

    @Provides
    @Singleton
    fun loadGraph(ctx: Context): Graph = measure("Deserialize java tree") {
        val tree = GeneratedJavaTree.loadGenerated(ctx)

        val images = SparseArray<Bitmap>(tree.icons.size)
        tree.icons.mapValues {
            it.value.toByteArray()
            val bytes = it.value.byteArray
            images.append(it.key, BitmapFactory.decodeByteArray(bytes, 0, bytes.size))
        }

        val nodes = SparseArray<AbstractNode>(tree.nodes.count)
        fun Iterable<ProtoNode>.processNode(constructor: (TreeNodeData) -> AbstractNode) {
            forEach { n ->
                val info = TreeNodeData(n.x, n.y, n.id, images[n.unallocIcon], images[n.allocIcon])
                nodes.put(n.id, constructor(info))
            }
        }
        val classIds = TreeConstants.classNodeIdToClassId.toMap()
        tree.nodes.CLASS.forEach { n ->
            val info = TreeNodeData(n.x, n.y, n.id, images[n.unallocIcon], images[n.allocIcon])
            val classId = classIds[n.id]!!
            nodes.put(n.id, ClassNode(info, classId))
        }
        tree.nodes.JEWEL.forEach { n ->
            val info = TreeNodeData(n.x, n.y, n.id, images[n.unallocIcon], images[n.allocIcon])
            val jewels = mapOf(
                    "Crimson Jewel" to TreeConstants.JEWEL_ALLOC_CRIMSON_ICON_ID,
                    "Cobalt Jewel" to TreeConstants.JEWEL_ALLOC_COBALT_ICON_ID,
                    "Viridian Jewel" to TreeConstants.JEWEL_ALLOC_VIRIDIAN_ICON_ID,
                    "Prismatic Jewel" to TreeConstants.JEWEL_ALLOC_PRISMATIC_ICON_ID
            )
            val abyssJewels = POEData.abyssJewelTypes.map { it to TreeConstants.JEWEL_ALLOC_ABYSS_ICON_ID }

            val jewelIcons = (jewels + abyssJewels)
                .mapValues { images.get(it.value) }

            nodes.put(n.id, JewelNode(info, jewelIcons))
        }
        tree.nodes.NORMAL.processNode(::Node)
        tree.nodes.NOTABLE.processNode(::Notable)
        tree.nodes.MASTERY.processNode(::Mastery)
        tree.nodes.KEYSTONE.processNode(::Keystone)
        tree.nodes.ASC_NORMAL.processNode(::AscendancyNode)
        tree.nodes.ASC_NOTABLE.processNode(::AscendancyNotable)
        tree.nodes.ASC_START.processNode(::AscendancyStart)
        tree.nodes.ASC_SCION_MULTI_CHOICE.processNode(::ScionMultiChoice)
        tree.nodes.ASC_SCION_MULTI_CHOICE_OPTION.processNode(::ScionSingleChoice)
        tree.nodes.ASC_SCION_PATH_OF_X.processNode(::ScionPathOfX)

        val edges = ArrayList<Edge>(tree.lines.size + tree.arcs.size)
        val nodeEdges = mutableMapOf<AbstractNode, MutableList<Edge>>()
        tree.lines.forEach {
            val edge = Line(nodes[it.node1], nodes[it.node2])
            if (!edge.node1.isAsc && !edge.node2.isAsc) {
                edges.add(edge)
            }
            nodeEdges.getOrPut(edge.node1, ::ArrayList).add(edge)
            nodeEdges.getOrPut(edge.node2, ::ArrayList).add(edge)
        }
        tree.arcs.forEach {
            val n1 = nodes[it.node1]
            val edge = Orbit(n1, nodes[it.node2], it.x, it.y, it.start, it.sweep, it.radius)
            if (!edge.node1.isAsc && !edge.node2.isAsc) {
                edges.add(edge)
            }
            nodeEdges.getOrPut(edge.node1, ::ArrayList).add(edge)
            nodeEdges.getOrPut(edge.node2, ::ArrayList).add(edge)
        }
        nodeEdges.onEach { (node, connections) ->
            node.connections = connections
        }

        val groups = SparseArray<Group>(tree.groups.size)
        tree.groups.forEach {
            groups.append(it.id, Group(it.x, it.y, it.nodes.map(nodes::get), images[it.background]))
        }

        val ascendancies = tree.ascendanceis.map {
            val ascGroups = ArrayList<Group>(it.groups.size)
            it.groups.forEach {
                val group = groups.get(it)
                group.nodes.forEach { nodes.remove(it.id) }
                ascGroups.add(group)
                groups.remove(it)
            }
            AscendancyGraph(
                    it.name,
                    it.x,
                    it.y,
                    it.flavor,
                    ascGroups,
                    it.button,
                    images[TreeConstants.ASC_UNALLOC_BUTTON_ICON_ID],
                    images[TreeConstants.ASC_ALLOC_BUTTON_ICON_ID]
            )
        }.associateBy { it.name }
        val treeRect = Rect(0, 0, tree.right, tree.bottom)

        val loadImage = { ascName: String ->
            val mutable = BitmapFactory.Options().apply {
                inMutable = true
            }
            BitmapFactory.decodeStream(ctx.assets.open("asc/$ascName.png"), null, mutable)
        }
        return@measure Graph(nodes, nodes.values(), edges, groups.values(), ascendancies, treeRect, loadImage)
    }
}