package ru.hungrymole.pob.components.skills.group

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import org.luaj.vm2.Globals
import ru.hungrymole.pob.components.skills.GroupItemSlot
import ru.hungrymole.pob.components.skills.SearchableSkill
import ru.hungrymole.pob.components.skills.Skill
import ru.hungrymole.pob.components.skills.SkillGroup
import ru.hungrymole.pob.components.skills.group.search.GemRepository
import ru.hungrymole.pob.dialogs.*
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.*
import ru.hungrymole.pob.ui.list.ListViewModel
import javax.inject.Inject

/**
 * Created by khusainov.d on 18.07.2017.
 */
class SkillGroupVM @Inject constructor(
  private val lua: Globals,
  private val delegate: GemRepository,
  ctx: Context
) : ListViewModel<Skill>(ctx) {

    private val displayGroup get() = lua.getBy("build.skillsTab.displayGroup").checktable()
    private val groupSlotDropList get() = lua.getBy("build.skillsTab.controls.groupSlot.list").toList<GroupItemSlot>()

    val deleteSkill = PublishSubject.create<Int>()
    val nameChanges = PublishSubject.create<Pair<Int, SearchableSkill>>()
    val levelChanges = PublishSubject.create<Pair<Int, Int>>()
    val qualityChanges = PublishSubject.create<Pair<Int, Int>>()
    val enabledChanges = PublishSubject.create<Pair<Int, Boolean>>()

    override fun getState() = Single.fromCallable {
        var group = displayGroup.toObject<SkillGroup>()
        if (group.gemList.isEmpty() || group.gemList.last().nameSpec != "") {
            //there should always be one empty gem for editing at last position
            val newSlot = group.gemList.size + 1
            lua.getBy("build.skillsTab").selfCall("CreateGemSlot", newSlot)
            lua.getBy("build.skillsTab.controls.gemSlot${newSlot}Enable.changeFunc").call(true)
            group = displayGroup.toObject<SkillGroup>()
        }
        group.gemList
    }.luaSubscribe()

    override fun subscribeOnStart(subscriptions: CompositeDisposable) {
        //gems
        subscriptions += nameChanges.filter { it.first != -1 }.lua().doOnNext { (i, skill) ->
            val nameSpec = lua.getBy("build.skillsTab.controls.gemSlot${i + 1}Name")
            nameSpec["gemChangeFunc"].call(skill.gemId, false)
            nameSpec["dropped"] = false
        }.flatMapSingle { reloadState() }.subscribe()

        subscriptions += deleteSkill.filter { it != -1 }.lua().doOnNext { i ->
            lua.getBy("build.skillsTab.controls.gemSlot${i + 1}Delete.onClick()")
        }.flatMapSingle { reloadState() }.subscribe()

        subscriptions += levelChanges.filter { it.first != -1 }.lua().subscribe { (i, lvl) ->
            lua.getBy("build.skillsTab.controls.gemSlot${i + 1}Level.changeFunc").call("$lvl")
        }

        subscriptions += qualityChanges.filter { it.first != -1 }.lua().subscribe { (i, q) ->
            lua.getBy("build.skillsTab.controls.gemSlot${i + 1}Quality.changeFunc").call("$q")
        }

        subscriptions += enabledChanges.lua().filter { it.first != -1 }.subscribe { (i, enabled) ->
            lua.getBy("build.skillsTab.controls.gemSlot${i + 1}Enable.changeFunc").call(enabled)
        }
    }

    fun searchFilterGems(q: String, pos: Int): Single<List<SearchableSkill>> {
        if (pos == -1) return Single.just(emptyList())
        return delegate.query(q, pos)
    }

    fun getGroupConfig() = Single.fromCallable {
        val group = displayGroup.toObject<SkillGroup>()
        val selectedSlot = groupSlotDropList.indexOfFirst { it.slotName == group.slot } + 1
        listOf(
            TextConfigEntry("Name", group.label ?: ""),
            SpinnerConfigEntry(groupSlotDropList, selectedSlot).apply { label = "Socketed in" },
            CheckboxConfigEntry("Enabled", group.enabled))
    }.luaSubscribe()

    fun applyGroupConfig(it: ConfigResult) = Single.fromCallable {
        val label = it.byLabel<TextConfigEntry>("Name").text
        lua.getBy("build.skillsTab.controls.groupLabel.changeFunc").call(label)

        val slot = it.byLabel<SpinnerConfigEntry<GroupItemSlot>>("Socketed in").index + 1
        lua.getBy("build.skillsTab.controls.groupSlot").selfCall("SetSel", slot)

        val enabled = it.byLabel<CheckboxConfigEntry>("Enabled").value
        lua.getBy("build.skillsTab.controls.groupEnabled.changeFunc").call(enabled)
    }.luaSubscribe()

}