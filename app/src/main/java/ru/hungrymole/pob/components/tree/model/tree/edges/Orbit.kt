package ru.hungrymole.pob.components.tree.model.tree.edges

import android.graphics.Rect
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 14.01.15.
 */
class Orbit(
        node1: AbstractNode,
        node2: AbstractNode,
        val cx: Int,
        val cy: Int,
        val start: Float,
        val sweep: Float,
        val orbit: Float
) : Edge(node1, node2) {

    override fun addToDedicatedPath(scale: Float, region: Rect) {
        path.addArc(
                (cx - region.left - orbit) * scale,
                (cy - region.top - orbit) * scale,
                (cx - region.left + orbit) * scale,
                (cy - region.top + orbit) * scale,
                start,
                sweep
        )
    }
}
