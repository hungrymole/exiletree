package ru.hungrymole.pob.components

import io.reactivex.subjects.PublishSubject

    /**
 * Created by Dinar Khusainov on 06.02.2018.
 */

open class Event

typealias EventBus = PublishSubject<Event>