package ru.hungrymole.pob.components.items.search

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposables
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.activity_build.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.components.setTooltip
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.SEARCH_HIGHLIGHT_COLOR
import ru.hungrymole.pob.dialogs.ConfigDialog
import ru.hungrymole.pob.dialogs.rxConfirmDialog
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.inflate
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.generator.lua.SpanInfo
import ru.hungrymole.pob.ui.list.RecyclerFragment
import ru.hungrymole.pob.ui.list.RecyclerHolder
import ru.hungrymole.pob.ui.rx.plusAssign
import ru.hungrymole.pob.ui.view.SearchManager
import javax.inject.Inject


/**
 * Created by khusainov.d on 26.08.2017.
 */
class ItemSearchFragment : RecyclerFragment<Unique, ItemSearchVM, ItemSearchFragment.SearchItemHolder>() {

    @Inject lateinit var search: SearchManager
    @Inject override lateinit var vm: ItemSearchVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        act.di().inject(this)
    }

    override val fabImage = R.drawable.ic_filter_list_white_24dp

    override fun createHolder(viewType: Int, view: View) = SearchItemHolder(view)
    override fun createView(viewType: Int, parent: ViewGroup): View {
        return inflate(R.layout.item_unique_list_tooltip, parent)
    }

    override fun SearchItemHolder.subscribeHolder() {
        subscriptions += view
          .clicks()
          .flatMapMaybe {
              act
                .rxConfirmDialog("Add unique to build?")
                .filter { it }
                .doOnSuccess { vm.itemClicks.onNext(item) }
          }
          .subscribe()
    }

    override fun onStart(subscriptions: CompositeDisposable) {
        super.onStart(subscriptions)
        subscriptions += fab.clicks()
                .map { vm.getSearchFilters() }
                .flatMapMaybe { ConfigDialog(act, "Filter", it) }
                .doOnNext(vm::setSearchFilters)
                .subscribe()

        subscriptions += search
            .queryChanges
            .subscribe(vm.searchQueries::onNext)

        subscriptions += Disposables.fromAction(search::hide)

        subscriptions += search.visibilityChanges.subscribe { searchVisible ->
            act.build_tab_layout.visible = !searchVisible
        }
    }

    override fun updateState(state: List<Unique>) {
        items.clear()
        items.addAll(state)
        toggleEmptyState()
        adapter.notifyDataSetChanged()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_item_search, menu)
        this += menu.findItem(R.id.act_search_item)
            .clicks()
            .subscribe { search.show() }
    }

    override fun areContentsTheSame(old: Unique, new: Unique): Boolean {
        return old.tooltipMatches?.sumBy { it.hashCode() } == new.tooltipMatches?.sumBy { it.hashCode() }
    }

    override fun SearchItemHolder.bind(item: Unique) {
        val highlights = item
            .tooltipMatches
            .orEmpty()
            .map { SpanInfo(SEARCH_HIGHLIGHT_COLOR, it.start, it.endInclusive + 1) }

        vm.getTooltip(item)
            .also { it.highlights += highlights }
            .let { label.setTooltip(it) }
    }

    class SearchItemHolder(v: View) : RecyclerHolder(v) {
        val label = v as TextView
    }
}