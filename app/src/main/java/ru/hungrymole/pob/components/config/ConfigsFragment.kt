package ru.hungrymole.pob.components.config

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.View
import com.jakewharton.rxbinding2.support.v4.view.pageSelections
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.activity_build.*
import kotlinx.android.synthetic.main.fragment_view_pager.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.ctx
import ru.hungrymole.pob.extensions.intPref
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.ui.FabFragment
import javax.inject.Inject

/**
 * Created by khusainov.d on 07.08.2017.
 */
class ConfigsFragment : FabFragment<List<Section>, ConfigsVM>() {

    @Inject override lateinit var vm: ConfigsVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
    }

    private val sections = mutableListOf<Section>()
    private val tabLayout by lazy { act.build_tab_layout }

    override val layout = R.layout.fragment_view_pager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view_pager.adapter = SectionsAdapter(childFragmentManager, sections)
        tabLayout.apply {
            visible = true
            tabGravity = TabLayout.GRAVITY_CENTER
            tabMode = TabLayout.MODE_SCROLLABLE
            setupWithViewPager(view_pager)
        }
    }

    override fun onStart(subscriptions: CompositeDisposable) {
        var configTabPos by ctx.intPref(0)
        subscriptions += view_pager
          .pageSelections()
          .skip(1)
          .subscribe { configTabPos = it }
    }

    override fun updateState(state: List<Section>) {
        sections.clear()
        sections.addAll(state)
        sections.sortBy { it.order }
        view_pager.adapter?.notifyDataSetChanged()
        val configTabPos by ctx.intPref(0)
        val b = configTabPos
        view_pager.currentItem = configTabPos
    }

    override fun onDestroyView() {
        super.onDestroyView()
        tabLayout.apply {
            setupWithViewPager(null)
            visible = false
        }
    }


    private class SectionsAdapter(
      manager: FragmentManager,
      private val sections: List<Section>
    ) : FragmentStatePagerAdapter(manager) {
        override fun getItem(position: Int) = ConfigSectionFragment().apply {
            arguments = Bundle().also {
                it.putString("name", sections[position].name)
            }
        }

        override fun getPageTitle(position: Int) = sections[position].name
        override fun getCount() = sections.size
    }
}