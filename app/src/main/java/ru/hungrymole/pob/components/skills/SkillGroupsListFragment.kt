package ru.hungrymole.pob.components.skills

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.checkedChanges
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.item_edit_group.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.Router
import ru.hungrymole.pob.build.TooltipManager
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.dialogs.ConfigDialog
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.inflate
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.ui.list.RecyclerFragment
import ru.hungrymole.pob.ui.list.RecyclerHolder
import ru.hungrymole.pob.ui.rx.plusAssign
import javax.inject.Inject

/**
 * Created by khusainov.d on 17.07.2017.
 */
class SkillGroupsListFragment : RecyclerFragment<SkillGroup, SkillGroupsListVM, SkillGroupsListFragment.GroupHolder>() {

    @Inject override lateinit var vm: SkillGroupsListVM
    @Inject lateinit var router: Router
    @Inject lateinit var tooltipManager: TooltipManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
        setHasOptionsMenu(true)
    }

    override val fabImage = R.drawable.ic_add_white_24dp

    override fun onViewCreated(subscriptions: CompositeDisposable) {
        super.onViewCreated(subscriptions)
        subscriptions += fab.clicks().subscribe(vm.addGroup::onNext)
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_skill_groups, menu)

        this += menu
          .findItem(R.id.act_skills_settings)
          .clicks()
          .flatMapSingle { vm.getGroupConfig() }
          .main()
          .flatMapMaybe { ConfigDialog(act, "Skills settings", it) }
          .flatMapSingle(vm::applyGroupConfig)
          .subscribe()
    }

    override fun onSwiped(vh: GroupHolder, item: SkillGroup) {
        val i = vh.adapterPosition
        items.removeAt(i)
        adapter.notifyItemRemoved(i)
        vm.deleteGroup.onNext(i)
    }

    override fun canSwipe(vh: GroupHolder, item: SkillGroup) = item.source == null

    override fun areContentsTheSame(old: SkillGroup, new: SkillGroup): Boolean {
        return old.enabled == new.enabled
    }

    override fun createHolder(viewType: Int, view: View) = GroupHolder(view)
    override fun createView(viewType: Int, parent: ViewGroup): View {
        return inflate(R.layout.item_edit_group, parent)
    }

    override fun GroupHolder.subscribeHolder() {
        subscriptions += enabled.checkedChanges()
          .filterOnlyUserInput()
          .subscribe { vm.enabledChanges.onNext(adapterPosition to it) }

        subscriptions += view
          .clicks()
          .flatMapSingle { vm.editGroupIfPossible(adapterPosition) }
          .main()
          .subscribe { msg ->
              when {
                  msg.value != null -> tooltipManager.showToolTip(msg.value)
                  else              -> router.showDisplayGroup()
              }
          }
    }

    override fun GroupHolder.bind(group: SkillGroup) {
        label.text = group.displayLabel
        enabled.isChecked = group.enabled
    }

    class GroupHolder(v: View) : RecyclerHolder(v) {
        val label = view.edit_group_label
        val enabled = view.edit_group_enabled
    }
}