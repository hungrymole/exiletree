package ru.hungrymole.pob.components.skills.group

import android.os.Bundle
import android.support.v7.widget.AppCompatAutoCompleteTextView
import android.view.Menu
import android.view.MenuInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.view.focusChanges
import com.jakewharton.rxbinding2.widget.checkedChanges
import com.jakewharton.rxbinding2.widget.itemClickEvents
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.item_edit_skill.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.components.skills.Skill
import ru.hungrymole.pob.components.skills.group.search.SearchGemAdapter
import ru.hungrymole.pob.dialogs.ConfigDialog
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.color
import ru.hungrymole.pob.extensions.inflate
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.ui.list.RecyclerFragment
import ru.hungrymole.pob.ui.list.RecyclerHolder
import ru.hungrymole.pob.ui.rx.plusAssign
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by khusainov.d on 18.07.2017.
 */
class SkillGroupFragment : RecyclerFragment<Skill, SkillGroupVM, SkillGroupFragment.SkillHolder>() {

    @Inject override lateinit var vm: SkillGroupVM
    private var textColor: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
        setHasOptionsMenu(true)
        textColor = act.color(R.color.primaryDarkColor)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.fragment_skill_group, menu)

        this += menu.findItem(R.id.act_skill_group_settings).clicks()
            .flatMapSingle { vm.getGroupConfig() }
            .main()
            .flatMapMaybe { ConfigDialog(act, "Settings", it) }
            .flatMapSingle(vm::applyGroupConfig)
            .subscribe()
    }

    override fun createView(viewType: Int, parent: ViewGroup): View {
        return inflate(R.layout.item_edit_skill, parent)
    }

    override fun createHolder(viewType: Int, view: View) = SkillHolder(view).apply {
        view.edit_skill_name.let {
            it.setAdapter(SearchGemAdapter(act))

            //todo hack
            val v = if (it is AppCompatAutoCompleteTextView) it::class.java.superclass else it::class.java
            v.getDeclaredField("mThreshold").apply {
                isAccessible = true
                set(it, 0)
            }
        }
    }

    override fun areContentsTheSame(old: Skill, new: Skill): Boolean {
        return old.color == new.color &&
            old.level == new.level &&
            old.quality == new.quality &&
            old.enabled == new.enabled
    }

    override fun SkillHolder.bind(skill: Skill) {
        name.setTextColor(skill.color)
        name.setText(skill.nameSpec, false)
        enabled.isChecked = skill.enabled
        level.value = skill.level
        quality.value = skill.quality
    }

    override fun SkillHolder.subscribeHolder() {
        subscriptions += level.changes
            .filterOnlyUserInput()
            .debounce(500, TimeUnit.MILLISECONDS)
            .subscribe { vm.levelChanges.onNext(adapterPosition to it) }

        subscriptions += quality.changes
            .filterOnlyUserInput()
            .debounce(500, TimeUnit.MILLISECONDS)
            .subscribe { vm.qualityChanges.onNext(adapterPosition to it) }

        subscriptions += enabled.checkedChanges()
            .filterOnlyUserInput()
            .subscribe { vm.enabledChanges.onNext(adapterPosition to it) }

        //search
        var itemSelected: String? = null
        subscriptions += name.textChanges()
            .filterOnlyUserInput()
            .filter {
                val proceed = itemSelected == null
                if (!proceed) {
                    itemSelected = null
                }
                proceed
            }
            .debounce(250, TimeUnit.MILLISECONDS)
            .flatMapSingle { vm.searchFilterGems(it.toString(), adapterPosition) }
            .main()
            .subscribe { filter.items = it }

        subscriptions += name.itemClickEvents().map {
            val clicked = filter.items[it.position()]
            val skillName = clicked.name
            itemSelected = skillName
            item.nameSpec = skillName
            clicked
        }.subscribe { vm.nameChanges.onNext(adapterPosition to it) }
/**
        name.setOnDismissListener {
            if (itemSelected == null) {
                val firstSuggestion = filter.items.firstOrNull() ?: return@setOnDismissListener
                val skillName = if (!name.text.isNullOrEmpty()) firstSuggestion.name else ""
                name.setText(skillName, false)
                name.dismissDropDown()
                itemSelected = skillName
                item.nameSpec = skillName
                vm.nameChanges.onNext(adapterPosition to firstSuggestion)
            }
        }
        */

        subscriptions += name.focusChanges().subscribe {
            if (adapterPosition == -1) return@subscribe
            val color = if (it) textColor else item.color
            name.setTextColor(color)
        }
    }

    override fun canSwipe(vh: SkillHolder, item: Skill): Boolean {
        return vh.adapterPosition != items.lastIndex
    }

    override fun onSwiped(vh: SkillHolder, item: Skill){
        vm.deleteSkill.onNext(vh.adapterPosition)
    }

    class SkillHolder(view: View) : RecyclerHolder(view) {
        val name = view.edit_skill_name
        val level = view.edit_skill_level
        val quality = view.edit_skill_quality
        val enabled = view.edit_skill_enabled

        val filter by lazy { name.adapter as SearchGemAdapter }
    }
}