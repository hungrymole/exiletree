package ru.hungrymole.pob.components.impor

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import ru.hungrymole.pob.components.Event
import ru.hungrymole.pob.lua.KeyName
import ru.hungrymole.pob.lua.Table

/**
 * Created by Dinar Khusainov on 03.02.2018.
 */

@Parcelize
@SuppressLint("ParcelCreator")
class ImportInput(
    val accountName: String? = null
) : Parcelable


object ImportEvent : Event()

class ImportState(
    val accountName: String,
    val loading: Boolean,
    val needAuth: Boolean,
    val status: String?
)

data class SubmitImport(
    val accountName: String,
    val sessionId: String?
)

@Table
data class PoeCharacter(
    val league: String,
    val name: String,
    val level: Int,
    val classId: Int,
    val ascendancyClass: Int,
    @KeyName("class") val className: String
) {

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as PoeCharacter

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }
}