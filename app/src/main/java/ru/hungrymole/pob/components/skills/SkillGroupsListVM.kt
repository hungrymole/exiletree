package ru.hungrymole.pob.components.skills

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import org.luaj.vm2.Globals
import ru.hungrymole.pob.dialogs.ConfigResult
import ru.hungrymole.pob.dialogs.NumberConfigEntry
import ru.hungrymole.pob.dialogs.byLabel
import ru.hungrymole.pob.extensions.Optional
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.*
import ru.hungrymole.pob.ui.list.ListViewModel
import javax.inject.Inject

/**
 * Created by khusainov.d on 18.07.2017.
 */
class SkillGroupsListVM @Inject constructor(
  private val lua: Globals,
  ctx: Context
) : ListViewModel<SkillGroup>(ctx) {

    private val groupList get() = lua.getBy("build.skillsTab.controls.groupList").checktable()
    private val sourceNote get() = lua.getBy("build.skillsTab.controls.sourceNote").checktable()

    val deleteGroup = PublishSubject.create<Int>()
    val enabledChanges = PublishSubject.create<Pair<Int, Boolean>>()
    val addGroup = PublishSubject.create<Unit>()

    override fun getState() = Single.fromCallable {
        groupList["list"].toList<SkillGroup>()
    }.luaSubscribe()

    override fun subscribeOnStart(subscriptions: CompositeDisposable) {
        subscriptions += addGroup.lua().doOnNext {
            groupList.getBy("controls.new.onClick()")
        }.flatMapSingle { reloadState() }.subscribe()

        subscriptions += deleteGroup.lua().flatMapSingle {
            setDisplayGroup(it)
        }.doOnNext {
            groupList.getBy("controls.delete.onClick()")
        }.flatMapSingle { reloadState() }.subscribe()

        subscriptions += enabledChanges.lua().flatMapSingle { a ->
            setDisplayGroup(a.first).map { a.second }
        }.subscribe {
            lua.getBy("build.skillsTab.controls.groupEnabled.changeFunc").call(it)
        }
    }


    fun editGroupIfPossible(groupPos: Int) = setDisplayGroup(groupPos).map {
        val fromItem = sourceNote.getBy("shown()").optboolean(false)
        val tooltip by lazy {
            val tooltip = lua.getBy("new").call("Tooltip")
            sourceNote
              .getBy("label()")
              .checkjstring()
              .lines()
              .forEach { tooltip.selfCall("AddLine", 14, it) }
            return@lazy Tooltip(tooltip)
        }

        when {
            fromItem -> Optional.from(tooltip)
            else     -> Optional.empty()
        }
    }

    private fun setDisplayGroup(i: Int) = Single.fromCallable {
        val index = i + 1
        val group = groupList.get("list")[index]
        val current = lua.getBy("build.skillsTab.displayGroup")
        if (current != group) {
            groupList["selIndex"] = index
            groupList["selValue"] = group
            groupList.selfCall("OnSelect", index, group)
        }
    }.luaSubscribe()

     fun getGroupConfig() = Single.fromCallable {
         val skillTab = lua.getBy("build.skillsTab")
         val defaultGemLevel = skillTab["defaultGemLevel"].optint(20)
         val defaultGemQuality = skillTab["defaultGemQuality"].optint(0)
         listOf(
           NumberConfigEntry("Default gem level", 0..30, defaultGemLevel),
           NumberConfigEntry("Default gem quality", 0..63, defaultGemQuality)
         )
    }.luaSubscribe()

    fun applyGroupConfig(cfg: ConfigResult) = Single.fromCallable {
        val skillTab = lua.getBy("build.skillsTab")
        skillTab["defaultGemLevel"] = cfg
          .byLabel<NumberConfigEntry>("Default gem level")
          .value
        skillTab["defaultGemQuality"] = cfg
          .byLabel<NumberConfigEntry>("Default gem quality")
          .value
    }.luaSubscribe()
}