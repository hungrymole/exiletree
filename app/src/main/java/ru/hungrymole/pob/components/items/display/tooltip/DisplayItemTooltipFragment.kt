package ru.hungrymole.pob.components.items.display.tooltip

import android.os.Bundle
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_display_item_tooltip.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.components.setTooltip
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.lua.Tooltip
import ru.hungrymole.pob.ui.mvvm.ViewFragment
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 03.09.2017.
 */
class DisplayItemTooltipFragment : ViewFragment<Tooltip, DisplayItemTooltipVM>() {

    @Inject override lateinit var vm: DisplayItemTooltipVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
    }

    override val layout = R.layout.fragment_display_item_tooltip
    override fun onStart(subscriptions: CompositeDisposable) {
    }

    override fun updateState(state: Tooltip) {
        display_item_tooltip.setTooltip(state)
    }
}