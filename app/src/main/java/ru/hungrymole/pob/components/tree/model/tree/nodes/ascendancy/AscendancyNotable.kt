package ru.hungrymole.pob.components.tree.model.tree.nodes.ascendancy

import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData
import ru.hungrymole.pob.components.tree.model.tree.nodes.Notable

/**
 * Sample text
 * Created by dhusainov on 03.03.2016.
 */
open class AscendancyNotable(data: TreeNodeData) : Notable(data) {
    override val isAsc = true
}