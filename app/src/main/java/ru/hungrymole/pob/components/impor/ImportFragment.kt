package ru.hungrymole.pob.components.impor

import android.os.Bundle
import android.support.design.widget.Snackbar
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.fragment_import.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.di
import ru.hungrymole.pob.dialogs.ConfigDialog
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.extensions.toast
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.network.NetworkMonitor
import ru.hungrymole.pob.ui.mvvm.ViewFragment
import timber.log.Timber
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 03.02.2018.
 */
class ImportFragment : ViewFragment<ImportState, ImportVM>() {

    companion object {
        const val INPUT = "input"
    }

    @Inject lateinit var network: NetworkMonitor
    @Inject override lateinit var vm: ImportVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        act.di().inject(this)
    }

    override val layout = R.layout.fragment_import

    override fun onViewCreated(subscriptions: CompositeDisposable) {
        super.onViewCreated(subscriptions)
        arguments
            ?.getParcelable<ImportInput>(INPUT)
            ?.let(vm::setInput)
    }

    private lateinit var state: ImportState
    override fun updateState(state: ImportState) {
        this.state = state
        import_status.text = state.status
        import_account_name.setText(state.accountName)
        import_progress.visible = state.loading
        import_start.isEnabled = !state.loading
        toggleAuth(state.needAuth)
    }

    private fun toggleAuth(needAuth: Boolean) {
        import_session.visible = needAuth
        import_login.visible = needAuth
    }

    private val noInternet by lazy { Snackbar.make(view!!, "No internet", Snackbar.LENGTH_INDEFINITE) }
    override fun onStart(subscriptions: CompositeDisposable) {
        subscriptions += import_start.clicks()
            .subscribe {
                val accountName = import_account_name.text?.toString() ?: ""
                val sessionId = import_session.text?.toString()?.takeIf { state.needAuth }
                vm.startImport.onNext(SubmitImport(accountName, sessionId))
            }
        subscriptions += import_login.clicks()
            .flatMapMaybe { ConfigDialog(act, "Path Of Exile Login", vm.getConfigs()) }
            .flatMapSingle { vm.applyConfig(it) }
            .main()
            .subscribeBy(Timber::e) {
                act.toast(it)
            }
        subscriptions += network.updates
            .main()
            .subscribe {
                if (it) noInternet.dismiss() else noInternet.show()
                import_start.isEnabled = it
                import_login.isEnabled = it
            }
    }

    override fun onDestroy() {
        super.onDestroy()
        noInternet.dismiss()
    }
}