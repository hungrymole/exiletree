package ru.hungrymole.pob.components.items.display.tooltip

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import org.luaj.vm2.Globals
import ru.hungrymole.pob.lua.Tooltip
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.ui.mvvm.ViewModel
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 03.09.2017.
 */
class DisplayItemTooltipVM @Inject constructor(
  private val lua: Globals,
  ctx: Context
) : ViewModel<Tooltip>(ctx) {
    override fun getState() = Single.fromCallable {
        lua.getBy("build.itemsTab.displayItemTooltip").let(::Tooltip)
    }

    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {}
    override fun subscribeOnStart(subscriptions: CompositeDisposable) {}
}