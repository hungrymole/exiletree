package ru.hungrymole.pob.components.items.display

import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.support.v4.view.pageSelections
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.activity_build.*
import kotlinx.android.synthetic.main.fragment_view_pager.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.components.items.display.edit.DisplayItemEditFragment
import ru.hungrymole.pob.components.items.display.tooltip.DisplayItemTooltipFragment
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.ui.FabFragment
import ru.hungrymole.pob.ui.rx.RxFragment

/**
 * Created by khusainov.d on 09.10.2017.
 */
class DisplayItemTabFragment : RxFragment() {

    val tabLayout by lazy { act.build_tab_layout }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_view_pager, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        view_pager.adapter = Adapter(childFragmentManager, tabs)

        tabLayout.apply {
            visible = true
            tabGravity = TabLayout.GRAVITY_FILL
            tabMode = TabLayout.MODE_FIXED
            setupWithViewPager(view_pager)
        }
    }

    override fun onViewCreated(subscriptions: CompositeDisposable) {
        subscriptions += view_pager.pageSelections().subscribe { i ->
            childFragmentManager.fragments.forEach {
                if (it is FabFragment<*, *>) {
                    it.hideFab()
                }
            }

            val current = childFragmentManager.fragments.firstOrNull { it.javaClass == tabs[i].second().javaClass }
            if (current != null && current is FabFragment<*, *>) {
                current.showFab()
            }
        }
    }

    override fun onStart(subscriptions: CompositeDisposable) {}

    override fun onDestroyView() {
        super.onDestroyView()
        tabLayout.apply {
            setupWithViewPager(null)
            visible = false
        }
    }

    private val tabs = listOf(
      "Tooltip" to ::DisplayItemTooltipFragment,
      "Edit" to ::DisplayItemEditFragment
    )

    private class Adapter(
      manager: FragmentManager,
      private val tabs: List<Pair<String, () -> Fragment>>
    ) : FragmentStatePagerAdapter(manager) {
        override fun getCount() = tabs.size
        override fun getItem(position: Int) = tabs[position].second()
        override fun getPageTitle(position: Int) = tabs[position].first
    }
}