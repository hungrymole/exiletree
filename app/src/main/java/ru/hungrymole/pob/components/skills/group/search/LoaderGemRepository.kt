package ru.hungrymole.pob.components.skills.group.search

import dagger.Lazy
import io.reactivex.Single
import ru.hungrymole.pob.components.skills.SearchableSkill
import ru.hungrymole.pob.extensions.filterAndAdd
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.interloop.loaders.SkillInfo
import ru.hungrymole.pob.lua.interloop.loaders.SkillLoader

/**
 * Created by khusainov.d on 19.09.2017.
 */
@Deprecated("...")
class LoaderGemRepository(
    private val loader: Lazy<SkillLoader>
) : GemRepository {

    private val searchable by lazy { loader.get().searchable }

    override fun query(q: String, pos: Int) = Single.fromCallable {
        val result = mutableListOf<SkillInfo>()
        searchable.filterAndAdd(result) {
            it.nameSpec.split(" ")
                    .joinToString("") { it[0].toString() }
                    .equals(q, true)//Simple abbreviation ("CtF" -> "Cold to Fire")
        }.filterAndAdd(result) {
            it.nameSpec.contains(q, true)//Contains //Exact match (case-insensitive)
        }
        emptyList<SearchableSkill>()
//        result.map { SearchableSkill(it.nameSpec, it.color ?: Color.BLACK) }
    }.luaSubscribe()
}