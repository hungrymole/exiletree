package ru.hungrymole.pob.components.impor.list

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import org.luaj.vm2.Globals
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.components.impor.ImportEvent
import ru.hungrymole.pob.components.impor.PoeCharacter
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.*
import ru.hungrymole.pob.ui.list.ListViewModel
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 04.02.2018.
 */
class ImportCharactersVM @Inject constructor(
  private val lua: Globals,
  private val eventBus: EventBus,
  ctx: Context
) : ListViewModel<PoeCharacter>(ctx) {

  companion object {
    private val defaultLeagues = listOf("Standard", "Hardcore", "Void")
  }

  private fun getChars(): Single<List<PoeCharacter>> = Single.fromCallable {
    lua
      .getBy("build.importTab.controls.charSelect.list")
      .arrayValues()
      .map { it["char"].toObject<PoeCharacter>() }
  }.luaSubscribe()

  override fun getState(): Single<List<PoeCharacter>> = getChars().map { chars ->
    val (asc, normal) = chars
      .sortedByDescending { it.level }
      .partition { it.ascendancyClass != 0 }
    val (old, currLeague) = asc.partition { it.league in defaultLeagues }
    currLeague + old + normal
  }

  fun import(char: PoeCharacter): Single<String> = getChars().map { chars ->
    val luaPos = chars.indexOf(char)
    when {
      luaPos == -1 -> "Char not found"
      else         -> import(luaPos)
    }
  }

    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {
        subscriptions += eventBus
            .ofType<ImportEvent>()
            .debounce(400, TimeUnit.MILLISECONDS)
            .flatMapSingle { reloadState() }
            .subscribe()

    }

    private fun import(pos: Int): String {
        lua.getBy("build.importTab.controls.charImportItemsClearSkills").set("state", true)
        lua.getBy("build.importTab.controls.charImportItemsClearItems").set("state", true)
        lua.getBy("build.importTab.controls.charImportTreeClearJewels").set("state", true)
        lua.getBy("build.importTab.controls.charSelect").selfCall("SetSel", pos + 1)
        try {
            lua.getBy("build.importTab").selfCall("DownloadPassiveTree")
        } catch (e: Throwable) {
            Timber.e(e)
            return "Tree import failed"
        }
        try {
            lua.getBy("build.importTab").selfCall("DownloadItems")
        } catch (e: Throwable) {
            Timber.e(e)
            return "Items import failed"
        }
        return "Import successful"
    }
}