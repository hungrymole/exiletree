package ru.hungrymole.pob.components.items.search

/**
 * Created by khusainov.d on 26.08.2017.
 */

enum class QueryType {
    Anywhere,
    Names,
    Modifiers
}

class Unique(
    val id: Int,
    val tooltipMatches: List<IntRange>? = null
) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Unique

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id
    }

}