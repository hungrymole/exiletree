package ru.hungrymole.pob.components.general

import io.reactivex.rxkotlin.ofType
import io.reactivex.subjects.BehaviorSubject
import org.luaj.vm2.Globals
import ru.hungrymole.pob.build.BuildRefreshEvent
import ru.hungrymole.pob.common.formatted
import ru.hungrymole.pob.common.isDecimal
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.extensions.rx.computation
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.lua.arrayValues
import ru.hungrymole.pob.lua.getBy
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Dinar Khusainov on 31.12.2017.
 */

@Singleton
class BuildAttrsModel @Inject constructor(
    private val lua: Globals,
    private val eventBus: EventBus
) {
    private val SPACE = ' '
    private val PERCENT = "%"
    private val NON_BREAKING_SPACE = '\u00A0'

    val statMap = BehaviorSubject.create<BuildAttrs>().apply {
        onNext(mutableListOf())
    }

    val currentStat get() = statMap.value ?: error("WTF")

    init {
        eventBus
            .ofType<BuildRefreshEvent>()
            .lua()
            .map { getAttrs() }
            .subscribe(statMap::onNext)
    }

    private fun getAttrs(): BuildAttrs {
        val attrs = mutableListOf<Pair<String, String>>()
        lua.getBy("build.controls.statBox.list")
            .arrayValues()
            .forEach {
                val name = it[1].optjstring(null) ?: return@forEach
                val value = it[2].tostring().optjstring("")
                val formattedValue = when {
                    value.endsWith(PERCENT) -> value.dropLast(1).toDouble().formatted + PERCENT
                    value.isDecimal() -> value.toDouble().formatted
                    else -> value
                }
                attrs += name to formattedValue
            }
        return attrs
    }
}