package ru.hungrymole.pob.components.tree.model.tree.nodes.ascendancy

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import kdbush.int32.KDBushIntFactory
import ru.hungrymole.pob.components.tree.model.tree.Group
import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.ascTextPaint
import ru.hungrymole.pob.components.tree.model.tree.edges.Edge
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode
import ru.hungrymole.pob.extensions.toSparseArray
import ru.hungrymole.pob.generator.tree.AscendancyButton
import ru.hungrymole.pob.generator.tree.AscendancyText
import ru.hungrymole.pob.generator.tree.TreeConstants

/**
 * Sample text
 * Created by dhusainov on 03.03.2016.
 */
class AscendancyGraph(
        val name: String,
        private val x: Int,
        private val y: Int,
        private val text: AscendancyText,
        private val groups: List<Group>,
        info: AscendancyButton,
        buttonUnalloc: Bitmap,
        buttonAlloc: Bitmap) {

    val nodes = groups.flatMap { it.nodes }

    val edges = nodes
            .flatMap { it.connections }
            .distinct()
            .filter { it.node1.isAsc && it.node2.isAsc }

    val nodeIndexes = nodes
        .groupBy { it.allocIcon.width }
        .mapValues { KDBushIntFactory.fromPoints(it.value) }
        .toSparseArray()

    val edgeIndexes = edges
            .filterNot(Edge::invisible)
            .let(KDBushIntFactory::fromPoints)
            .let { Drawing.EDGE_DRAWING_OFFSET to it }
            .let(::mapOf)
            .toSparseArray()

    val button = TreeNodeData(info.x, info.y, 0, buttonUnalloc, buttonAlloc)
        .let { AscendancyButton(info.angle, it) }

    fun press(x: Int, y: Int, scale: Float): AbstractNode? {
        return when {
            button.contains(x, y, scale) -> button
            !button.isAllocated          -> null
            else                         -> nodes.find { it.contains(x, y, scale) }
        }
    }

    private val rect = Rect()
    fun canDraw(region: Rect): Boolean {
        val bg = background ?: return false
        val halfSize = bg.width / 2
        rect.set(x - halfSize, y - halfSize, x + halfSize, y + halfSize)
        return button.isAllocated && Rect.intersects(rect, region)
    }

    fun reset() = nodes.forEach { it.isAllocated = false }

    fun attach(img: Bitmap) {
        val paint = ascTextPaint.also { it.color = text.color }
        val textLeft = text.left / TreeConstants.RESIZE
        val textTop = text.top / TreeConstants.RESIZE
        val canvas = Canvas(img)
        var i = 0F
        for (line in text.text.split("\n")) {
            canvas.drawText(line, textLeft, textTop + i, paint)
            i += 20
        }
        background = img
    }

    fun detach() {
        background?.recycle()
        background = null
        nodes.forEach { it.isAllocated = false }
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as AscendancyGraph

        if (name != other.name) return false

        return true
    }

    override fun hashCode(): Int {
        return name.hashCode()
    }

    private var background: Bitmap? = null
    private val matrix = Drawing.mainThreadMatrix
    private val bitmapDrawPaint = Drawing.bitmapDrawPaint
    fun drawBackground(canvas: Canvas, scale: Float, region: Rect) {
        val img = background ?: return
        val centerX = (x - region.left) * scale
        val centerY = (y - region.top) * scale
        matrix.reset()
        matrix.postTranslate(centerX - (img.width / 2), centerY - (img.height / 2))
        matrix.postScale(scale, scale, centerX, centerY)
        canvas.drawBitmap(background, matrix, bitmapDrawPaint)
    }
}
