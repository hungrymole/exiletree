package ru.hungrymole.pob.components.tree.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.*;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.Scroller;

import io.reactivex.subjects.PublishSubject;
import kotlin.Pair;
import ru.hungrymole.pob.components.tree.model.tree.Graph;
import ru.hungrymole.pob.components.tree.model.tree.drawing.GraphDrawer;
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode;
import ru.hungrymole.pob.components.tree.model.tree.nodes.ClassNode;
import ru.hungrymole.pob.components.tree.model.tree.nodes.ascendancy.AscendancyButton;

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 05.12.14.
 */
public class TreeView extends View
		implements ScaleGestureDetector.OnScaleGestureListener, GestureDetector.OnGestureListener {

	public static final float MAX_TREE_SCALE = 0.4f;
	public static final float MIN_TREE_SCALE = 5f;


	private GestureDetector listener;
	private ScaleGestureDetector scaleListener;
	private Scroller scroller;
	private float scale;
	private AbstractNode pressedNode;
	private boolean scrolled;
	private final RectF region = new RectF();
	private static final RectF subRect = new RectF();
	private SharedPreferences preferences;

	private Graph graph;
	public void setGraph(Graph graph) {
		this.graph = graph;
	}

	public Graph getGraph() {
		return graph;
	}

	public TreeView(Context context, AttributeSet attrs) {
		super(context, attrs);
		preferences = context.getSharedPreferences("savedParams", Context.MODE_PRIVATE);
        scale = preferences.getFloat("regionScale", 1.0f);
		listener = new GestureDetector(context, this);
		scaleListener = new ScaleGestureDetector(context, this);
		scroller = new Scroller(context);
	}

	@Override
	protected void onSizeChanged(int w, int h, int oldw, int oldh) {
		super.onSizeChanged(w, h, oldw, oldh);
		float x = preferences.getFloat("regionCenterX", 0F);
		float y = preferences.getFloat("regionCenterY", 0F);
		if (x == 0F || y == 0F) {
			ClassNode center = graph.getExile();
			region.set(center.getX() - w / scale / 2, center.getY() - h / scale / 2,
						center.getX() + w / scale / 2, center.getY() + h / scale / 2);
		}
		else {
			subRect.set(this.region);
			subRect.set(x - w / scale / 2, y - h / scale / 2, x + w / scale / 2, y + h / scale / 2);
			fitSubRectInTree(subRect);
			region.set(subRect);
		}
	}

	public void saveInstance() {
		SharedPreferences.Editor editor = preferences.edit();
		editor.putFloat("regionCenterX", region.centerX());
		editor.putFloat("regionCenterY", region.centerY());
		editor.putFloat("regionScale", scale);
		editor.apply();
	}

	@Override
	protected void onDetachedFromWindow() {
		super.onDetachedFromWindow();
		saveInstance();
	}

	@Override
	public boolean onTouchEvent(MotionEvent event) {
		listener.onTouchEvent(event);
		scaleListener.onTouchEvent(event);
		return true;
	}

	@Override
	protected void onDraw(Canvas canvas) {
		graph.getDrawer().drawRegion(canvas, region);
	}

    ///	//draw 66% of calls
    long counter = 2;
    public void redraw() {
//        if (counter++ % 3 != 0) {
			invalidate();
//        }
    }

    private boolean scaleRegion(float scale) {
		scale = 1F / scale;
		subRect.set(region);
		if (scale != 1.0f) {
			float width = (subRect.width() - scale * subRect.width()) / 2;
			float height = (subRect.height() - scale * subRect.height()) / 2;
			subRect.left = subRect.left + width;
			subRect.top = subRect.top + height;
			subRect.right = subRect.right - width;
			subRect.bottom = subRect.bottom - height;
		}
		scale = getHeight() / subRect.height();
		if (scale < MIN_TREE_SCALE && scale > MAX_TREE_SCALE) {
			fitSubRectInTree(subRect);
			region.set(subRect);
			this.scale = scale;
			return true;
		}
		else {
			return false;
		}
	}

	private void fitSubRectInTree(RectF subRect) {
		RectF treeSize = graph.getTreeSize();
		if (!treeSize.contains(subRect)) {
			float xOffset = treeSize.left > subRect.left ? treeSize.left - subRect.left : 0 +
				treeSize.right < subRect.right ? treeSize.right - subRect.right : 0;
			float yOffset = treeSize.top > subRect.top ? treeSize.top - subRect.top : 0 +
				treeSize.bottom < subRect.bottom ? treeSize.bottom - subRect.bottom : 0;
			subRect.offset(xOffset, yOffset);
		}
	}

	private boolean offsetRegion(float xOffset, float yOffset) {
		subRect.set(region);
		subRect.offset(xOffset / scale, yOffset / scale);
		if (graph.getTreeSize().contains(subRect)) {
			region.set(subRect);
			return true;
		}
		else {
			return false;
		}
	}

	@Override
	public boolean onScale(ScaleGestureDetector detector) {
		if (scaleRegion(detector.getScaleFactor())) {
			invalidate();
		}
		return true;
	}

	@Override
	public boolean onScaleBegin(ScaleGestureDetector detector) {
		return true;
	}

	@Override
	public void onScaleEnd(ScaleGestureDetector detector) {
        invalidate();
	}

	@Override
	public void onShowPress(MotionEvent e) {
		if (pressedNode != null && !pressedNode.isClass() && !(pressedNode instanceof AscendancyButton)) {
//			description = new NodeDescription(pressedNode);
			invalidate();
		}
	}

	@Override
	public boolean onDown(MotionEvent e) {
		scroller.forceFinished(true);
		scrolled = false;
		pressedNode = graph.getPressedNode(e.getX(), e.getY(), this, region);
		return true;
	}

	final PublishSubject<AbstractNode> nodeClicks = PublishSubject.create();
	final PublishSubject<AbstractNode> nodeLongPresses = PublishSubject.create();

	@Override
	public boolean onSingleTapUp(MotionEvent e) {
		if (pressedNode != null && !scrolled) {
			nodeClicks.onNext(pressedNode);
            pressedNode = null;
		}
		return true;
	}

	@Override
	public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {
		scrolled = true;
		if (offsetRegion(distanceX, distanceY)) {
			redraw();
		}
		return true;
	}

	@Override
	public void onLongPress(MotionEvent e) {
        Pair<Float, Float> point = new Pair<>(e.getRawX(), e.getRawY());
        if (pressedNode != null) {
            nodeLongPresses.onNext(pressedNode);
        }
    }

	@Override
	public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
		scrolled = true;
		scroller.forceFinished(true);
		scroller.fling(((int)e1.getX()), ((int)e1.getY()), ((int)(velocityX / 2)), ((int)(velocityY / 2)), -100000,
				100000, -100000, 100000);
		Runnable fling = new Runnable() {
			int lastX = scroller.getStartX();
			int lastY = scroller.getStartY();

			@Override
			public void run() {
				if (!scroller.isFinished()) {
					scroller.computeScrollOffset();
					int currX = scroller.getCurrX();
					int currY = scroller.getCurrY();
					offsetRegion(lastX - currX, lastY - currY);
					lastX = currX;
					lastY = currY;
					invalidate();
					redraw();
					post(this);
				} else {
                    invalidate();
                }
			}
		};
		post(fling);
		return true;
	}

	private static Paint notificationPaint = new Paint();
	private static TextPaint textPaint = new TextPaint();
	private static TextPaint namePaint = new TextPaint();

	static {
		notificationPaint.setColor(Color.parseColor("#B0000000"));
		notificationPaint.setAntiAlias(true);

		namePaint.setColor(Color.parseColor("#BAFFFFFF"));
		namePaint.setTextSize(30);
		namePaint.setAntiAlias(true);

		textPaint.setColor(Color.parseColor("#B0FFFFFF"));
		textPaint.setTextSize(25);
		textPaint.setAntiAlias(true);

	}
}

