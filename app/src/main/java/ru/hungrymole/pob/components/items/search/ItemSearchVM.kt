package ru.hungrymole.pob.components.items.search

import android.content.Context
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import org.luaj.vm2.Globals
import ru.hungrymole.pob.db.Reference
import ru.hungrymole.pob.dialogs.BaseConfigEntry
import ru.hungrymole.pob.dialogs.ConfigResult
import ru.hungrymole.pob.dialogs.SpinnerConfigEntry
import ru.hungrymole.pob.dialogs.spinner
import ru.hungrymole.pob.extensions.rx.ioSubscribe
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.extensions.toast
import ru.hungrymole.pob.lua.Tooltip
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.lua.selfCall
import ru.hungrymole.pob.ui.list.ListViewModel
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by khusainov.d on 26.08.2017.
 */
class ItemSearchVM @Inject constructor(
  ctx: Context,
  private val lua: Globals,
  private val search: UniqueSearch
) : ListViewModel<Unique>(ctx) {

    val searchQueries = PublishSubject.create<String>()
    val itemClicks = PublishSubject.create<Unique>()

    override fun getState() = search
        .filterUniques()
        .ioSubscribe()

    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {
        super.subscribeOnCreate(subscriptions)
        subscriptions += searchQueries
            .debounce(500, TimeUnit.MILLISECONDS)
            .doOnNext { search.query = it.trim() }
            .flatMapSingle { reloadState() }
            .subscribe()

        subscriptions += itemClicks
            .lua()
            .map(::addItem)
            .main()
            .subscribe {
                val text = if (it) "Unique added to build" else "Failed to parse unique"
                ctx.toast(text)
            }
    }

    private fun addItem(unique: Unique): Boolean = try {
        val raw = search.getRaw(unique)
        val itemTab = lua.getBy("build.itemsTab")
        itemTab.selfCall("CreateDisplayItemFromRaw", raw, true)
        itemTab.selfCall("AddDisplayItem")
        true
    } catch (e: Exception) {
        Timber.e(e)
        false
    }

    private val tooltips = mutableMapOf<Unique, Tooltip>()
    fun getTooltip(unique: Unique): Tooltip {
        return tooltips.getOrPut(unique) { search.getTooltip(unique) }
    }

    fun getSearchFilters(): List<BaseConfigEntry> {
        return listOf(
            SpinnerConfigEntry("Search in", QueryType.values().toList(), search.queryType),
            SpinnerConfigEntry("League", search.leagues, search.league),
            SpinnerConfigEntry("Type", search.itemTypes, search.itemType)
        )
    }

    fun setSearchFilters(filters: ConfigResult) {
        search.queryType = filters.spinner<QueryType>("Search in").value
        search.league = filters.spinner<Reference>("League").value
        search.itemType = filters.spinner<Reference>("Type").value
        reloadState().subscribe()
    }

    override fun onDestroy() {
        super.onDestroy()
        tooltips.clear()
    }
}