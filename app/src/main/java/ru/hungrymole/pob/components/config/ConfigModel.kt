package ru.hungrymole.pob.components.config

import ru.hungrymole.pob.lua.HasTableKey
import ru.hungrymole.pob.lua.KeyName
import ru.hungrymole.pob.lua.Table

/**
 * Created by khusainov.d on 07.08.2017.
 */

@Table
open class Section(@KeyName("section") val name: String,
                            val order: Int)

@Table
open class ConfigItem(val label: String) : HasTableKey()

open class ConfigItemWithValue<VT : Any>(val id: String,
                                                  val type: String,
                                                  label: String) : ConfigItem(label) {
    lateinit var value: VT
    override fun equals(other: Any?): Boolean {
        if (other !is ConfigItemWithValue<*>) return false

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }
}

@Table
class ConfigItemCheckbox(@KeyName("var") id: String,
                                  label: String,
                                  type: String) : ConfigItemWithValue<Boolean>(id, type, label)

@Table
class ConfigItemNumber(@KeyName("var") id: String,
                                label: String,
                                type: String) : ConfigItemWithValue<Int>(id, type, label)

@Table
class ConfigItemSpinner(@KeyName("var") id: String,
                                 label: String,
                                 type: String,
                                 val list: List<ConfigSpinnerItem>) : ConfigItemWithValue<ConfigSpinnerItem>(id, type, label)

@Table
class SectionItems(val labels: List<ConfigItem>,
                            val checkboxes: List<ConfigItemCheckbox>,
                            val numbers: List<ConfigItemNumber>,
                            val spinners: List<ConfigItemSpinner>)

@Table
class ConfigSpinnerItem(val label: String,
                                 @KeyName("val") val value: String) {

    override fun toString() = label

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ConfigSpinnerItem) return false

        if (label != other.label) return false

        return true
    }

    override fun hashCode(): Int {
        return label.hashCode()
    }
}

var ConfigItemSpinner.selected
    get() = list.indexOf(value)
    set(index) {
        value = list[index]
    }

val CONFIG_CHECKBOX = 0
val CONFIG_SPINNER = 1
val CONFIG_NUMBER = 2
val CONFIG_EMPTY = 3