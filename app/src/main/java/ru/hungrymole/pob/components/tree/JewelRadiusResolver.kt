package ru.hungrymole.pob.components.tree

import android.graphics.Color.*

/**
 * Created by khusainov.d on 06.09.2017.
 */
private val intJewels = listOf(
        "Anatomical Knowledge",
        "Eldritch Knowledge",
        "Careful Planning",
        "Efficient Training")

private val strJewels = listOf(
        "Brute Force Solution",
        "Fluid Motion",
        "Spire of Stone")

private val dexJewels = listOf(
        "Fertile Mind",
        "Inertia",
        "Static Electricity",
        "Pugilist")

private val transform = listOf(
        "Energy From Within" to (listOf("maximum Life") to BLUE + DKGRAY),
        "Healthy Mind" to (listOf("maximum Life") to BLUE + MAGENTA),
        "Energised Armour" to (listOf("maximum energy Shield") to BLUE + MAGENTA),

        "The Red Dream" to (listOf("Fire Res", "Elemental Res") to RED),
        "The Red Nightmare" to (listOf("Fire Res", "Elemental Res") to RED + BLACK),
        "The Green Dream" to (listOf("Cold Res", "Elemental Res") to GREEN),
        "The Green Nightmare" to (listOf("Cold Res", "Elemental Res") to GREEN + BLACK),
        "The Blue Dream" to (listOf("Lightning Res", "Elemental Res") to BLUE),
        "The Blue Nightmare" to (listOf("Lightning Res", "Elemental Res") to BLUE + BLACK),

        "Fireborn" to (listOf(
                "Physical Damage",
                "Cold Damage",
                "Lightning Damage",
                "Chaos Damage") to RED + MAGENTA),
        "Lioneye's Fall" to (listOf(
                "Dagger",
                "Mace",
                "Staff",
                "Axe",
                "Claw",
                "Sword",
                "Melee Crit",
                "One Handed",
                "Two Handed") to GRAY))
        .toMap()

private val matcher: (List<String>, List<NodeInfo>) -> List<NodeInfo> = { keywords, nodes ->
    nodes.filter {
        it.sd.firstOrNull { attribute ->
            keywords.firstOrNull { attribute.contains(it) } != null
        } != null
    }
}

const val INTUITIVE_LEAP = "Intuitive Leap"
fun getAffectedNodesAndColor(jewel: SocketedJewel,
                                      nodes: List<NodeInfo>): Pair<List<NodeInfo>, Int> {
    val name = jewel.title
    return when {
        name == INTUITIVE_LEAP -> nodes to MAGENTA
        name == "Inspired Learning" -> nodes.filter { it.type == "notable" } to MAGENTA
        transform[name] != null -> transform[name]!!.let { matcher(it.first, nodes) to it.second }
        name in strJewels || jewel.raw.contains("With at least 40 Str") -> matcher(listOf("to Str"), nodes) to RED
        name in dexJewels || jewel.raw.contains("With at least 40 Dex") -> matcher(listOf("to Dex"), nodes) to GREEN
        name in intJewels || jewel.raw.contains("With at least 40 Int") -> matcher(listOf("to Int"), nodes) to BLUE
        else -> emptyList<NodeInfo>() to 0
    }
}