package ru.hungrymole.pob.components.items.items

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.plusAssign
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.build.BuildRefreshEvent
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.components.items.BuildItem
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.Tooltip
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.lua.selfCall
import ru.hungrymole.pob.lua.toList
import ru.hungrymole.pob.ui.list.ListViewModel
import javax.inject.Inject

/**
 * Created by khusainov.d on 11.08.2017.
 */
class BuildItemsVM @Inject constructor(
  private val lua: Globals,
  private val eventBus: EventBus,
  ctx: Context
) : ListViewModel<BuildItem>(ctx) {

    private val itemList get() = lua.getBy("build.itemsTab.controls.itemList")

    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {
      /*
        subscriptions += Single
                .fromCallable { lua.getBy("build.itemsTab").selfCall("SortItemList") }
                .luaSubscribe()
                .subscribe()
      */
        subscriptions += eventBus
          .ofType<BuildRefreshEvent>()
          .lua()
          .flatMapSingle { reloadState() }
          .subscribe()
    }

  override fun getState(): Single<List<BuildItem>> = Single.fromCallable {
    lua
      .getBy("build.itemsTab.items")
      .toList<BuildItem>()
      .onEach { item ->
        item.tooltip = itemList
          .selfCall("GetRowValue", 1, LuaValue.NIL, item.id)
          .checkjstring()
          .let(Tooltip.Companion::buildSingleLine)
      }
  }.luaSubscribe()

  fun getItemTooltip(item: BuildItem) = Single.fromCallable<Tooltip> {
    val luaItem = lua.getBy("build.itemsTab.items.[${item.id}]")
    val luaTooltip = lua.getBy("new").call("Tooltip")
    lua
      .getBy("build.itemsTab")
      .selfCall("AddItemTooltip", luaTooltip, luaItem)
    Tooltip(luaTooltip)
  }.luaSubscribe()

  fun setDisplayItem(item: BuildItem) = Single.fromCallable<Unit> {
    lua
      .getBy("build.itemsTab.controls.itemList")
      .selfCall("OnSelClick", 0, item.id, true)
  }.luaSubscribe()

  fun deleteItem(item: BuildItem) = Single.fromCallable<Unit> {
    lua
      .getBy("build.itemsTab.controls.itemList")
      .selfCall("OnSelDelete", LuaValue.NIL, item.id)
  }.luaSubscribe()
    .flatMap { reloadState() }
}