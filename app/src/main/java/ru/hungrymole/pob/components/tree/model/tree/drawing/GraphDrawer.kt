package ru.hungrymole.pob.components.tree.model.tree.drawing

import android.graphics.*
import android.os.Build
import android.util.SparseArray
import kdbush.FloatDeque
import kdbush.int32.KDBushInt
import kdbush.int32.KDBushIntFactory
import kdbush.int32.PointInt
import ru.hungrymole.pob.BuildConfig
import ru.hungrymole.pob.common.forEachByIndex
import ru.hungrymole.pob.components.tree.model.tree.Graph
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.EDGE_DRAWING_OFFSET
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.EDGE_LINE_WIDTH
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.edgeDrawers
import ru.hungrymole.pob.components.tree.model.tree.edges.Edge
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode
import ru.hungrymole.pob.components.tree.model.tree.nodes.JewelNode
import ru.hungrymole.pob.extensions.toSparseArray
import timber.log.Timber

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 17.12.14.
 */
class GraphDrawer(val graph: Graph) {

    private val BG_COLOR = Color.parseColor("#1B1310")
    private val rect = Rect()
    private val point = Point()
    private val points = FloatDeque(32)
    private val pic = Picture()

    private val jewelNodes = graph.nodes.filterIsInstance<JewelNode>()

    private val nodeIndexes = graph.nodes
            .groupBy { it.allocIcon.width }
            .mapValues { KDBushIntFactory.fromPoints(it.value) }
            .toSparseArray()

    private val groupIndexes = graph.groups
            .filter { it.bg != null }
            .groupBy { it.bg!!.width }//b
            .mapValues { KDBushIntFactory.fromPoints(it.value) }
            .toSparseArray()

    private val edgeIndexes = graph.edges
            .filterNot(Edge::invisible)
            .let(KDBushIntFactory::fromPoints)
            .let { EDGE_DRAWING_OFFSET to it }
            .let(::mapOf)
            .toSparseArray()

    fun drawRegion(canvas: Canvas, treeRegion: RectF) {
//        measureNano("Draw tree") {
            _drawRegion(canvas, treeRegion)
//        }
    }

    private fun _drawRegion(canvas: Canvas, treeRegion: RectF) {
        canvas.getClipBounds(rect)
        val newScale = rect.height().toFloat() / treeRegion.height()
        treeRegion.copyTo(rect)

//        val cantUsePictureApi = canvas.isHardwareAccelerated && Build.VERSION.SDK_INT < Build.VERSION_CODES.M
        val cantUsePictureApi = true
        when {
            cantUsePictureApi -> drawTreeRegionOnCanvas(canvas, rect, newScale)
            else              -> pic.apply {
                val accumulatingCanvas = beginRecording(canvas.width, canvas.height)
                drawTreeRegionOnCanvas(accumulatingCanvas, rect, newScale)
                draw(canvas)
            }
        }
    }

    private fun drawTreeRegionOnCanvas(canvas: Canvas, region: Rect, scale: Float) {
        canvas.drawColor(BG_COLOR)

        val groups =
//            measureNano("Find groups") {
                groupIndexes.findPointsWithOffsets(region)
//            }
//        measureNano("Draw groups") {
            groups.forEachByIndex { group ->
                group.drawBackground(canvas, scale, region)
            }
//        }

        val nodes =
//            measureNano("Find nodes") {
                nodeIndexes.findPointsWithOffsets(region)
//            }

        val edges =
//            measureNano("Find edges") {
                edgeIndexes.findPointsWithOffsets(region)
//            }

        val edgesDrawn =
//            measureNano("Draw edges") {
                drawEdges(edges, canvas, region, scale)
//            }

//        measureNano("Draw nodes") {
            nodes.forEachByIndex { node ->
                node.draw(canvas, scale, region)
            }
//        }

//        measureNano("Draw jewels") {
            jewelNodes.forEachByIndex { jewel ->
                jewel.drawJewel(canvas, scale, region)
            }
//        }
//        measureNano("Draw ascendancy") {
            graph.ascendancy.apply {
                button.draw(canvas, scale, region)
                if (canDraw(region)) {
                    drawBackground(canvas, scale, region)
                    val ascNodes = nodeIndexes.findPointsWithOffsets(region)
                    val ascEdges = edgeIndexes.findPointsWithOffsets(region)
                    drawEdges(ascEdges, canvas, region, scale)
                    ascNodes.forEachByIndex { node ->
                        node.draw(canvas, scale, region)
                    }
                }
            }
//        }

        drawSearchPoints(graph.nodes, canvas, region, scale)
        drawSearchPoints(graph.ascendancy.nodes, canvas, region, scale)

        points.clear()
        graph.ascendancies.forEachByIndex { asc ->
            if (asc == graph.ascendancy && graph.ascendancy.button.isAllocated) return@forEachByIndex
            asc.nodes.forEachByIndex { node ->
                if (node.matchesSearch && region.contains(node.x, node.y)) {
                    val canvasX = (node.x - region.left) * scale
                    val canvasY = (node.y - region.top) * scale
                    points.push(canvasX)
                    points.push(canvasY)
                }
            }
        }
        canvas.drawPoints(points.getDeque(), 0, points.getCount(), Drawing.highlightedPointPaint)

        if (BuildConfig.DEBUG) {
            Timber.tag("Draw").d("Groups - ${groups.size}, Nodes - ${nodes.size}, Edges - $edgesDrawn")
        }
    }

    private fun drawSearchPoints(nodes: List<AbstractNode>,
                                 canvas: Canvas,
                                 region: Rect,
                                 scale: Float) {
        val cx = region.centerX()
        val cy = region.centerY()
        points.clear()
        nodes.forEachByIndex { node ->
            if (node.matchesSearch && !region.contains(node.x, node.y)) {
                LiangBarsky.clip(cx, cy, node.x, node.y, region, point)
                val canvasX = (point.x - region.left) * scale
                val canvasY = (point.y - region.top) * scale
                points.push(canvasX)
                points.push(canvasY)
            }
        }
        canvas.drawPoints(points.getDeque(), 0, points.getCount(), Drawing.highlightedPointPaint)
    }

    private fun <T : PointInt> SparseArray<KDBushInt<T>>.findPointsWithOffsets(region: Rect): List<T> {
        val result = mutableListOf<T>()
        for (i in 0 until size()) {
            val index = valueAt(i)
            val halfSize = keyAt(i)
            val expandBy = -halfSize
            result += index.range(
                    region.left + expandBy,
                    region.top + expandBy,
                    region.right - expandBy,
                    region.bottom - expandBy
                )
//            }
        }
        return result
    }

    private fun drawEdges(edges: List<Edge>,
                          canvas: Canvas,
                          region: Rect,
                          scale: Float): Int {
        val scaledEdgeWidth = EDGE_LINE_WIDTH * scale

        edgeDrawers.forEachByIndex { (paint, path) ->
            paint.strokeWidth = scaledEdgeWidth
            path.reset()
        }

        edges.forEachByIndex { e ->
            e.addToDedicatedPath(scale, region)
        }

        edgeDrawers.forEachByIndex { (paint, path) ->
            if (!path.isEmpty) {
                canvas.drawPath(path, paint)
            }
        }

        return edges.size
    }
}
