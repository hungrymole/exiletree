package ru.hungrymole.pob.components

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.hungrymole.pob.common.castOrError
import ru.hungrymole.pob.lua.Tooltip
import ru.hungrymole.pob.ui.adapters.MutableArrayAdapter

class TooltipElementAdapter<T : HasSingleLineTooltip>(
  act: Activity,
  items: List<T>
) : MutableArrayAdapter<T>(act, items) {

  override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
    val text = super.getView(position, convertView, parent) as TextView
    val item = getItem(position)
    item
      .tooltip
      ?.let(text::setSingleLineTooltip)
    ?: text.setText(item.toString())
    return text
  }

  override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
    val text = super.getDropDownView(position, convertView, parent) as TextView
    val item = getItem(position)
    item
      .tooltip
      ?.let(text::setSingleLineTooltip)
    ?: text.setText(item.toString())
    return text
  }
}

interface HasSingleLineTooltip {
  var tooltip: Tooltip?
}

class TooltipElementAdapter2(
  act: Activity,
  items: List<Tooltip>
) : MutableArrayAdapter<Tooltip>(act, items) {

  override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View =
    super
      .getView(position, convertView, parent)
      .castOrError<TextView>()
      .apply { setTooltip(getItem(position), drawBackground = false) }

  override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View =
    super
      .getDropDownView(position, convertView, parent)
      .castOrError<TextView>()
      .apply { setTooltip(getItem(position), drawBackground = false) }
}