package ru.hungrymole.pob.components

import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.drawable.GradientDrawable
import android.graphics.drawable.LayerDrawable
import android.support.design.widget.AppBarLayout
import android.support.design.widget.AppBarLayout.LayoutParams.*
import android.text.Spannable
import android.text.style.*
import android.view.View
import android.widget.TextView
import com.squareup.wire.Message
import ru.hungrymole.pob.R
import ru.hungrymole.pob.common.forEachByIndex
import ru.hungrymole.pob.extensions.color
import ru.hungrymole.pob.extensions.dpToPxRatio
import ru.hungrymole.pob.extensions.getActivity
import ru.hungrymole.pob.generator.Generated
import ru.hungrymole.pob.lua.Tooltip

/**
 * Created by khusainov.d on 24.05.2017.
 */

fun Context.ascendancyOrClassAvatar(name: String): Int {
    return resources.getIdentifier("${name.toLowerCase()}_avatar", "drawable", packageName)
}

var View.coordinatorScrollEnabled: Boolean
    get() = (layoutParams as AppBarLayout.LayoutParams).scrollFlags != 0
    set(value) {
        (layoutParams as AppBarLayout.LayoutParams).scrollFlags = if (!value) 0 else SCROLL_FLAG_ENTER_ALWAYS + SCROLL_FLAG_SCROLL
    }

fun TextView.setTooltip(tooltip: Tooltip, drawBackground: Boolean = true) {
    val spans = Spannable.Factory.getInstance()
    val act = getActivity()
    val ratio = act.dpToPxRatio
    val borderColor = tooltip.color
    val strokeWidth = ratio * 4
    val startMargin = (ratio * 8).toInt()
    if (drawBackground) {
        background = arrayOf(
          GradientDrawable().apply { setColor(act.color(R.color.bg_tooltip)) },
          GradientDrawable().apply { setStroke(strokeWidth.toInt(), borderColor) }
        ).let(::LayerDrawable)
    }
    val content = spans.newSpannable(tooltip.text)
    tooltip.textColors.forEachByIndex {
        content.setSpan(ForegroundColorSpan(it.value), it.start, it.end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    }
    tooltip.separators.forEachByIndex {
        content.setSpan(SeparatorSpan(borderColor, strokeWidth), it, it + 1, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    }
    tooltip.textSizes.forEachByIndex {
        val textProportion = it.value / Tooltip.DEFAULT_SIZE.toFloat()
        content.setSpan(RelativeSizeSpan(textProportion), it.start, it.end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
        content.setSpan(LeadingMarginSpan.Standard(startMargin, startMargin), it.start, it.end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    }
    tooltip.highlights.forEachByIndex {
        content.setSpan(BackgroundColorSpan(it.value), it.start, it.end, Spannable.SPAN_INCLUSIVE_INCLUSIVE)
    }

    setText(content)
}

fun TextView.setSingleLineTooltip(tooltip: Tooltip) {
    if (tooltip.lines.size != 1) error("Invalid tooltip line count")
    setTooltip(tooltip, drawBackground = false)
}

private class SeparatorSpan(val color: Int,
                    val strokeWidth: Float) : ReplacementSpan() {

    override fun draw(canvas: Canvas, text: CharSequence,
                      start: Int, end: Int,
                      x: Float, top: Int,
                      y: Int, bottom: Int,
                      paint: Paint) {
        val mid = (top + bottom) / 2f
        Paint(paint).also {
            it.color = color
            it.strokeWidth = strokeWidth
            canvas.drawLine(x, mid, x + canvas.width, mid, it)
        }
    }

    override fun getSize(paint: Paint?, text: CharSequence?, start: Int, end: Int, fm: Paint.FontMetricsInt?) = 0
}

fun <M : Message<*, *>> Generated<M>.loadGenerated(ctx: Context): M = read(ctx.assets::open)