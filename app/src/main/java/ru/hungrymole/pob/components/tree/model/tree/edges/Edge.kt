package ru.hungrymole.pob.components.tree.model.tree.edges

import android.graphics.Paint
import android.graphics.Path
import android.graphics.Rect
import kdbush.int32.PointInt
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.edgeAllocPaint
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.edgeHightlightedAllocPaint
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.edgeHightlightedUnallocPaint
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing.edgeUnallocPaint
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode

/**
 * Class represents graphs Edges
 * IntelliJ IDEA.
 * Created by dhusainov on 13.01.15.
 */
abstract class Edge(
        val node1: AbstractNode,
        val node2: AbstractNode
) : PointInt {

    override val x = (node1.x + node2.x) / 2
    override val y = (node1.y + node2.y) / 2

    val invisible = (node1.isAsc && !node2.isAsc) || (node2.isAsc && !node1.isAsc)

    private var paint: Paint = edgeUnallocPaint.first
    protected var path: Path = edgeUnallocPaint.second
    fun updateDrawInfo() {
        val (newPaint, newPath) = when {
            isAllocated && !highlighted  -> edgeAllocPaint
            !isAllocated && !highlighted -> edgeUnallocPaint
            isAllocated                  -> edgeHightlightedUnallocPaint
            else                         -> edgeHightlightedAllocPaint
        }
        paint = newPaint
        path = newPath
    }

    var highlighted = false
    fun highlight() {
        if (highlighted) return
        highlighted = true
        updateDrawInfo()
    }

    fun unhighlight() {
        if (!highlighted) return
        highlighted = false
        updateDrawInfo()
    }

    fun nodes() = setOf(node1, node2)
    val isAllocated: Boolean get() = node1.isAllocated && node2.isAllocated

    override fun equals(o: Any?): Boolean {
        if (o === this) return true
        o as Edge
        return node1 === o.node1 && node2 === o.node2 || node1 === o.node2 && node2 === o.node1
    }

    private val cachedHash = node1.id.hashCode() + node2.id.hashCode()
    override fun hashCode(): Int = cachedHash

    fun getSecondNode(source: AbstractNode): AbstractNode {
        return if (source === node1) node2 else node1
    }

    operator fun contains(region: Rect): Boolean {
        return region.contains(node1.x, node1.y) || region.contains(node2.x, node2.y)
    }

    fun allocate(allocated: Boolean) {
        node1.isAllocated = allocated
        node2.isAllocated = allocated
        updateDrawInfo()
    }

    abstract fun addToDedicatedPath(scale: Float, region: Rect)
}
