package ru.hungrymole.pob.components.tree.model.tree

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import kdbush.int32.PointInt
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 04.12.14.
 */
class Group(
    override var x: Int,
    override var y: Int,
    val nodes: List<AbstractNode>,
    var bg: Bitmap?
) : PointInt {

    private val matrix = Drawing.mainThreadMatrix
    private val bgBitmap = Drawing.bitmapDrawPaint
    private val bgHalfWidth = bg?.width?.div(2) ?: 0
    private val bgHalfHeight = bg?.height?.div(2) ?: 0
    fun drawBackground(canvas: Canvas, scale: Float, region: Rect) {
        bg ?: return
        val centerX = (x - region.left) * scale
        val centerY = (y - region.top) * scale
        matrix.reset()
        matrix.postTranslate(centerX - bgHalfWidth, centerY - bgHalfHeight)
        matrix.postScale(scale, scale, centerX, centerY)
        canvas.drawBitmap(bg, matrix, bgBitmap)
    }
}
