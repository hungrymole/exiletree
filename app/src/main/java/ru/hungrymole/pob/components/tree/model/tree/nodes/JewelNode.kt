package ru.hungrymole.pob.components.tree.model.tree.nodes

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData
import ru.hungrymole.pob.components.tree.model.tree.drawing.JewelDrawer

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 29.07.15.
 */
class JewelNode(
        data: TreeNodeData,
        val typeIcons: Map<String, Bitmap>
) : Notable(data) {

    override var isAllocated: Boolean
        get() = super.isAllocated
        set(allocated) {
            super.isAllocated = allocated
            if (!allocated) {
                controller?.detach()
            }
        }

    override val isJewel = true
    override var allocIcon: Bitmap = super.allocIcon

    override var controller: JewelDrawer? = null
        set(value) {
            field = value
            allocIcon = when {
                value != null -> typeIcons[value.jewelType]!!
                else -> super.allocIcon
            }
            isAllocated = isAllocated
        }

    override fun draw(canvas: Canvas, scale: Float, region: Rect) {
        val centerX = (x - region.left) * scale
        val centerY = (y - region.top) * scale
        matrix.reset()
        matrix.postTranslate(centerX - iconHalfWidth, centerY - iconHalfWidth)
        matrix.postScale(scale, scale, centerX, centerY)
        canvas.drawBitmap(icon, matrix, defaultPaint);
    }

    fun drawJewel(canvas: Canvas, scale: Float, region: Rect) {
        controller?.drawJewelExtras(canvas, scale, region)
    }
}
