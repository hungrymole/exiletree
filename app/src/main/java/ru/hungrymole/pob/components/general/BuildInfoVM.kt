package ru.hungrymole.pob.components.general

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.common.`try`
import ru.hungrymole.pob.dialogs.*
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.lua.isEmpty
import ru.hungrymole.pob.lua.selfCall
import ru.hungrymole.pob.lua.toObject
import ru.hungrymole.pob.ui.mvvm.ViewModel
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 01.08.2017.
 */

class BuildInfoVM @Inject constructor(
  private val attrs: BuildAttrsModel,
  private val lua: Globals,
  ctx: Context
) : ViewModel<BuildInfoState>(ctx) {

    override fun getState() = Single.fromCallable {
        val name = lua.getBy("build.buildName").checkjstring()
        val asc = lua.getBy("build.spec.curAscendClassName").checkjstring()
        val points = lua.getBy("build.controls.pointDisplay.width()").toObject<BuildPoints>()
        BuildInfoState(name, asc, points, attrs.currentStat)
    }.luaSubscribe()

    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {
        subscriptions += attrs.statMap
            .flatMapSingle { reloadState() }
            .subscribe()
    }

    override fun subscribeOnStart(subscriptions: CompositeDisposable) {}

  fun LuaValue.validateDropdown(): Boolean {
    val shown = get("shown").optboolean(false)
    val isEmpty = get("list").checktable().isEmpty()
    return shown && !isEmpty
  }

    fun getBuildConfig() = Single.fromCallable {

        val configs = mutableListOf<BaseConfigEntry>()
        fun LuaValue.addConfig(label: String) {
          if (validateDropdown()) {
            configs += toObject<DropdownConfig>().also { it.label = label }
          }
        }

        configs += lua.getBy("build.characterLevel").checkint().let { NumberConfigEntry("Level", 1..100, it) }
        lua
          .getBy("build.controls.bandit")
          .takeIf { it.validateDropdown() }
          ?.toObject<BanditDropdownConfig>()
          ?.also { it.label = "Bandit" }
          ?.let(configs::add)
        lua.getBy("build.controls.mainSocketGroup").addConfig("Main group")
        `try` { lua.getBy("build.controls.mainSkill").addConfig("Main skill") }
        `try` { lua.getBy("build.controls.mainSkillPart").addConfig("Skill part") }
        configs
    }.luaSubscribe()

    fun updateBuildConfig(r: ConfigResult) = Single.fromCallable {

        fun LuaValue.applyConfig(l: String) {
            val shown = get("shown").optboolean(false)
            val isEmpty = get("list").checktable().isEmpty()
            if (shown && !isEmpty) {
                val i = r.byLabel<SpinnerConfigEntry<Any>>(l).index + 1
                selfCall("SetSel", i)
            }
        }

        val level = r.byLabel<NumberConfigEntry>("Level").value.toString()
        lua.getBy("build.controls.characterLevel.changeFunc").call(level)
        lua.getBy("build.controls.bandit").applyConfig("Bandit")
        lua.getBy("build.controls.mainSocketGroup").applyConfig("Main group")
        lua.getBy("build.controls.mainSkill").applyConfig("Main skill")
        lua.getBy("build.controls.mainSkillPart").applyConfig("Skill part")
    }.flatMap { reloadState() }.luaSubscribe()
}