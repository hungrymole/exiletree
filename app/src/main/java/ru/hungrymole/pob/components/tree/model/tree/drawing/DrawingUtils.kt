package ru.hungrymole.pob.components.tree.model.tree.drawing

import android.graphics.*

/**
 * Created by Dinar Khusainov on 16.12.2017.
 */
object Drawing {
    const val EDGE_LINE_WIDTH = 8f
    const val EDGE_DRAWING_OFFSET = 200

    private fun edgePaint(block: Paint.() -> Unit) = Paint().apply {
        block()
        isAntiAlias = true
        style = Paint.Style.STROKE
        strokeWidth = EDGE_LINE_WIDTH
    }
    @JvmStatic
    val edgeUnallocPaint = edgePaint {
        setARGB(255, 192, 192, 192) //silver
    } to Path()

    @JvmStatic
    val edgeAllocPaint = edgePaint {
        setARGB(255, 255, 215, 0) //gold
    } to Path()

    @JvmStatic
    val edgeHightlightedAllocPaint = edgePaint {
        color = Color.GREEN
    } to Path()

    @JvmStatic
    val edgeHightlightedUnallocPaint = edgePaint {
        color = Color.RED
    } to Path()

    @JvmStatic
    val edgeDrawers = listOf(
            edgeUnallocPaint,
            edgeAllocPaint,
            edgeHightlightedAllocPaint,
            edgeHightlightedUnallocPaint
    )

    @JvmStatic
    val bitmapDrawPaint = Paint().apply {
        isAntiAlias = false
        isFilterBitmap = true
        isDither = false
        alpha = 255
    }
    @JvmStatic
    val jewelCirclePaint = Paint().apply {
        setARGB(80, 138, 138, 184)
        isAntiAlias = true
        style = Paint.Style.FILL
        strokeWidth = 4f
    }
    @JvmStatic
    val ascTextPaint = Paint().apply {
        isAntiAlias = true
        typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD_ITALIC)
        textSize = 17F
    }

    val SEARCH_HIGHLIGHT_COLOR = Color.argb(192, 255, 0, 0)

    @JvmStatic
    val highlightedFramePaint = Paint().apply {
        isAntiAlias = false
        isFilterBitmap = true
        isDither = false

        colorFilter = PorterDuffColorFilter(
            SEARCH_HIGHLIGHT_COLOR,
            PorterDuff.Mode.SRC_ATOP
        )
    }

    @JvmStatic
    val highlightedPointPaint = Paint().apply {
        strokeCap = Paint.Cap.ROUND
        color = SEARCH_HIGHLIGHT_COLOR
        strokeWidth = 12f
    }

    @JvmStatic
    val mainThreadMatrix = Matrix()
}

fun RectF.toRect(): Rect = run { Rect(left.toInt(), top.toInt(), right.toInt(), bottom.toInt()) }

fun RectF.copyTo(rect: Rect) {
    apply { rect.set(left.toInt(), top.toInt(), right.toInt(), bottom.toInt()) }
}

fun Rect.copyTo(rect: RectF) {
    apply { rect.set(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat()) }
}

fun Rect.toRectF(): RectF = run { RectF(left.toFloat(), top.toFloat(), right.toFloat(), bottom.toFloat()) }