package ru.hungrymole.pob.components.tree.model.tree

import android.graphics.Bitmap

/**
 * Created by Dinar on 01.12.2017.
 */
class TreeNodeData(
        val x: Int,
        val y: Int,
        val id: Int,
        val unallocIcon: Bitmap,
        val allocIcon: Bitmap/*,
        val connections: List<Edge>*/)