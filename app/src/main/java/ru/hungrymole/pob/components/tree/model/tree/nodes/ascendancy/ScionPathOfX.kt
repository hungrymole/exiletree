package ru.hungrymole.pob.components.tree.model.tree.nodes.ascendancy

import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData

/**
 * Created by khusainov.d on 04.10.2017.
 */
class ScionPathOfX(data: TreeNodeData) : AscendancyNotable(data)