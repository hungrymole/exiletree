package ru.hungrymole.pob.components.skills.group.search

import io.reactivex.Single
import ru.hungrymole.pob.components.skills.SearchableSkill

/**
 * Created by khusainov.d on 19.09.2017.
 */
interface GemRepository {

    fun query(q: String, pos: Int) : Single<List<SearchableSkill>>
}