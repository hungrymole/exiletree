package ru.hungrymole.pob.components.tree.model.tree.nodes

import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Rect
import kdbush.int32.PointInt
import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData
import ru.hungrymole.pob.components.tree.model.tree.drawing.Drawing
import ru.hungrymole.pob.components.tree.model.tree.drawing.JewelDrawer
import ru.hungrymole.pob.components.tree.model.tree.edges.Edge

/**Sample text
 * Created by dhusainov on 19.08.14.
 */
abstract class AbstractNode(data: TreeNodeData) : PointInt {
    override val x: Int = data.x
    override val y: Int = data.y
    val id: Int = data.id
    val unallocIcon: Bitmap = data.unallocIcon
    open val allocIcon: Bitmap = data.allocIcon
    var connections: List<Edge> = emptyList()

    open val isClass = false
    open val isJewel = false
    open val isAsc = false
    open val hasTooltip = true

    var matchesSearch = false

    open var isAllocated: Boolean = false
        set(allocated) {
            field = allocated
            icon = if (allocated) allocIcon else unallocIcon
            iconHalfWidth = icon.width / 2
            iconHalfHeight = icon.height / 2
            for (i in 0 until connections.size) {
                connections[i].updateDrawInfo()
            }
        }

    init {
        isAllocated = false
    }

    open var controller: JewelDrawer? = null
    var icon = unallocIcon
    var iconHalfWidth = 0
    var iconHalfHeight = 0
    protected val matrix = Drawing.mainThreadMatrix
    protected val defaultPaint = Drawing.bitmapDrawPaint
    protected val searchPaint = Drawing.highlightedFramePaint
    open fun draw(canvas: Canvas, scale: Float, region: Rect) {
        val centerX = (x - region.left) * scale
        val centerY = (y - region.top) * scale
        matrix.reset()
        matrix.postTranslate(centerX - iconHalfWidth, centerY - iconHalfHeight)
        matrix.postScale(scale, scale, centerX, centerY)
        val paint = if (matchesSearch) searchPaint else defaultPaint
        canvas.drawBitmap(icon, matrix, paint)
        controller?.drawNodesExtras(this, centerX, centerY, canvas, scale)
    }

    open fun contains(pressedX: Int, pressedY: Int, scale: Float): Boolean {
        val factor = if (scale > 0.9f) 1f else 2f - scale
        val iconHalfSize = iconHalfWidth * factor
        return x >= pressedX - iconHalfSize &&
                x <= pressedX + iconHalfSize &&
                y >= pressedY - iconHalfSize &&
                y <= pressedY + iconHalfSize
    }

    override fun equals(o: Any?): Boolean = this === o
    override fun hashCode(): Int = id
}
