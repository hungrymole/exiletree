package ru.hungrymole.pob.components.items.search

import android.database.sqlite.SQLiteDatabase
import io.reactivex.Single
import ru.hungrymole.pob.components.items.search.QueryType.*
import ru.hungrymole.pob.db.*
import ru.hungrymole.pob.extensions.moveBy
import ru.hungrymole.pob.generator.db.DB
import ru.hungrymole.pob.generator.lua.TooltipInfo
import ru.hungrymole.pob.lua.Tooltip
import timber.log.Timber
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Dinar Khusainov on 28.01.2018.
 */
@Singleton
class UniqueSearch @Inject constructor(
    private val db: SQLiteDatabase
) {

    val leagues: List<Reference> = DB.Leagues.load(db)
    val itemTypes: List<Reference> = DB.ItemTypes.load(db)

    var queryType: QueryType = Anywhere
    var query = ""
    var league = leagues.first()
    var itemType = itemTypes.first()

    fun getTooltip(unique: Unique): Tooltip {
        return DB.Uniques.run {
            db.query("select $tooltipInfo from $tableName where $id = ${unique.id}")
                .strictFirstConsume { it.getBlob(tooltipInfo) }
                .let(TooltipInfo.ADAPTER::decode)
                .let(::Tooltip)
        }
    }
//
//    fun getTooltipMods(unique: Unique): String {
//        return POBDB.UniquesFTSMods.run {
//            db.query("select $tooltipMods from $tableName where $uniqueId = ${unique.id}")
//                .strictFirstConsume { it.getString(tooltipMods) }
//        }
//    }

    fun getRaw(unique: Unique): String = DB.Uniques.run {
        val query = "select $raw from $tableName where $id = ${unique.id}"
        Timber.d(query)
        db
            .query(query)
            .strictFirstConsume { it.getString(raw) }
    }

    fun filterUniques(): Single<List<Unique>> = Single.fromCallable {
        val intersects = mutableListOf<String>()
        DB.UniquesLeagues.apply {
            if (league.id == 0) return@apply
            intersects += "select $uniqueId as id from $tableName where $leagueId = ${league.id}"
        }

        DB.UniquesTypes.apply {
            if (itemType.id == 0) return@apply
            intersects += "select $uniqueId as id from $tableName where $itemTypeId = ${itemType.id}"
        }

        val filtersSubquery = intersects.joinToString("\nintersect\n")

        val sql = when {
            query.length > 1 -> DB.UnuqueFTSSearch.run {
                val fullMatch = "*$query*"
                val ftsSearch = when (queryType) {
                    Anywhere  -> fullMatch
                    Names     -> "$uniqueName: $fullMatch"
                    Modifiers -> "$uniqueMods: $fullMatch"
                }
                val whereClause = when {
                    filtersSubquery.isNotEmpty() -> "id in ($filtersSubquery) and $tableName match '$ftsSearch'"
                    else                         -> "$tableName match '$ftsSearch'"
                }
                """select
                    offsets($tableName) as offsets,
                    $tableName.$uniqueId as id,
                    ${DB.Uniques.tableName}.${DB.Uniques.tooltipModsOffset}
                    from $tableName
                    join ${DB.Uniques.tableName} on ${DB.Uniques.tableName}.${DB.Uniques.id} = id
                    where $whereClause
                    """
                    .trimIndent()
            }
            filtersSubquery.isNotEmpty() -> filtersSubquery
            else                         -> "select ${DB.Uniques.id} as id from ${DB.Uniques.tableName}"
        }

        db.query(sql).mapConsume {
            val matches = optString("offsets")
                ?.split(" ")
                ?.windowed(4,4)
                ?.map { match ->
                    val matchOffset = match[2].toInt()
                    val length = match[3].toInt()
                    val matchRange = matchOffset until matchOffset + length
                    val needOffset = match[0].toInt() != 0
                    when {
                        needOffset -> matchRange.moveBy(getInt(DB.Uniques.tooltipModsOffset))
                        else       -> matchRange
                    }
                }
            Unique(getInt("id"), matches)
        }
    }
}