package ru.hungrymole.pob.components

/**
 * Created by khusainov.d on 31.07.2017.
 */
open class DropdownControl<T>(selIndex: Int,
                              val list: List<T>) {
    var value: T = list.first()
    var index: Int = selIndex - 1//in lua tables(arrays) start from 1
        set(value) {
            field = value
            this.value = list[value]
        }
}

interface HasColor {
    val color: Int?
}