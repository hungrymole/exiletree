package ru.hungrymole.pob.components.tree.model.tree.edges

import android.graphics.Rect
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 14.01.15.
 */
class Line(
        node1: AbstractNode,
        node2: AbstractNode
) : Edge(node1, node2) {

    private val n1x = node1.x
    private val n1y = node1.y
    private val n2x = node2.x
    private val n2y = node2.y

    override fun addToDedicatedPath(scale: Float, region: Rect) {
        path.moveTo((n1x - region.left) * scale, (n1y - region.top) * scale)
        path.lineTo((n2x - region.left) * scale, (n2y - region.top) * scale)
    }
}
