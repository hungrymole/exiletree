package ru.hungrymole.pob.components.items.slots

import ru.hungrymole.pob.components.DropdownControl
import ru.hungrymole.pob.lua.Table
import ru.hungrymole.pob.lua.Tooltip

/**
 * Created by khusainov.d on 11.08.2017.
 */

@Table
class ItemSlot(val slotName: String,
               val label: String,
               val active: Boolean?,//flask checkbox
               val items: List<Int>,
               list: List<String>,
               selIndex: Int
) : DropdownControl<String>(selIndex, list) {

    val tooltips = items.mapIndexed { i, _ -> Tooltip.buildSingleLine(list[i]) }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (other !is ItemSlot) return false

        if (slotName != other.slotName) return false

        return true
    }

    override fun hashCode(): Int {
        return slotName.hashCode()
    }
}

