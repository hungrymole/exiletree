package ru.hungrymole.pob.components.tree

import ru.hungrymole.pob.lua.Color
import ru.hungrymole.pob.lua.HasTableKey
import ru.hungrymole.pob.lua.Table

/**
 * Created by khusainov.d on 01.08.2017.
 */
class TreeState(val allocated: List<Int>,
                val classId: Int,
                val ascClass: String,
                val jewels: Map<Int, SocketedJewel>,
                val searchStr: String?
)

@Table
class SocketedJewel(val title: String?,
                    val raw: String,
                    val baseName: String,
                    val jewelRadiusIndex: Int?) {
    var radius = 0F
    var affectedNodes = emptyList<Int>()
    var affectedNodeColor = 0
}

@Table
class JewelRadiusInfo(val rad: Int,
                      @Color val col: Int) : HasTableKey()

@Table
class NodeInfo(val type: String,
               val alloc: Boolean?,
               val sd: List<String>) : HasTableKey()