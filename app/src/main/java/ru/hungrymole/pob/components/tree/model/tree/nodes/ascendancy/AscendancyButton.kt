package ru.hungrymole.pob.components.tree.model.tree.nodes.ascendancy

import android.graphics.Canvas
import android.graphics.Rect
import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode

class AscendancyButton(val angle: Float, data: TreeNodeData) : AbstractNode(data) {

    override val hasTooltip = false

    override fun draw(canvas: Canvas, scale: Float, region: Rect) {
        val centerX = (x - region.left) * scale
        val centerY = (y - region.top) * scale
        matrix.reset()
        matrix.postTranslate(centerX - iconHalfWidth, centerY - iconHalfWidth)
        matrix.postScale(scale, scale, centerX, centerY)
        matrix.postRotate(angle, centerX, centerY)
        canvas.drawBitmap(icon, matrix, defaultPaint)
    }
}