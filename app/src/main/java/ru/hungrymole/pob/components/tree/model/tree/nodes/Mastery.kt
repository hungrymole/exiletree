package ru.hungrymole.pob.components.tree.model.tree.nodes

import ru.hungrymole.pob.components.tree.model.tree.TreeNodeData
import ru.hungrymole.pob.components.tree.model.tree.drawing.JewelDrawer

/**
 * IntelliJ IDEA.
 * Created by dhusainov on 05.12.14.
 */
class Mastery(data: TreeNodeData) : AbstractNode(data) {

    override val hasTooltip = false

    override var controller: JewelDrawer? = null
        set(controller) {}

    override fun contains(pressedX: Int, pressedY: Int, scale: Float): Boolean = false
}
