package ru.hungrymole.pob.components.skills.group.search

import io.reactivex.Single
import org.luaj.vm2.Globals
import ru.hungrymole.pob.components.skills.SearchableSkill
import ru.hungrymole.pob.extensions.measure
import ru.hungrymole.pob.extensions.onEachIndexed
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.*
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by khusainov.d on 19.09.2017.
 */
@Singleton
class LuaGemRepository @Inject constructor(
  private val lua: Globals
) : GemRepository {

  override fun query(q: String, pos: Int) = Single.fromCallable {
    logLua("q - $q")
    val gemSelectControl = lua.getBy("build.skillsTab.controls.gemSlot${pos + 1}Name")
    gemSelectControl["buf"] = q
    measure("longGemsQuery") {
      gemSelectControl["changeFunc"].call()
    }
    measure("getSearchResult") {
      lua
        .getBy("getSearchResult")
        .call(gemSelectControl)
        .toList<SearchableSkill>()
        .onEachIndexed { i, skill ->
          if (i > 9) return@onEachIndexed
          skill
            .tooltip = gemSelectControl
            .selfCall("GetTooltip", i + 1)
            .let(::Tooltip)
        }
    }
  }.luaSubscribe()
}