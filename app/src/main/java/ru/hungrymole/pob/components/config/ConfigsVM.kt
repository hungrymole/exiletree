package ru.hungrymole.pob.components.config

import android.content.Context
import io.reactivex.Single
import org.luaj.vm2.Globals
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.lua.selfCall
import ru.hungrymole.pob.lua.toList
import ru.hungrymole.pob.ui.list.ListViewModel
import javax.inject.Inject

class ConfigsVM @Inject constructor(
  private val lua: Globals,
  ctx: Context
) : ListViewModel<Section>(ctx) {

    override fun getState() = Single.fromCallable {
        lua.getBy("build.configTab").selfCall("GetNonEmptySections").toList<Section>()
    }.luaSubscribe()
}