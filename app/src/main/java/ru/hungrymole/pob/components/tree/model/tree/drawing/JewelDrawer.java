package ru.hungrymole.pob.components.tree.model.tree.drawing;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import ru.hungrymole.pob.components.tree.SocketedJewel;
import ru.hungrymole.pob.components.tree.model.tree.nodes.AbstractNode;

import java.util.ArrayList;
import java.util.List;

/**Sample text
 * Created by dhusainov on 26.09.2015 17:46.
 */
public class JewelDrawer {
	private static final Paint circlePaint = Drawing.getJewelCirclePaint();

	public final SocketedJewel jewel;
	public final AbstractNode jewelNode;
    public final List<AbstractNode> affectedNodes = new ArrayList<>();
	protected final Paint affectedNodesPaint = new Paint();


	public JewelDrawer(SocketedJewel jewel, AbstractNode jewelNode, List<AbstractNode> affectedNodes) {
		this.jewel = jewel;
		this.jewelNode = jewelNode;
		affectedNodesPaint.setColor(adjustAlpha(jewel.getAffectedNodeColor(), 0.33F));
		this.affectedNodes.addAll(affectedNodes);
		for (AbstractNode node : this.affectedNodes) {
			node.setController(this);
		}
		jewelNode.setController(this);
	}

	public void drawNodesExtras(AbstractNode source, float cx, float cy, Canvas canvas, float scale) {
		canvas.drawCircle(cx, cy, (source.getIconHalfWidth() * 2.33F) * scale, affectedNodesPaint);
	}

	public void drawJewelExtras(Canvas canvas, float scale, Rect region) {
		float cx = (jewelNode.getX() - region.left) * scale;
		float cy = (jewelNode.getY() - region.top) * scale;
		float circleRadius = scale * jewel.getRadius();
		canvas.drawCircle(cx, cy, circleRadius, circlePaint);
	}

	public void detach() {
		for (AbstractNode node : affectedNodes) {
			node.setController(null);
		}
		jewelNode.setController(null);
	}

	public String getJewelType() {
        return jewel.getBaseName();
	}

	private int adjustAlpha(int color, float factor) {
		int alpha = Math.round(0xFF * factor);
		int red = Color.red(color);
		int green = Color.green(color);
		int blue = Color.blue(color);
		return Color.argb(alpha, red, green, blue);
	}
}
