package ru.hungrymole.pob.dialogs

import android.app.Activity
import android.view.View
import android.widget.TableLayout
import com.afollestad.materialdialogs.MaterialDialog
import com.jakewharton.rxbinding2.widget.checkedChanges
import com.jakewharton.rxbinding2.widget.itemSelections
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.Maybe
import io.reactivex.MaybeObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.layout_config_dialog_entry_checkbox.view.*
import kotlinx.android.synthetic.main.layout_config_dialog_entry_label.view.*
import kotlinx.android.synthetic.main.layout_config_dialog_entry_number.view.*
import kotlinx.android.synthetic.main.layout_config_dialog_entry_spinner.view.*
import kotlinx.android.synthetic.main.layout_config_dialog_entry_text.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.ui.adapters.spinnerAdapter

/**
 * Created by khusainov.d on 26.08.2017.
 */
class ConfigDialog(val act: Activity,
                   val title: String,
                   val items: List<BaseConfigEntry>) : Maybe<ConfigResult>() {

    val subscriptions = CompositeDisposable()
    val dialog by lazy { buildDialog() }
    lateinit var obs: MaybeObserver<in ConfigResult>

    override fun subscribeActual(observer: MaybeObserver<in ConfigResult>) {
        obs = observer
        dialog.show()
    }

    fun buildView(): View {
        val root = act.layoutInflater.inflate(R.layout.layout_config_dialog, null) as TableLayout
        items.forEach { entry ->
            val view = when (entry) {
                is SpinnerConfigEntry<*> -> act.layoutInflater.inflate(R.layout.layout_config_dialog_entry_spinner, null).also {
                    it.config_dialog_entry_spinner.run {
                        adapter = act.spinnerAdapter(entry.list)
                        setSelection(entry.index)
                        subscriptions += itemSelections().subscribe { entry.index = it }
                    }
                }
                is TextConfigEntry -> act.layoutInflater.inflate(R.layout.layout_config_dialog_entry_text, null).also {
                    it.config_dialog_entry_text.run {
                        setText(entry.text)
                        subscriptions += textChanges().subscribe { entry.text = it.toString() }
                    }
                }
                is CheckboxConfigEntry -> act.layoutInflater.inflate(R.layout.layout_config_dialog_entry_checkbox, null).also {
                    it.config_dialog_entry_checkbox.run {
                        isChecked = entry.value
                        subscriptions += checkedChanges().subscribe { entry.value = it }
                    }
                }
                is NumberConfigEntry -> act.layoutInflater.inflate(R.layout.layout_config_dialog_entry_number, null).also {
                    it.config_dialog_entry_number.run {
                        minValue = entry.range.first
                        maxValue = entry.range.last
                        value = entry.value
                        subscriptions += changes.subscribe { entry.value = it }
                    }
                }
                else -> null!!
            }
            view.config_dialog_entry_label.text = entry.label + ":"
            root.addView(view)
        }
        return root
    }

    fun buildDialog() = MaterialDialog.Builder(act).run {0
        title(title)
        customView(buildView(), true)
        positiveText("Ok")
        negativeText("Cancel")
        var pressed = false
        onAny { dialog, which -> pressed = true }
        onPositive { _, _ -> obs.onSuccess(ConfigResult(items)) }
        onNegative { _, _ -> obs.onComplete() }
        dismissListener {
            if (!pressed) obs.onComplete()
            subscriptions.dispose()
        }
        build()
    }
}