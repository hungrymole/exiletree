package ru.hungrymole.pob.dialogs

import ru.hungrymole.pob.components.DropdownControl
import ru.hungrymole.pob.lua.Table

/**
 * Created by khusainov.d on 26.08.2017.
 */

interface BaseConfigEntry {
    val label: String?
}

open class SpinnerConfigEntry<T>(list: List<T>,
                            selIndex: Int) : DropdownControl<T>(selIndex, list), BaseConfigEntry {

    constructor(label: String, list: List<T>, selItem: T) : this(list, list.indexOf(selItem) + 1) {
        this.label = label
    }

    override var label = ""
}

class TextConfigEntry(override val label: String,
                      var text: String = "") : BaseConfigEntry

class CheckboxConfigEntry(override val label: String,
                          var value: Boolean = false) : BaseConfigEntry

class NumberConfigEntry(override val label: String,
                        val range: IntRange,
                        var value: Int) : BaseConfigEntry

class ConfigResult(val items: List<BaseConfigEntry>)

@Table
class StringDropdownConfig(list: List<String>,
                           selIndex: Int) : SpinnerConfigEntry<String>(list, selIndex)

@Suppress("UNCHECKED_CAST")
fun <T : BaseConfigEntry> ConfigResult.byLabel(l: String) = items.first { it.label == l } as T
fun <T> ConfigResult.spinner(l: String) = items.first { it.label == l } as SpinnerConfigEntry<T>