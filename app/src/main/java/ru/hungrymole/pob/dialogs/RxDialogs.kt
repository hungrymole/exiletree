package ru.hungrymole.pob.dialogs

import android.app.Activity
import android.content.Context
import android.view.View
import com.afollestad.materialdialogs.MaterialDialog
import io.reactivex.Maybe
import io.reactivex.MaybeObserver
import io.reactivex.disposables.Disposables

/**
 * Created by khusainov.d on 10.08.2017.
 */

fun Context.rxConfirmDialog(title: String,
                            text: String? = null,
                            positive: String = "Ok",
                            negative: String = "Cancel") = Maybe.wrap<Boolean> { observer ->
    MaterialDialog.Builder(this).run {
        title(title)
        text?.let { content(it) }
        positiveText(positive)
        negativeText(negative)
        var pressed = false
        onAny { _, _ -> pressed = true }
        dismissListener { if (!pressed) observer.onComplete() }
        cancelListener { if (!pressed) observer.onComplete() }
        onNegative { _, _ -> observer.onSuccess(false) }
        onPositive { _, _ -> observer.onSuccess(true) }
    }.show()
}

fun Context.rxInputTextDialog(title: String) = object : Maybe<String>() {
    val dialog = MaterialDialog.Builder(this@rxInputTextDialog).run {
        title(title)
        var pressed = false
        onAny { dialog, which -> pressed = true }
        input(null, "", false) { d, str ->
            observer.onSuccess(str.toString())
        }
        onNegative { dialog, which -> observer.onComplete() }
        cancelListener { if (!pressed) observer.onComplete() }
    }
    lateinit var observer : MaybeObserver<in String>
    override fun subscribeActual(observer: MaybeObserver<in String>) {
        this.observer = observer
        dialog.show()
    }
}

fun Activity.radioButtonsDialog(title: String, items: List<String>): Maybe<String> {
    return Maybe.create<String> { emitter ->
        var dismissed = false
        val dismiss = {
            if (!dismissed) {
                dismissed = true
                emitter.onComplete()
            }
        }
        val dialog = MaterialDialog.Builder(this)
            .title(title)
            .items(items)
            .cancelListener { dismiss() }
            .dismissListener { dismiss() }
            .itemsCallbackSingleChoice(0)  { _: MaterialDialog, _: View, _: Int, choice: CharSequence ->
                emitter.onSuccess(choice.toString())
                true
            }
            .build()
        Disposables
            .fromAction(dialog::dismiss)
            .let(emitter::setDisposable)
        dialog.show()
    }
}

fun Activity.progressDialog(title: String): MaterialDialog {
    return MaterialDialog.Builder(this)
        .title(title)
        .progress(true, 0)
        .cancelable(false)
        .autoDismiss(false)
        .build()
}