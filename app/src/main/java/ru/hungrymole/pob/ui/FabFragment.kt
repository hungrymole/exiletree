package ru.hungrymole.pob.ui

import android.support.design.widget.FloatingActionButton
import ru.hungrymole.pob.R
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.ui.mvvm.ViewFragment
import ru.hungrymole.pob.ui.mvvm.ViewModel

/**
 * Created by khusainov.d on 09.08.2017.
 */
abstract class FabFragment<S: Any, VM : ViewModel<S>> : ViewFragment<S, VM>() {

    open val fabImage: Int? = null
    protected val fab by lazy { act.findViewById<FloatingActionButton>(R.id.fab) }

    override fun onStart() {
        super.onStart()
        showFab()
    }

    override fun onStop() {
        super.onStop()
        hideFab()
    }

    fun showFab() {
        if (fabImage != null && fab.tag?.toString().isNullOrEmpty()) {
            fab.tag = javaClass.toString()
            fab.visible = true
            fabImage?.let(fab::setImageResource)
        }
    }

    fun hideFab() {
        if (fab.tag == javaClass.toString()) {
            fab.visible = false
            fab.tag = ""
        }
    }
}