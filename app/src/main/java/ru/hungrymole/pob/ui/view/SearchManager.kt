package ru.hungrymole.pob.ui.view

import android.app.Activity
import android.content.Intent
import android.speech.RecognizerIntent
import android.view.inputmethod.EditorInfo.IME_ACTION_SEARCH
import com.jakewharton.rxbinding2.view.clicks
import com.jakewharton.rxbinding2.widget.editorActionEvents
import com.jakewharton.rxbinding2.widget.textChanges
import io.reactivex.subjects.PublishSubject
import kotlinx.android.synthetic.main.activity_build.*
import kotlinx.android.synthetic.main.layout_search.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.BuildActivity
import ru.hungrymole.pob.build.di.BuildScope
import ru.hungrymole.pob.extensions.hideKeyboard
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.ui.rx.plusAssign
import javax.inject.Inject

/**
 * Created by khusainov.d on 31.08.2017.
 */

@BuildScope
class SearchManager @Inject constructor(
    private val act: BuildActivity
) {

    companion object {
        const val VOICE_REQUEST = 1234
    }

    val queryChanges = PublishSubject.create<String>()
    val query get() = input.text?.toString() ?: ""
    val visibilityChanges = PublishSubject.create<Boolean>()

    private val searchView = act.build_search
    private val input = searchView.search_input
    private val cancel = searchView.search_back_button
    private val voice = searchView.search_voice_button

    init {
        voice.visible = isVoiceAvailable()
        act += voice.clicks().subscribe { requestVoice() }

        act += input.textChanges()
            .map(CharSequence::toString)
            .doOnNext(::toggleBackButtonIcon)
            .filter(String::isNotBlank)
            .subscribe(queryChanges::onNext)

        act += cancel.clicks().subscribe {
            when {
                input.text.isNullOrBlank() -> hide()
                else                       -> {
                    input.setText("")
                    queryChanges.onNext("")
                }
            }
        }
        act += input
          .editorActionEvents()
          .filter { it.actionId() == IME_ACTION_SEARCH }
          .subscribe { hide() }
    }

    fun setQuery(query: String) = input.setText(query)

    val showing get() = searchView.visible

    fun show() = toggleShow(true)

    fun hide() {
        act.hideKeyboard()
        toggleShow(false)
    }

    private fun toggleShow(visibility: Boolean) {
        visibilityChanges.onNext(visibility)
        searchView.visible = visibility
    }

    private fun toggleBackButtonIcon(input: String) {
        when {
            input.isBlank() -> R.drawable.ic_arrow_back_white_24px
            else            -> R.drawable.ic_cancel_white_24px
        }.let(cancel::setImageResource)
    }

    private fun requestVoice() {
        val intent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH).apply {
            putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_WEB_SEARCH)
            putExtra(RecognizerIntent.EXTRA_MAX_RESULTS, 1)
        }
        act.startActivityForResult(intent, VOICE_REQUEST)
    }

    fun processVoice(requestCode: Int, resultCode: Int, data: Intent?) {
        val success = requestCode == SearchManager.VOICE_REQUEST && resultCode == Activity.RESULT_OK
        data
            ?.takeIf { success }
            ?.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS)
            .orEmpty()
            .firstOrNull(CharSequence::isNotBlank)
            ?.let(::setQuery)
    }

    private fun isVoiceAvailable(): Boolean =
        act.packageManager
            .queryIntentActivities(Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH), 0)
            .isNotEmpty()
}