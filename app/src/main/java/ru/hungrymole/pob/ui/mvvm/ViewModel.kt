package ru.hungrymole.pob.ui.mvvm

import android.content.Context
import android.os.Handler
import android.os.Looper
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.subjects.PublishSubject
import ru.hungrymole.pob.extensions.toast
import timber.log.Timber

/**
 * Created by khusainov.d on 18.07.2017.
 */
abstract class ViewModel<S : Any>(
  protected val ctx: Context
) {

    abstract fun subscribeOnStart(subscriptions: CompositeDisposable)
    abstract fun subscribeOnCreate(subscriptions: CompositeDisposable)

    val state = PublishSubject.create<S>()
    abstract fun getState(): Single<S>
    fun reloadState() = getState()
            .doOnSuccess(state::onNext)
            .map { Unit }
            .doOnError(::reportError)
            .onErrorReturnItem(Unit)

    private var createSubscriptions = CompositeDisposable()
    open fun onCreate() {
        subscribeOnCreate(createSubscriptions)
        createSubscriptions += reloadState().subscribe()
    }

    private var startSubscriptions = CompositeDisposable()
    fun onStart() {
        startSubscriptions = CompositeDisposable()
        subscribeOnStart(startSubscriptions)
    }

    open fun onStop() {
        startSubscriptions.dispose()
    }

    open fun onDestroy() {
        createSubscriptions.dispose()
    }

    protected fun reportError(t: Throwable) {
        Timber.e(t)
        Looper
          .getMainLooper()
          .let(::Handler)
          .post { ctx.toast("Error occured") }
    }
}