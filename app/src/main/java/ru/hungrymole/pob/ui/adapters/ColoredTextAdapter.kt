package ru.hungrymole.pob.ui.adapters

import android.app.Activity
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import ru.hungrymole.pob.R
import ru.hungrymole.pob.components.HasColor
import ru.hungrymole.pob.extensions.color

/**
 * Created by khusainov.d on 04.09.2017.
 */
class ColoredTextAdapter<T : HasColor>(
    act: Activity,
    items: List<T>,
    layout: Int = android.R.layout.simple_spinner_dropdown_item
) : MutableArrayAdapter<T>(act, items.toMutableList(), layout) {

    val textColor = act.color(R.color.textBlackPrimary)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val text = super.getView(position, convertView, parent) as TextView
        text.setTextColor(getItem(position).color ?: textColor)
        return text
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val text = super.getDropDownView(position, convertView, parent) as TextView
        text.setTextColor(getItem(position).color ?: textColor)
        return text
    }
}