package ru.hungrymole.pob.ui.adapters

import android.app.Activity
import android.widget.ArrayAdapter

/**
 * Created by Dinar Khusainov on 06.01.2018.
 */
open class MutableArrayAdapter<T>(
    act: Activity,
    objects: List<T> = emptyList(),
    item: Int = android.R.layout.simple_spinner_dropdown_item
) : ArrayAdapter<T>(act, item, objects.toMutableList()) {

    fun setItems(items: List<T>) {
        clear()
        addAll(items)
    }
}