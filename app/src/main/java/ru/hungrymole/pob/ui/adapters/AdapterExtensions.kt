package ru.hungrymole.pob.ui.adapters

import android.app.Activity
import android.widget.ArrayAdapter

/**
 * Created by Dinar Khusainov on 31.12.2017.
 */
fun <T> Activity.spinnerAdapter(items: List<T>): ArrayAdapter<T> {
    return ArrayAdapter(this, android.R.layout.simple_spinner_dropdown_item, items)
}

fun <T> Activity.mutableAdapter(): MutableArrayAdapter<T> {
    return MutableArrayAdapter<T>(this)
}