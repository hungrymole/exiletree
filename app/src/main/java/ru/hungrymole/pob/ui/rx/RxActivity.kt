package ru.hungrymole.pob.ui.rx

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AppCompatActivity
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by khusainov.d on 22.09.2017.
 */
abstract class RxActivity : AppCompatActivity() {

    lateinit var createSubscriptions: CompositeDisposable
    lateinit var startSubscriptions: CompositeDisposable
    private val main = Handler(Looper.getMainLooper())

    override fun onCreate(savedInstanceState: Bundle?) {
        createSubscriptions = CompositeDisposable()
        super.onCreate(savedInstanceState)
        main.post { onCreate(createSubscriptions) }
    }

    override fun onStart() {
        super.onStart()
        startSubscriptions = CompositeDisposable().also(this::onStart)
    }

    override fun onStop() {
        super.onStop()
        startSubscriptions.dispose()
    }

    override fun onDestroy() {
        super.onDestroy()
        createSubscriptions.dispose()
    }

    abstract fun onCreate(subscriptions: CompositeDisposable)
    abstract fun onStart(subscriptions: CompositeDisposable)
}