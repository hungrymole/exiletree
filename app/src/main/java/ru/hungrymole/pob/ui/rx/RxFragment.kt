package ru.hungrymole.pob.ui.rx

import android.os.Bundle
import android.support.v4.app.Fragment
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by khusainov.d on 22.09.2017.
 */
abstract class RxFragment : Fragment() {

    lateinit var viewSubscriptions: CompositeDisposable
    lateinit var startSubscriptions: CompositeDisposable

    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        viewSubscriptions = CompositeDisposable().also(this::onViewCreated)
    }

    override fun onStart() {
        super.onStart()
        startSubscriptions = CompositeDisposable().also(this::onStart)
    }

    override fun onStop() {
        super.onStop()
        startSubscriptions.dispose()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        viewSubscriptions.dispose()
    }

    abstract fun onViewCreated(subscriptions: CompositeDisposable)
    abstract fun onStart(subscriptions: CompositeDisposable)
}