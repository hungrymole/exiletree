package ru.hungrymole.pob.ui.mvvm

import android.os.Bundle
import android.support.annotation.CallSuper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.ui.rx.RxFragment

/**
 * Created by khusainov.d on 18.07.2017.
 */
abstract class ViewFragment<S : Any, VM : ViewModel<S>> : RxFragment() {

    abstract val layout: Int
    abstract fun updateState(state: S)

    abstract var vm: VM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(layout, container, false)
    }

    @CallSuper
    override fun onViewCreated(subscriptions: CompositeDisposable) {
        subscriptions += vm.state.main().subscribe(this::updateState)
        vm.onCreate()
    }

    override fun onStart() {
        super.onStart()
        vm.onStart()
    }

    override fun onStop() {
        super.onStop()
        vm.onStop()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        vm.onDestroy()
    }
}