package ru.hungrymole.pob.ui.list

import android.content.Context
import io.reactivex.disposables.CompositeDisposable
import ru.hungrymole.pob.ui.mvvm.ViewModel

/**
 * Created by Dinar Khusainov on 27.08.2017.
 */
abstract class ListViewModel<IT>(ctx: Context) : ViewModel<List<IT>>(ctx) {
    override fun subscribeOnStart(subscriptions: CompositeDisposable) {}
    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {}
}