package ru.hungrymole.pob.ui.rx

import android.os.Looper
import io.reactivex.disposables.Disposable

/**
 * Created by khusainov.d on 22.09.2017.
 */

operator fun RxActivity?.plusAssign(d: Disposable): Unit {
    this ?: return
    if (Looper.myLooper() != Looper.getMainLooper()) throw RuntimeException("Not on the main thread")
    createSubscriptions.addAll(d)
}

operator fun RxFragment?.plusAssign(d: Disposable): Unit {
    this ?: return
    if (Looper.myLooper() != Looper.getMainLooper()) throw RuntimeException("Not on the main thread")
    viewSubscriptions.addAll(d)
}