package ru.hungrymole.pob.ui.list

import android.os.Bundle
import android.support.v7.util.DiffUtil
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.support.v7.widget.scrollStateChanges
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.plusAssign
import kotlinx.android.synthetic.main.fragment_recycler.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.extensions.act
import ru.hungrymole.pob.extensions.forEachChild
import ru.hungrymole.pob.extensions.rx.computationSubscribe
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.ui.FabFragment
import ru.hungrymole.pob.ui.rx.plusAssign

/**
 * Created by khusainov.d on 03.08.2017.
 */
abstract class
RecyclerFragment<T, VM : ListViewModel<T>, VH : RecyclerHolder> : FabFragment<List<T>, VM>() {
    protected val items = mutableListOf<T>()
    protected val adapter = RecyclerAdapter()

    override val layout = R.layout.fragment_recycler
    abstract fun createView(viewType: Int, parent: ViewGroup): View
    abstract fun createHolder(viewType: Int, view: View): VH
    abstract fun VH.bind(item: T)
    abstract fun VH.subscribeHolder()
    abstract fun areContentsTheSame(old: T, new: T): Boolean
    open fun canSwipe(vh: VH, item: T) = false
    open fun onSwiped(vh: VH, item: T) {}
    open fun getViewType(pos: Int) = 0
    open val emptyStateMessage: String get() = "List is empty"

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        recycler.layoutManager = LinearLayoutManager(act)
        recycler.addItemDecoration(DividerItemDecoration(act, DividerItemDecoration.VERTICAL))
        recycler.adapter = adapter
        object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {

            override fun onSwiped(vh: RecyclerView.ViewHolder, direction: Int) {
                onSwiped(vh as VH, items[vh.adapterPosition])
            }

            override fun getSwipeDirs(recyclerView: RecyclerView?, vh: RecyclerView.ViewHolder): Int {
                val pos = vh
                  .adapterPosition
                  .takeIf { it != -1 }
                  ?: return 0
                val canSwipe = canSwipe(vh as VH, items[pos])
                return if (!canSwipe) 0 else super.getSwipeDirs(recyclerView, vh)
            }
            override fun onMove(recyclerView: RecyclerView?, viewHolder: RecyclerView.ViewHolder?, target: RecyclerView.ViewHolder?) = false

        }.let { ItemTouchHelper(it) }.attachToRecyclerView(recycler)

        recycler_empty_text.text = emptyStateMessage
    }


    override fun onDestroyView() {
        super.onDestroyView()
        recycler?.forEachChild {//todo fix crash
            (recycler.findContainingViewHolder(it) as? VH)?.subscriptions?.dispose()
        }
    }

    override fun updateState(state: List<T>) = updateItems(state)

    override fun onStart(subscriptions: CompositeDisposable) {
        //todo state
        subscriptions += recycler.scrollStateChanges()
                .filter { it == RecyclerView.SCROLL_STATE_IDLE }
                .subscribe {  }
    }

    protected var currentDiff: Disposable? = null
    fun updateItems(new: List<T>) {
        currentDiff?.dispose()
        currentDiff = Single.fromCallable { DiffUtil.calculateDiff(ItemDiffer(items, new)) }
                .computationSubscribe()
                .main()
                .subscribe { diff ->
                    items.clear()
                    items.addAll(new)
                    diff.dispatchUpdatesTo(adapter)
                    toggleEmptyState()
                }
                .also(::plusAssign)//add to fragment as well lifecycle
    }

    protected fun toggleEmptyState() {
        recycler.visible = items.isNotEmpty()
        recycler_empty_text.visible = !recycler.visible
    }

    private inner class ItemDiffer(val old: List<T>, val new: List<T>) : DiffUtil.Callback() {
        override fun areContentsTheSame(oldI: Int, newI: Int) = areContentsTheSame(new[newI], old[oldI])
        override fun areItemsTheSame(oldI: Int, newI: Int) = new[newI] == old[oldI]
        override fun getOldListSize() = old.size
        override fun getNewListSize() = new.size
    }

    protected inner class RecyclerAdapter : RecyclerView.Adapter<VH>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VH {
            val view = createView(viewType, parent)
            val holder = createHolder(viewType, view)
            return holder.apply {
                processEvents = false
                subscribeHolder()
                processEvents = true
            }
        }

        override fun onBindViewHolder(holder: VH, position: Int) {
            processEvents = false
            holder.bind(holder.item)
            processEvents = true
        }

        override fun getItemViewType(position: Int) = getViewType(position)
        override fun getItemCount() = items.size
    }

    //counter RxBinding InitialValueObservable and binding events
    private var processEvents = false
    protected fun <T> Observable<T>.filterOnlyUserInput(): Observable<T> {
        return filter { processEvents }
    }

    protected val RecyclerHolder.item get() = items[adapterPosition]

}