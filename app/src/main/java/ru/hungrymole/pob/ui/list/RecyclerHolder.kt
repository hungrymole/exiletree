package ru.hungrymole.pob.ui.list

import android.support.v7.widget.RecyclerView
import android.view.View
import io.reactivex.disposables.CompositeDisposable

/**
 * Created by Dinar Khusainov on 30.12.2017.
 */
open class RecyclerHolder(val view: View) : RecyclerView.ViewHolder(view) {
    var subscriptions = CompositeDisposable()
}