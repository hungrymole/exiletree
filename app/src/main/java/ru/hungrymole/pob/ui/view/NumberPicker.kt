package ru.hungrymole.pob.ui.view

import android.content.Context
import android.util.AttributeSet
import android.view.Gravity
import android.widget.TextView
import com.michaelmuenzer.android.scrollablennumberpicker.ScrollableNumberPicker
import io.reactivex.subjects.PublishSubject

/**
 * Created by khusainov.d on 10.08.2017.
 */
class NumberPicker(context: Context?, attrs: AttributeSet?) : ScrollableNumberPicker(context, attrs) {

    val changes get() = _changes

    private val _changes = PublishSubject.create<Int>()

    override fun onAttachedToWindow() {
        super.onAttachedToWindow()
        (getChildAt(1) as? TextView)?.apply {
            minEms = 1
            gravity = Gravity.CENTER_HORIZONTAL
        }
        setListener(_changes::onNext)
    }

    override fun setValue(value: Int) {
        super.setValue(value)
    }
}