package ru.hungrymole.pob.extensions

import android.util.SparseArray

/**
 * Created by khusainov.d on 21.09.2017.
 */

fun <T> Iterable<T>.filterAndAdd(out: MutableCollection<T>, predicate: (T) -> Boolean): Iterable<T> {
    val (found, rest) = partition(predicate)
    out += found
    return rest
}

fun Iterable<Int>.isProgression(): Boolean {
    var prev = first()
    for (current in drop(1)) {
        if (prev > current) {
            return false
        }
        prev = current
    }
    return true
}

fun <T: Any> Iterable<T>.filterComplexAbbreviation(match: String): Iterable<Pair<T, List<IntRange>>> {
    return map {
        val text = it.toString().toLowerCase()
        var restOfQuery = match.toLowerCase()
        val matches = text.split(" ").mapNotNull { word ->
            if (restOfQuery.isEmpty()) return@mapNotNull null
            val charsMatched = word
                    .zip(restOfQuery) { wc, qc -> wc == qc }
                    .takeWhile { it }
                    .size
            restOfQuery = restOfQuery.drop(charsMatched)
            if (charsMatched != 0) {
                val start = text.indexOf(word)
                start.rangeTo(start + charsMatched - 1)
            } else null
        }

        if (restOfQuery.isEmpty() &&
                matches.isNotEmpty() &&
                matches.map { it.start }.isProgression()) {
            it to matches
        } else null
    }.filterNotNull()
}

fun <T : Any> SparseArray<T>.values(): List<T> {
    return (0 until size()).mapTo(ArrayList<T>(size()), ::valueAt)
}

fun IntRange.moveBy(size: Int): IntRange {
    return IntRange(start + size, endInclusive + size)
}

fun <T> Map<Int, T>.toSparseArray(): SparseArray<T> {
    return SparseArray<T>(size).also {
        forEach { (k, v) -> it.put(k, v) }
    }
}

inline fun <T, C : Iterable<T>> C.onEachIndexed(action: (Int, T) -> Unit): C {
    var index = 0
    for (item in this) action(index++, item)
    return this
}

inline fun <K, V> MutableMap<K, V>.getOrCreate(key: K, create: (K) -> V): V =
    get(key) ?: create(key).also { put(key, it) }