package ru.hungrymole.pob.extensions

import android.app.Activity
import android.content.Context
import android.support.annotation.LayoutRes
import android.support.v4.app.Fragment
import android.view.View
import android.view.ViewGroup

/**
 * Created by Dinar Khusainov on 31.12.2017.
 */

val Fragment.ctx get(): Context = act.applicationContext
val Fragment.act get(): Activity = activity ?: error("Activity not attached")

fun Fragment.inflate(@LayoutRes layout: Int, parent: ViewGroup, measure: Boolean = false): View {
    return act.layoutInflater.inflate(layout, parent, measure)
}