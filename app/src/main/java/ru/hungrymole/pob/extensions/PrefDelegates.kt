package ru.hungrymole.pob.extensions

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import kotlin.reflect.KProperty


abstract class PrefDelegate<T>(
  ctx: Context
) {

  protected val prefs: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(ctx)

  abstract operator fun getValue(thisRef: Any?, property: KProperty<*>): T
  abstract operator fun setValue(thisRef: Any?, property: KProperty<*>, value: T)
}

fun Context.nullableStringPref() = object : PrefDelegate<String?>(this) {
  override fun getValue(thisRef: Any?, property: KProperty<*>) = prefs.getString(property.name, null)
  override fun setValue(thisRef: Any?, property: KProperty<*>, value: String?) = prefs.edit().putString(property.name, value).apply()
}

fun Context.stringPref(defaultValue: String) = object : PrefDelegate<String>(this) {
  override fun getValue(thisRef: Any?, property: KProperty<*>) = prefs.getString(property.name, defaultValue)
  override fun setValue(thisRef: Any?, property: KProperty<*>, value: String) = prefs.edit().putString(property.name, value).apply()
}

fun Context.booleanPref(defValue: Boolean = false) = object : PrefDelegate<Boolean>(this) {
  override fun getValue(thisRef: Any?, property: KProperty<*>) = prefs.getBoolean(property.name, defValue)
  override fun setValue(thisRef: Any?, property: KProperty<*>, value: Boolean) = prefs.edit().putBoolean(property.name, value).apply()
}

fun Context.intPref(defValue: Int) = object : PrefDelegate<Int>(this) {
  override fun getValue(thisRef: Any?, property: KProperty<*>) = prefs.getInt(property.name, defValue)
  override fun setValue(thisRef: Any?, property: KProperty<*>, value: Int) = prefs.edit().putInt(property.name, value).apply()
}