package ru.hungrymole.pob.extensions.rx

import io.reactivex.Maybe
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import ru.hungrymole.pob.lua.luaScheduler

/**
 * Created by khusainov.d on 03.07.2017.
 */

fun <T> Observable<T>.ioSubscribe() = subscribeOn(Schedulers.io())
fun <T> Single<T>.ioSubscribe() = subscribeOn(Schedulers.io())
fun <T> Maybe<T>.ioSubscribe() = subscribeOn(Schedulers.io())
fun <T> Observable<T>.io() = observeOn(Schedulers.io())
fun <T> Single<T>.io() = observeOn(Schedulers.io())
fun <T> Maybe<T>.io() = observeOn(Schedulers.io())

fun <T> Observable<T>.computationSubscribe() = subscribeOn(Schedulers.computation())
fun <T> Single<T>.computationSubscribe() = subscribeOn(Schedulers.computation())
fun <T> Observable<T>.computation() = observeOn(Schedulers.computation())
fun <T> Single<T>.computation() = observeOn(Schedulers.computation())
fun <T> Maybe<T>.computation() = observeOn(Schedulers.computation())

fun <T> Observable<T>.mainSubscribe() = subscribeOn(AndroidSchedulers.mainThread())
fun <T> Single<T>.mainSubscribe() = subscribeOn(AndroidSchedulers.mainThread())
fun <T> Maybe<T>.mainSubscribe() = subscribeOn(AndroidSchedulers.mainThread())
fun <T> Observable<T>.main() = observeOn(AndroidSchedulers.mainThread())
fun <T> Single<T>.main() = observeOn(AndroidSchedulers.mainThread())
fun <T> Maybe<T>.main() = observeOn(AndroidSchedulers.mainThread())

fun <T> Observable<T>.lua() = observeOn(luaScheduler)
fun <T> Single<T>.lua() = observeOn(luaScheduler)
fun <T> Maybe<T>.lua() = observeOn(luaScheduler)
fun <T> Observable<T>.luaSubscribe() = subscribeOn(luaScheduler)
fun <T> Single<T>.luaSubscribe() = subscribeOn(luaScheduler)
fun <T> Maybe<T>.luaSubscribe() = subscribeOn(luaScheduler)