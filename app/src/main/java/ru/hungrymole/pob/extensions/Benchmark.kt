package ru.hungrymole.pob.extensions

import android.os.SystemClock
import timber.log.Timber

/**
 * Created by khusainov.d on 10.08.2017.
 */
inline fun <T> measure(msg: String? = "", block: () -> T): T {
    val start = System.currentTimeMillis()
    val result = block()
    val time = System.currentTimeMillis() - start
    val msg = msg ?: ""
    Timber.tag("Measure").d("$msg - $time")
    return result
}

inline fun <T> measureNano(msg: String? = "", block: () -> T): T {
    val start = SystemClock.elapsedRealtimeNanos()
    val result = block()
    val time = SystemClock.elapsedRealtimeNanos() - start
    val msg = msg ?: ""
    Timber.tag("Measure").d("$msg - $time")
    return result
}