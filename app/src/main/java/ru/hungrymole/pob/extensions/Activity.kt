package ru.hungrymole.pob.extensions

import android.Manifest
import android.app.Activity
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.Maybe


fun Activity.readPermissions(): Maybe<Unit> =
    RxPermissions(this)
        .request(Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
        .firstElement()
        .onErrorReturn { false }
        .filter { it }
        .map { Unit }