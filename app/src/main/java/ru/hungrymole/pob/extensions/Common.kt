package ru.hungrymole.pob.extensions

class Optional<T : Any?> private constructor (
  val value: T
) {
  companion object {
    fun empty() = Optional(null)
    fun <T : Any?> from(value: T) = Optional(value)
  }
}