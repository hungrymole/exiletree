package ru.hungrymole.pob.extensions

import android.app.Activity
import android.app.ActivityManager
import android.content.ClipboardManager
import android.content.Context
import android.content.ContextWrapper
import android.content.pm.ActivityInfo
import android.content.res.AssetManager
import android.os.Bundle
import android.os.Parcelable
import android.util.DisplayMetrics
import android.util.Log
import android.view.LayoutInflater
import android.view.Surface
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.Toast

/**
 * Created by khusainov.d on 03.07.2017.
 */

val Activity.dpToPxRatio: Float get() = DisplayMetrics().run {
    windowManager.defaultDisplay.getMetrics(this)
    density
}

fun AssetManager.fileAsString(path : String) = open(path).readBytes().let { String(it) }

//fun logEvent(name: String, vararg args: Pair<String, String>) = CustomEvent(name).apply {
//    args.forEach { putCustomAttribute(it.first, it.second) }
//    Answers.getInstance().logCustom(this)
//}
//
//fun Context?.error(m: Any, e: Throwable, showToast: Boolean = true): Unit {
//    if (showToast) {
//        toast("Произошла ошибка: $m")
//    }
//    Crashlytics.logException(e)
//    Log.e("Error", m.toString(), e)
//}

fun Any?.logD(m: Any?, tag: String? = null): Unit {
    if (this != null && m != null) {
        val tag = tag ?: this::class.java.name
        Log.d(tag, m.toString())
    }
}

val Context?.isInForeground: Boolean
    get() {
        this ?: return false
        val activityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val appProcesses = activityManager.runningAppProcesses ?: return false
        for (appProcess in appProcesses) {
            if (appProcess.importance == ActivityManager.RunningAppProcessInfo.IMPORTANCE_FOREGROUND && appProcess.processName == packageName) {
                return true
            }
        }
        return false
    }

var Activity.orientationEnabled: Boolean
    get() = true
    set(value) {
        if (!value) {
            requestedOrientation = when(windowManager.defaultDisplay.rotation) {
                Surface.ROTATION_0 -> ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
                Surface.ROTATION_90 -> ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE
                Surface.ROTATION_180 -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_PORTRAIT
                Surface.ROTATION_270 -> ActivityInfo.SCREEN_ORIENTATION_REVERSE_LANDSCAPE
                else -> throw IllegalStateException("WTF")
            }
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
    }

fun Context?.toast(m: Any?): Unit {
    this?.let { Toast.makeText(it, m.toString(), Toast.LENGTH_LONG).show() }
}

var View.visible: Boolean
    get() = if (visibility == View.VISIBLE) true else false
    set(value) {
        visibility = if (value) View.VISIBLE else View.GONE
    }

fun View.inflater(): LayoutInflater {
    return LayoutInflater.from(context)
}

inline fun ViewGroup.forEachChild(block: (View) -> Unit) {
    for (i in 0 until childCount) block(getChildAt(i))
}

fun View.getActivity(): Activity {
    var ctx = context
    while (ctx is ContextWrapper) {
        if (ctx is Activity) {
            return ctx
        }
        ctx = ctx.baseContext
    }
    throw IllegalStateException("Activity for $this not found")
}

fun Context.color(id: Int) = resources.getColor(id)

inline fun <reified T> Activity.getService(name: String): T = getSystemService(name) as T

fun Activity.clipboardService(): ClipboardManager = getService(Context.CLIPBOARD_SERVICE)

fun Activity.hideKeyboard() {
    val view = currentFocus ?: return
    val inputManager = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputManager.hideSoftInputFromWindow(view.windowToken, 0)
}

//from anko
fun bundleOf(vararg params: Pair<String, Any?>): Bundle {
    val b = Bundle()
    for (p in params) {
        val (k, v) = p
        when (v) {
            null            -> b.putSerializable(k, null)
            is Boolean      -> b.putBoolean(k, v)
            is Byte         -> b.putByte(k, v)
            is Char         -> b.putChar(k, v)
            is Short        -> b.putShort(k, v)
            is Int          -> b.putInt(k, v)
            is Long         -> b.putLong(k, v)
            is Float        -> b.putFloat(k, v)
            is Double       -> b.putDouble(k, v)
            is String       -> b.putString(k, v)
            is CharSequence -> b.putCharSequence(k, v)
            is Parcelable   -> b.putParcelable(k, v)
            is BooleanArray -> b.putBooleanArray(k, v)
            is ByteArray    -> b.putByteArray(k, v)
            is CharArray    -> b.putCharArray(k, v)
            is DoubleArray  -> b.putDoubleArray(k, v)
            is FloatArray   -> b.putFloatArray(k, v)
            is IntArray     -> b.putIntArray(k, v)
            is LongArray    -> b.putLongArray(k, v)
            is Bundle     -> b.putBundle(k, v)
            else          -> error("Unsupported bundle component (${v.javaClass})")
        }
    }
    return b
}