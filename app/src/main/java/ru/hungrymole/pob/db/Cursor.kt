package ru.hungrymole.pob.db

import android.database.Cursor
import java.util.*

inline fun <T, C : Cursor> C.strictFirst(f: (C) -> T): T =
    when {
        moveToFirst() -> f(this)
        else          -> error("empty cursor")
    }

inline fun <T, C : Cursor> C.maybeFirst(f: (C) -> T?): T? =
    when {
        moveToFirst() -> f(this)
        else          -> null
    }

inline fun <T> Cursor.strictFirstConsume(f: (Cursor) -> T): T =
    use { strictFirst(f) }

inline fun <T, C : Cursor> C.firstConsume(f: (C) -> T?): T? =
    use { maybeFirst(f) }

inline fun <K, T> Cursor.groupMap(keySelector: (Cursor) -> K, elemCreator: (Cursor) -> T): Map<K, List<T>> =
    HashMap<K, ArrayList<T>>().apply {
        forEachRow {
            val key = keySelector(it)
            val list = getOrPut(key) { ArrayList<T>() }
            list.add(elemCreator(it))
        }

        moveToFirst()
    }

/**
 * https://en.wikipedia.org/wiki/Fold_(higher-order_function)
 */
inline fun <T, C : Cursor> C.fold(zero: T, f: (C, T) -> T): T =
    if (moveToFirst()) {
        moveToPrevious()
        var accum = zero
        while (moveToNext()) {
            accum = f(this, accum)
        }
        accum
    } else zero

inline fun <T, C : Cursor> C.forEachRow(f: (C) -> T): Unit =
    fold(Unit) { c, x -> f(c) }

inline fun <T, C : Cursor> C.forEachConsume(f: (C) -> T): Unit =
    use { forEachRow(f) }

inline fun <T, C : Cursor> C.map(f: (C) -> T): List<T> =
    mapMutable(f)

inline fun <T, C : Cursor> C.mapMutable(f: (C) -> T): MutableList<T> =
    fold(ArrayList<T>(count)) { c, list -> list.apply { add(f(c)) } }

inline fun <T, C : Cursor> C.mapConsume(f: C.() -> T): List<T> =
    use { map(f) }



inline fun <T> Cursor.nullableByIndex(idx: Int, f: () -> T): T? =
    when {
        isNull(idx) -> null
        else        -> f()
    }

inline fun <T> Cursor.nullableByName(s: String, f: Cursor.(Int) -> T): T? {
    val idx = getColumnIndex(s)
    return when {
        idx == -1 -> null
        else      -> nullableByIndex(idx) { f(idx) }
    }
}

fun Cursor.optString(s: String): String? =
    nullableByName(s) { getString(it) }

fun Cursor.getInt(s: String): Int =
    getInt(getColumnIndex(s))

fun Cursor.getBool(s: String): Boolean =
    getInt(s) != 0

fun Cursor.getBool(i: Int): Boolean =
    getInt(i) != 0

fun Cursor.maybeBool(s: String): Boolean? =
    nullableByName(s) { getBool(it) }

fun Cursor.maybeInt(s: String): Int? =
    nullableByName(s) { getInt(it) }

fun Cursor.maybeLong(s: String): Long? =
    nullableByName(s) { getLong(it) }

fun Cursor.maybeDouble(s: String): Double? =
    nullableByName(s) { getDouble(s) }

fun Cursor.getLong(s: String): Long =
    getLong(getColumnIndex(s))

fun Cursor.getString(s: String): String =
    getString(getColumnIndex(s))

fun Cursor.maybeFloat(s: String): Float? =
    nullableByName(s) { getFloat(it) }

fun Cursor.getFloat(s: String): Float =
    getFloat(getColumnIndex(s))

fun Cursor.maybeBlob(s: String): ByteArray? =
    nullableByName(s) { getBlob(it) }

fun Cursor.getBlob(s: String): ByteArray =
    getBlob(getColumnIndex(s))

fun Cursor.maybeType(s: String): Int? =
    nullableByName(s) { getType(it) }

fun Cursor.getType(s: String): Int =
    getType(getColumnIndex(s))

inline fun <reified T : Enum<T>> Cursor.getEnum(s: String): T =
    enumValueOf(getString(s))

inline fun <reified T : Enum<T>> Cursor.getEnum(i: Int): T =
    enumValueOf(getString(i))

// TODO cache this iteration
inline fun <reified T : Enum<T>> enumValueOf(textVal: String): T =
    T::class.java.enumConstants.first { it.name == textVal }

fun Cursor.getDouble(s: String): Double =
    getDouble(getColumnIndex(s))

fun <C : Cursor> C.firstOrNull(): C? =
    getOrNull(0)

fun <C : Cursor> C.firstOrNullConsume(): C? =
    firstOrNull()
        .also { it ?: close() }

fun <C : Cursor> C.first(): C =
    firstOrNull() ?: error("expecting a non empty cursor")

fun <C : Cursor> C.last(): C =
    this[count - 1]

fun Cursor.isNotEmpty(): Boolean =
    count > 0

fun Cursor.isNotEmptyOrConsume(): Boolean =
    when {
        isNotEmpty() -> true
        else         -> use { false }
    }

fun Cursor.isEmpty(): Boolean =
    !isNotEmpty()

inline fun <T> Cursor.nullable(column: String, f: (Int) -> T): T? {
    val idx = getColumnIndex(column)
    return when {
        isNull(idx) -> null
        else        -> f(idx)
    }
}

inline fun <T> Cursor.notNull(column: String, f: (Int) -> T): T =
    nullable(column, f) ?: error("expecting $column not to be null")

fun firstLongColumn(it: Cursor): Long =
    it.getLong(0)

fun firstIntColumn(it: Cursor): Int =
    it.getInt(0)

fun <C : Cursor> C.getOrNull(idx: Int): C? =
    when {
        moveToPosition(idx) -> this
        else                -> null
    }

operator fun <C : Cursor> C.get(idx: Int): C =
    getOrNull(idx) ?: error("index out of bounds [$idx], count is $count")
