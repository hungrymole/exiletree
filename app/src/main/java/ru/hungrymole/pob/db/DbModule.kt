package ru.hungrymole.pob.db

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import dagger.Module
import dagger.Provides
import ru.hungrymole.pob.BuildConfig
import ru.hungrymole.pob.generator.db.DB
import java.io.File
import javax.inject.Singleton

/**
 * Created by Dinar Khusainov on 30.01.2018.
 */
@Module
class DbModule {

    @Provides
    @Singleton
    fun makeDb(ctx: Context): SQLiteDatabase {
        val filename = DB.filename
        val dbFile = File(ctx.filesDir.parentFile, "databases").run {
            mkdirs()
            File(this, filename)
        }
        if (!dbFile.exists() || BuildConfig.DEBUG) {
            ctx.assets
                .open("generated/$filename")
                .readBytes()
                .let(dbFile::writeBytes)
        }
        return object : SQLiteOpenHelper(ctx, filename, null, 1) {
            override fun onCreate(db: SQLiteDatabase) = Unit
            override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) = Unit
            override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) = Unit
        }.readableDatabase
    }
}