package ru.hungrymole.pob.db

import android.database.Cursor
import android.database.sqlite.SQLiteDatabase
import ru.hungrymole.pob.extensions.measure
import ru.hungrymole.pob.generator.db.DB

/**
 * Created by Dinar Khusainov on 28.01.2018.
 */
private object refs : DB.ReferenceTable() {
    override val tableName = ""
}

data class Reference(
    val id: Int,
    val name: String
) {
    constructor(c: Cursor) : this(
        c.getInt(refs.id),
        c.getString(refs.name)
    )

    override fun toString() = name
}

fun DB.ReferenceTable.load(db: SQLiteDatabase): List<Reference> {
    return db.query("select * from $tableName")
        .mapConsume(::Reference)
}

fun SQLiteDatabase.query(sql: String): Cursor {
    return measure(sql) {
        rawQuery(sql, emptyArray())
    }
}