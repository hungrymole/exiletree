package ru.hungrymole.pob.network

import dagger.Module
import dagger.Provides
import okhttp3.ConnectionPool
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import okhttp3.logging.HttpLoggingInterceptor.Level.BODY
import okhttp3.logging.HttpLoggingInterceptor.Level.HEADERS
import ru.hungrymole.pob.BuildConfig
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Created by Dinar Khusainov on 03.02.2018.
 */
@Module
class NetworkModule {

    companion object {
        const val OKHTTP_POE = "pathofexile"
        const val OKHTTP_DEFAULT = "default"
    }

    private val connectionPool = ConnectionPool()

    private val headerLogger = HttpLoggingInterceptor
        .Logger { Timber.tag("OkHttp").i(it) }
        .let(::HttpLoggingInterceptor)
        .apply { level = HEADERS }

    private val fullLogger = HttpLoggingInterceptor
        .Logger { Timber.tag("OkHttp").d(it) }
        .let(::HttpLoggingInterceptor)
        .apply { level = BODY }

    private fun buildBaseOkhttp(also: OkHttpClient.Builder.() -> Unit = {}): OkHttpClient =
        OkHttpClient.Builder()
            .connectionPool(connectionPool)
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(30, TimeUnit.SECONDS)
            .also(also)
            .build()

    @Provides
    @Singleton
    fun poeCookies(): PoeCookieJar = PoeCookieJar()

    @Provides
    @Singleton
    @Named(OKHTTP_POE)
    fun poeClient(jar: PoeCookieJar): OkHttpClient = buildBaseOkhttp {
        cookieJar(jar)
        (if (BuildConfig.DEBUG) fullLogger else headerLogger)
            .let(::addInterceptor)
    }

    @Provides
    @Singleton
    @Named(OKHTTP_DEFAULT)
    fun mainClient(): OkHttpClient = buildBaseOkhttp {
        (if (BuildConfig.DEBUG) fullLogger else headerLogger)
            .let(::addInterceptor)
    }
}