package ru.hungrymole.pob.network

import okhttp3.Cookie
import okhttp3.CookieJar
import okhttp3.HttpUrl

/**
 * Created by Dinar Khusainov on 20.02.2018.
 */

class PoeCookieJar : CookieJar {

    private val store = mutableMapOf<String, MutableList<Cookie>>()

    override fun saveFromResponse(url: HttpUrl, cookies: MutableList<Cookie>) {
        store.getOrPut(url.host(), ::mutableListOf).addAll(cookies)
    }

    override fun loadForRequest(url: HttpUrl): MutableList<Cookie> {
        return store
            .getOrElse(url.host(), ::mutableListOf)
            .reversed()
            .toMutableList()
    }

    fun getPOESESSIDs(): List<String> {
        return store.values.firstOrNull()
            ?.filter { it.name() == "POESESSID" }
            ?.map { it.value() }
            .orEmpty()
    }

    var count = 0
    fun clear() {
        count = 0
        store.clear()
    }
}