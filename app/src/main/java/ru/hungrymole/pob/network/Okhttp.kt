package ru.hungrymole.pob.network

import io.reactivex.Single
import okhttp3.*

    /**
     * Created by Dinar Khusainov on 03.02.2018.
     */

private typealias RequestHeader = Pair<String, Any>
private typealias PostParam = Pair<String, Any>

fun OkHttpClient.postBody(url: String, vararg params: PostParam): Single<Response> {
    val body = FormBody.Builder()
    params.forEach { (name, value) -> body.add(name, value.toString()) }
    return request(url, "POST", body = body.build())
}

fun OkHttpClient.get(url: String, vararg headers: RequestHeader): Single<Response> {
    return request(url, "GET") {
        headers.forEach { (name, value) -> header(name, value.toString()) }
    }
}

private fun OkHttpClient.request(url: String,
                                 method: String,
                                 body: RequestBody? = null,
                                 also: Request.Builder.() -> Unit = {}): Single<Response> {
    return Request.Builder()
        .url(url)
        .method(method, body)
        .also(also)
        .build()
        .let(::newCall)
        .let(::CallExecuteSingle)
}

class HttpException(
    val code: Int,
    val response: String?
) : Throwable(response)