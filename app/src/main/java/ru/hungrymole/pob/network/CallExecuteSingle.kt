package ru.hungrymole.pob.network

import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable
import io.reactivex.exceptions.CompositeException
import io.reactivex.exceptions.Exceptions
import io.reactivex.plugins.RxJavaPlugins
import okhttp3.Call
import okhttp3.Response

//https://github.com/square/retrofit/blob/master/retrofit-adapters/rxjava2/src/main/java/retrofit2/adapter/rxjava2/CallExecuteObservable.java
class CallExecuteSingle(private val originalCall: Call) : Single<Response>() {

    override fun subscribeActual(obs: SingleObserver<in Response>) {
        val call = originalCall.clone()
        obs.onSubscribe(CallDisposable(call))

        try {
            val response = call.execute()
            if (!call.isCanceled) {
                val body = response?.body()
                when {
                    !response.isSuccessful -> {
                        val errorBody = body?.string()
                        response.close()
                        obs.onError(HttpException(response.code(), errorBody))
                    }
                    else                   -> obs.onSuccess(response)
                }
            }
        } catch (t: Throwable) {
            Exceptions.throwIfFatal(t)
            if (!call.isCanceled) {
                try {
                    obs.onError(t)
                } catch (inner: Throwable) {
                    Exceptions.throwIfFatal(inner)
                    RxJavaPlugins.onError(CompositeException(t, inner))
                }
            }
        }
    }

    private class CallDisposable(private val call: Call) : Disposable {
        override fun dispose() = call.cancel()
        override fun isDisposed(): Boolean = call.isCanceled
    }
}