package ru.hungrymole.pob.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import javax.inject.Inject
import javax.inject.Singleton


/**
 * Created by Dinar Khusainov on 09.02.2018.
 */
@Singleton
class NetworkMonitor @Inject constructor(
    ctx: Context
) : ConnectivityManager.NetworkCallback() {

    val updates: Observable<Boolean> get() = _updates

    private val _updates = BehaviorSubject.create<Boolean>()

    init {
        val mgr = ctx.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val request = NetworkRequest.Builder()
            .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .build()
        mgr.registerNetworkCallback(request, this)
        notify(mgr.activeNetworkInfo?.isConnected ?: false)
    }

    fun isConnected(): Boolean = _updates.value ?: false

    private fun notify(connected: Boolean) = _updates.onNext(connected)
    override fun onLost(network: Network?) = notify(false)
    override fun onAvailable(network: Network?) = notify(true)
}