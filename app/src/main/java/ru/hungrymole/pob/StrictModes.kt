package ru.hungrymole.pob

import android.os.StrictMode

object StrictModes {

  fun mkVmPolicy() =
      StrictMode.VmPolicy.Builder()
      .detectAll()
      .penaltyLog()
      .build()

  fun mkThredPolicy() =
      StrictMode.ThreadPolicy.Builder()
      .detectAll()
      .permitDiskReads()//too much log spam
      .permitDiskWrites()
      .penaltyLog()
      .build()

  fun enable() = when {
    BuildConfig.DEBUG -> {
      StrictMode.setVmPolicy(mkVmPolicy())
      StrictMode.setThreadPolicy(mkThredPolicy())
    }
    else              -> Unit
  }
}