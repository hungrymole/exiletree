package ru.hungrymole.pob.buildlist

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import com.jakewharton.rxbinding2.view.clicks
import io.reactivex.Maybe
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.item_build.view.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.BuildActivity
import ru.hungrymole.pob.build.BuildManager
import ru.hungrymole.pob.build.BuildManager.Companion.LOAD_FILE
import ru.hungrymole.pob.components.ascendancyOrClassAvatar
import ru.hungrymole.pob.di.di
import ru.hungrymole.pob.dialogs.rxConfirmDialog
import ru.hungrymole.pob.extensions.*
import ru.hungrymole.pob.ui.list.RecyclerFragment
import ru.hungrymole.pob.ui.list.RecyclerHolder
import java.io.File
import javax.inject.Inject

/**
 * Created by Dinar on 16.01.2018.
 */
class BuildListFragment : RecyclerFragment<BuildInfo, BuildListVM, BuildListFragment.BuildHolder>() {

    @Inject override lateinit var vm: BuildListVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ctx.di.inject(this)
    }

    override val emptyStateMessage: String get() = """
    |No saved builds
    |${BuildManager.buildsDir.absolutePath}
    """.trimMargin()

    override val fabImage: Int? get() = R.drawable.ic_add_white_24dp

    override fun canSwipe(vh: BuildHolder, item: BuildInfo): Boolean = true

    override fun onSwiped(vh: BuildHolder, item: BuildInfo) {
        startSubscriptions += vm.deleteBuild(item).subscribe()
    }

    override fun createView(viewType: Int, parent: ViewGroup): View {
        return inflate(R.layout.item_build, parent)
    }

    override fun createHolder(viewType: Int, view: View) = BuildHolder(view)

    override fun BuildHolder.subscribeHolder() {
        subscriptions += view.clicks().subscribe {
            openBuild(item.file)
        }
    }

    override fun areContentsTheSame(old: BuildInfo, new: BuildInfo) = false

    override fun BuildHolder.bind(item: BuildInfo) {
        name.text = item.name
        item.ascendancy
                ?.let(ctx::ascendancyOrClassAvatar)
                ?.let(avatar::setImageResource)
    }

    fun openBuild(buildFile: File) {
        Intent(act, BuildActivity::class.java)
                .apply { putExtra(LOAD_FILE, buildFile) }
                .let(::startActivity)
    }

    override fun onViewCreated(subscriptions: CompositeDisposable) {
        super.onViewCreated(subscriptions)
        subscriptions += fab.clicks().flatMapMaybe {
            when {
                vm.unsaved.exists() -> act.rxConfirmDialog("Continue with unsaved build?", negative = "make new")
                else                -> Maybe.just(false)
            }
        }.subscribe { resumeUnsaved ->
            if (!resumeUnsaved) vm.unsaved.delete()
            openBuild(vm.unsaved)
        }
    }

    override fun onStart(subscriptions: CompositeDisposable) {
        super.onStart(subscriptions)
        subscriptions += act
            .readPermissions()
            .flatMapSingleElement { vm.reloadState() }
            .subscribeBy(onError = {
                act.toast("Error. Try to enable file permissions")
            })
    }

    class BuildHolder(view: View) : RecyclerHolder(view) {
        val name = view.build_name
        val level = view.build_points
        var avatar = view.build_avatar
    }
}