package ru.hungrymole.pob.buildlist

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import ru.hungrymole.pob.R


/**
 * Created by Dinar Khusainov on 29.07.2017.
 */
class BuildListActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_build_list)
        supportFragmentManager.beginTransaction()
                .add(R.id.build_list_container, BuildListFragment())
                .commit()
    }
}