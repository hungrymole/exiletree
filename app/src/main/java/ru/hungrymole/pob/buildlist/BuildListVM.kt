package ru.hungrymole.pob.buildlist

import android.content.Context
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import ru.hungrymole.pob.build.BuildManager
import ru.hungrymole.pob.build.BuildManager.Companion.UNNAMED_BUILD
import ru.hungrymole.pob.common.substring
import ru.hungrymole.pob.ui.list.ListViewModel
import timber.log.Timber
import java.io.File
import javax.inject.Inject

/**
 * Created by Dinar on 16.01.2018.
 */

class BuildListVM @Inject constructor(
  ctx: Context
) : ListViewModel<BuildInfo>(ctx) {

    private val buildsDir = BuildManager.buildsDir
    val unsaved by lazy { File(ctx.filesDir, UNNAMED_BUILD) }

    override fun getState() = Single.fromCallable {
        if (!buildsDir.exists()) {
            buildsDir.mkdirs()
        }
        buildsDir.listFiles().orEmpty().map {
            try {
                val xml = it.readText()
                val asc = xml.substring(after = "ascendClassName=\"", until = "\"")
                BuildInfo(it, it.name.replace(".xml", ""), asc)
            } catch (e: Exception) {
                Timber.e(e)
                null
            }
        }.filterNotNull()
    }

    fun deleteBuild(build: BuildInfo) =
      Single
      .fromCallable { build.file.delete() }
        .flatMap { reloadState() }


    override fun subscribeOnCreate(subscriptions: CompositeDisposable) {}
    override fun subscribeOnStart(subscriptions: CompositeDisposable) {}
}