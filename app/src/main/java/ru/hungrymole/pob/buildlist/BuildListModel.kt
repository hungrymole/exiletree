package ru.hungrymole.pob.buildlist

import java.io.File

/**
 * Created by Dinar on 16.01.2018.
 */
class BuildInfo(
        val file: File,
        val name: String,
        val ascendancy: String?
)