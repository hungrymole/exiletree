package ru.hungrymole.pob.debug

import android.util.Log
import com.crashlytics.android.core.CrashlyticsCore
import timber.log.Timber

/**
 * Created by Dinar on 26.02.2018.
 */
class CrashlyticsTree(
    private val core: CrashlyticsCore
) : Timber.Tree() {

  override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {
    core.log(message)
    Log.println(priority, tag, message)
    t?.let(core::logException)
  }
}