package ru.hungrymole.pob

/**
 * Created by Dinar Khusainov on 05.01.2018.
 */
object POEData {
    val abyssJewelTypes = listOf(
        "Ghastly Eye Jewel",
        "Hypnotic Eye Jewel",
        "Murderous Eye Jewel",
        "Searching Eye Jewel")
}