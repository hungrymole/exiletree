package ru.hungrymole.pob.di

import dagger.Module
import dagger.Provides
import io.reactivex.subjects.PublishSubject
import ru.hungrymole.pob.components.Event
import ru.hungrymole.pob.components.EventBus
import javax.inject.Singleton

/**
 * Created by khusainov.d on 27.07.2017.
 */
@Module
class RxModule {

    @Provides @Singleton
    fun buildFlag(): EventBus = PublishSubject.create<Event>()
}