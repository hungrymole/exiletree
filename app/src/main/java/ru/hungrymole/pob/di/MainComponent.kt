package ru.hungrymole.pob.di

import android.content.Context
import dagger.BindsInstance
import dagger.Component
import ru.hungrymole.pob.App
import ru.hungrymole.pob.build.di.BuildComponent
import ru.hungrymole.pob.buildlist.BuildListFragment
import ru.hungrymole.pob.components.tree.TreeModule
import ru.hungrymole.pob.db.DbModule
import ru.hungrymole.pob.lua.LuaModule
import ru.hungrymole.pob.network.NetworkModule
import javax.inject.Singleton

/**
 * Created by Dinar Khusainov on 23.10.2016.
 */
@Singleton
@Component(modules = arrayOf(
  LuaModule::class,
  RxModule::class,
  TreeModule::class,
  DbModule::class,
  NetworkModule::class)
)
interface MainComponent {
  fun buildComponent(): BuildComponent.Builder
  fun inject(app: App)
  fun inject(buildListFragment: BuildListFragment)
  fun ctx(): Context

  @Component.Builder
  interface Builder {
    @BindsInstance @Singleton fun ctx(ctx: Context): Builder
    fun build(): MainComponent
  }
}

val Context.di: MainComponent get() = (applicationContext as App).injector