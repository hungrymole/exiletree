package ru.hungrymole.pob.build.toolbar

import android.widget.ListPopupWindow
import com.jakewharton.rxbinding2.view.clicks
import kotlinx.android.synthetic.main.activity_build.*
import org.luaj.vm2.Globals
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.BuildActivity
import ru.hungrymole.pob.build.di.BuildScope
import ru.hungrymole.pob.components.general.BuildAttrsModel
import ru.hungrymole.pob.extensions.dpToPxRatio
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.ui.adapters.spinnerAdapter
import ru.hungrymole.pob.ui.rx.plusAssign
import javax.inject.Inject

/**
 * Created by Dinar Khusainov on 31.12.2017.
 */
@BuildScope
class ToolbarAttrManager @Inject constructor(
    private val act: BuildActivity,
    private val lua: Globals,
    private val buildAttrs: BuildAttrsModel
) {

    private val attrUpdates = buildAttrs.statMap

    private var current = "Total DPS:"

    init {
        act.supportActionBar?.setDisplayShowTitleEnabled(false)
        act += attrUpdates
            .main()
            .subscribe { updateMainAttr() }

        act += act.build_select_attr_button.clicks()
            .subscribe { showPopup() }
    }

    private fun showPopup() = ListPopupWindow(act).apply {
        anchorView = act.build_toolbar
        height = act.resources.getDimension(R.dimen.attr_selection_dialog_width)
            .times(act.dpToPxRatio)
            .toInt()
        val items = buildAttrs
            .currentStat
            .map { (k, v) -> if (k.isEmpty()) "" else "$k ${v}" }
        setAdapter(act.spinnerAdapter(items))
        setOnItemClickListener { _, _, position, id ->
            val attrKey = items[position].split(":")[0]
            if (attrKey.isNotBlank()) {
                current = "$attrKey:"
                updateMainAttr()
                dismiss()
            }
        }
        show()
    }

    private fun updateMainAttr() {
        val value = buildAttrs.currentStat.firstOrNull { it.first == current }?.second
        act.build_attr_value.text = "$current\n${value ?: 0}"
    }
}