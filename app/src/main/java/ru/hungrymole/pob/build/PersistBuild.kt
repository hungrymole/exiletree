package ru.hungrymole.pob.build

import android.annotation.SuppressLint
import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Dinar Khusainov on 10.02.2018.
 */
@SuppressLint("ParcelCreator")
@Parcelize
data class PersistBuild(
    val name: String,
    val xml: String
) : Parcelable