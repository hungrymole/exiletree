package ru.hungrymole.pob.build.di

import android.app.Activity
import dagger.BindsInstance
import dagger.Subcomponent
import ru.hungrymole.pob.build.BuildActivity
import ru.hungrymole.pob.components.config.ConfigSectionFragment
import ru.hungrymole.pob.components.config.ConfigsFragment
import ru.hungrymole.pob.components.general.BuildInfoFragment
import ru.hungrymole.pob.components.impor.ImportFragment
import ru.hungrymole.pob.components.impor.list.ImportCharactersFragment
import ru.hungrymole.pob.components.items.display.edit.DisplayItemEditFragment
import ru.hungrymole.pob.components.items.display.tooltip.DisplayItemTooltipFragment
import ru.hungrymole.pob.components.items.items.BulidItemsFragment
import ru.hungrymole.pob.components.items.search.ItemSearchFragment
import ru.hungrymole.pob.components.items.slots.ItemSlotsFragment
import ru.hungrymole.pob.components.skills.SkillGroupsListFragment
import ru.hungrymole.pob.components.skills.group.SkillGroupFragment
import ru.hungrymole.pob.components.tree.ui.TreeFragment
import javax.inject.Singleton

/**
 * Created by khusainov.d on 11.10.2017.
 */
@BuildScope
@Subcomponent
interface BuildComponent {
    fun inject(act: BuildActivity)

    fun inject(view: TreeFragment)
    fun inject(view: BulidItemsFragment)
    fun inject(view: SkillGroupsListFragment)
    fun inject(view: BuildInfoFragment)
    fun inject(view: ItemSearchFragment)
    fun inject(view: ImportFragment)
    fun inject(view: ConfigsFragment)
    fun inject(view: ConfigSectionFragment)
    fun inject(view: DisplayItemEditFragment)
    fun inject(view: DisplayItemTooltipFragment)
    fun inject(view: ImportCharactersFragment)
    fun inject(view: ItemSlotsFragment)
    fun inject(view: SkillGroupFragment)

  @Subcomponent.Builder
  interface Builder {
    @BindsInstance @Singleton fun act(act: BuildActivity): Builder
    fun build(): BuildComponent
  }
}

fun Activity.di(): BuildComponent  = (this as BuildActivity).injector