package ru.hungrymole.pob.build.toolbar.progress

import io.reactivex.rxkotlin.ofType
import kotlinx.android.synthetic.main.activity_build.*
import ru.hungrymole.pob.build.BuildActivity
import ru.hungrymole.pob.build.di.BuildScope
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.ui.rx.plusAssign
import javax.inject.Inject

@BuildScope
class ProgressManager @Inject constructor(
    private val act: BuildActivity,
    private val eventbus: EventBus
){

    private val progressBar = act.build_progres

    init {
        progressBar.visible = false
        act += eventbus
            .ofType<ProgressEvent>()
            .main()
            .subscribe(::process)
    }

    private fun process(event: ProgressEvent) {
        progressBar.progress = event.progress
        progressBar.visible = event.progress < 99
    }
}