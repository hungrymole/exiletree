package ru.hungrymole.pob.build

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import com.jakewharton.rxbinding2.support.design.widget.itemSelections
import io.reactivex.rxkotlin.ofType
import kotlinx.android.synthetic.main.activity_build.*
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.BuildScope
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.components.config.ConfigsFragment
import ru.hungrymole.pob.components.general.BuildInfoFragment
import ru.hungrymole.pob.components.impor.ImportFragment
import ru.hungrymole.pob.components.impor.ImportFragment.Companion.INPUT
import ru.hungrymole.pob.components.impor.ImportInput
import ru.hungrymole.pob.components.items.ItemsTabsFragment
import ru.hungrymole.pob.components.items.display.DisplayItemTabFragment
import ru.hungrymole.pob.components.skills.SkillGroupsListFragment
import ru.hungrymole.pob.components.skills.group.SkillGroupFragment
import ru.hungrymole.pob.components.tree.ui.TreeFragment
import ru.hungrymole.pob.extensions.bundleOf
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.ui.rx.plusAssign
import javax.inject.Inject

/**
 * Created by khusainov.d on 11.10.2017.
 */
@BuildScope
class Router @Inject constructor(
    val eventBus: EventBus,
    val act: BuildActivity
) : LifecycleObserver {

    private val bottomBar = act.build_navigation

    companion object {
        private const val SEL = "itemId"
        private const val DIS_GROUP = "showingDisplayGroup"
    }

    init {
        act += bottomBar.itemSelections().skip(1).filter { started }.subscribe {
            val frag = when (it.itemId) {
                R.id.nav_general -> BuildInfoFragment::class.java
                R.id.nav_tree -> TreeFragment::class.java
                R.id.nav_skills -> if (isOnDisplayGroup) SkillGroupFragment::class.java else SkillGroupsListFragment::class.java
                R.id.nav_items -> ItemsTabsFragment::class.java
                R.id.nav_config -> ConfigsFragment::class.java
                else -> null!!
            }
            goto(frag)
        }
        act += eventBus
            .ofType<PastebinImportEvent>()
            .main()
            .subscribe {
                //reload state after import
                bottomBar.selectedItemId = bottomBar.selectedItemId
            }
    }

    fun menuClick(id: Int) {
        when(id) {
            R.id.nav_import -> showImport()
        }
    }

    fun showImport(input: ImportInput? = null) {
        goto<ImportFragment>(args = bundleOf(INPUT to input))
    }

    private var pendingFirstLoad: Int? = null
    private var started = false
    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        started = false
    }
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun onStart() {
        started = true
        pendingFirstLoad?.let(bottomBar::setSelectedItemId)
        pendingFirstLoad = null
    }

    fun onSaveInstanceState(out: Bundle) {
        out.putInt(SEL, bottomBar.selectedItemId)
        out.putBoolean(DIS_GROUP, isOnDisplayGroup)
    }

    fun restoreState(savedInstanceState: Bundle?) {
        isOnDisplayGroup = savedInstanceState?.getBoolean(DIS_GROUP, false) ?: false
        val firstScreen = R.id.nav_general
        val savedScreen = savedInstanceState
          ?.getInt(SEL, 0)
          ?.takeIf { it != 0 }
        when {
            started && savedScreen != null -> bottomBar.selectedItemId = savedScreen
            started                        -> bottomBar.selectedItemId = firstScreen
            else                           -> pendingFirstLoad = savedScreen ?: firstScreen
        }
    }

    var isOnDisplayGroup = false
    fun showDisplayGroup() {
        goto<SkillGroupFragment>()
        isOnDisplayGroup = true
    }

    fun onBackPressed(): Boolean {
        val current = act.supportFragmentManager.first(
                SkillGroupFragment::class.java,
                DisplayItemTabFragment::class.java)
        when {
            current is SkillGroupFragment -> {
                goto<SkillGroupsListFragment>()
                act.invalidateOptionsMenu()
                isOnDisplayGroup = false
            }
            current is DisplayItemTabFragment -> goto<ItemsTabsFragment>()
        }
        return current != null
    }

    private fun FragmentManager.first(vararg classes: Class<*>): Fragment? {
        return classes.mapNotNull { findFragmentByTag(it.name) }.firstOrNull()
    }

    private inline fun <reified FT : Fragment> goto(backstack: Boolean = false, args: Bundle? = null) {
        goto(FT::class.java, backstack, args)
    }

    private fun <T : Fragment> goto(cls: Class<T>, backstack : Boolean = false, args: Bundle? = null) {
        val f = cls.newInstance()
        args?.let { f.arguments = it }
        act.supportFragmentManager.beginTransaction().replace(R.id.build_main_fragment_container, f, cls.name).apply {
            if (backstack) {
                addToBackStack(null)
            }
        }.commit()
        act.invalidateOptionsMenu()
    }
}