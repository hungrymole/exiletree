package ru.hungrymole.pob.build

import ru.hungrymole.pob.components.Event

object PastebinImportEvent : Event()
object BuildRefreshEvent : Event()
object PhoneScreenTouchedEvent : Event()
object BuildLoadEvent : Event()