package ru.hungrymole.pob.build

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import kotlinx.android.synthetic.main.activity_build.*
import org.luaj.vm2.Globals
import ru.hungrymole.pob.App
import ru.hungrymole.pob.BuildConfig
import ru.hungrymole.pob.R
import ru.hungrymole.pob.StrictModes
import ru.hungrymole.pob.build.toolbar.ToolbarAttrManager
import ru.hungrymole.pob.build.toolbar.progress.ProgressManager
import ru.hungrymole.pob.common.substring
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.components.impor.ImportInput
import ru.hungrymole.pob.di.di
import ru.hungrymole.pob.extensions.rx.lua
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.extensions.toast
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.lua.selfCall
import ru.hungrymole.pob.ui.rx.RxActivity
import ru.hungrymole.pob.ui.rx.plusAssign
import ru.hungrymole.pob.ui.view.SearchManager
import timber.log.Timber
import java.util.concurrent.TimeUnit
import javax.inject.Inject

class BuildActivity : RxActivity() {
  @Inject lateinit var lua: Globals
  @Inject lateinit var eventBus: EventBus
  @Inject lateinit var search: SearchManager
  @Inject lateinit var tooltip: TooltipManager
  @Inject lateinit var router: Router
  @Inject lateinit var pastebin: PastebinImportManager
  @Inject lateinit var buildManager: BuildManager

  //need to init now
  @Inject lateinit var attrManager: ToolbarAttrManager
  @Inject lateinit var progress: ProgressManager

  val injector by lazy {
    di
      .buildComponent()
      .act(this)
      .build()
  }

  private val loadFinished get() = ::buildManager.isInitialized

  override fun onCreate(savedInstanceState: Bundle?) {
    setContentView(R.layout.activity_build) //dependencies use views from root layout, initialize it before fragments
    super.onCreate(null)
    setSupportActionBar(build_toolbar)
    build_navigation.itemIconTintList = null

    StrictModes.enable()

    val changedConfig = lastNonConfigurationInstance != null
    when {
      changedConfig -> configChangeLoad(savedInstanceState)
      else          -> freshLoad(savedInstanceState)
    }
  }

  private fun freshLoad(savedInstanceState: Bundle?) {
    toggleLoading(loading = true)
    this += App
      .onLoaded
      .firstOrError()
      .main()
      .doOnSuccess { injector.inject(this) }
      .lua()
      .map { buildManager.loadBuild(savedInstanceState, intent) }
      .main()
      .subscribeBy(onSuccess = {
        toggleLoading(loading = false)
        commonLoad(savedInstanceState)

        eventBus.onNext(BuildRefreshEvent)
        intent.data?.let(::resolveImport)
      }, onError = {
        toast("Failed to load build: ${it.message}")
        Timber.e(it, "Build laod fail")
        finish()
      })
  }

  private fun configChangeLoad(savedInstanceState: Bundle?) {
    injector.inject(this)
    commonLoad(savedInstanceState)
  }

  private fun commonLoad(savedInstanceState: Bundle?) {
    lifecycle.addObserver(router)
    lifecycle.addObserver(buildManager)
    eventBus.onNext(BuildLoadEvent)
    router.restoreState(savedInstanceState)

    onNewIntent
      ?.data
      ?.let(::resolveImport)
      .also { onNewIntent = null }
  }

  private var onNewIntent: Intent? = null
  override fun onNewIntent(intent: Intent?) {
    super.onNewIntent(intent)
    when {
      loadFinished -> intent?.data?.let(::resolveImport)
      else         -> onNewIntent = intent
    }
  }

  private fun resolveImport(uri: Uri) {
    when (uri.host) {
      getString(R.string.poe_host) -> {
        val prefix = "/account/view-profile/"
        val accountName = uri.path.substring(prefix, "/")
        val accountName2 = uri.path.replace(prefix, "")
        ImportInput(
          accountName = accountName ?: accountName2
        ).let(router::showImport)
      }
      pastebin.host                -> pastebin.import(uri)
      else                         -> Unit
    }
  }

  fun toggleLoading(loading: Boolean) {
    build_loading_progress.visible = loading
    build_appbar.visible = !loading
    build_navigation.visible = !loading
    build_main_fragment_container.visible = !loading
  }

  override fun onCreate(subscriptions: CompositeDisposable) {}

  override fun onStart(subscriptions: CompositeDisposable) {
    subscriptions += Observable
      .interval(200, TimeUnit.MILLISECONDS)
      .filter { loadFinished }
      .lua()
      .filter { lua["build"]["buildFlag"].checkboolean() }
      .subscribe {
        lua.getBy("build").selfCall("OnFrame")
        eventBus.onNext(BuildRefreshEvent)
      }
  }

  override fun onSaveInstanceState(outState: Bundle) {
    super.onSaveInstanceState(outState)
    if (loadFinished) {
      buildManager.onSaveInstanceState(outState)
      router.onSaveInstanceState(outState)
    }
  }

  override fun onRetainCustomNonConfigurationInstance() = Unit

  override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
    search.processVoice(requestCode, resultCode, data)
  }

  override fun onCreateOptionsMenu(menu: Menu): Boolean {
    menu.clear()
    menuInflater.inflate(R.menu.activity_build_menu, menu)
    return true
  }

  override fun onOptionsItemSelected(item: MenuItem): Boolean {
    when (item.itemId) {
      R.id.action_save_build    -> buildManager.saveBuild()
      R.id.action_save_build_as -> buildManager.saveBuildAs()
      R.id.act_import_pastebin  -> pastebin.askForImport()
      R.id.act_export_pastebin  -> pastebin.export()
      R.id.act_feedback         -> openFeedbackEmail()
      else                      -> router.menuClick(item.itemId)
    }
    return true
  }

  private fun openFeedbackEmail() {
    val intent = Intent(Intent.ACTION_SENDTO).apply {
      data = Uri.parse("mailto:")
      putExtra(Intent.EXTRA_EMAIL, arrayOf(BuildConfig.report_email))
      putExtra(Intent.EXTRA_SUBJECT, "User Feedback")
    }
    Intent
      .createChooser(intent, "Send feedback email")
      .takeIf { intent.resolveActivity(packageManager) != null }
      ?.let(::startActivity)
    ?: toast("No mail client found")
  }

  override fun onBackPressed() {
    when {
      !loadFinished          -> finish()
      search.showing         -> search.hide()
      router.onBackPressed() -> Unit
      else                   -> super.onBackPressed()
    }
  }

  override fun onUserInteraction() {
    super.onUserInteraction()
    if (loadFinished) eventBus.onNext(PhoneScreenTouchedEvent)
  }
}