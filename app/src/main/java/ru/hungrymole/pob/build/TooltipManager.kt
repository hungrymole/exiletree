package ru.hungrymole.pob.build

import io.reactivex.rxkotlin.ofType
import kotlinx.android.synthetic.main.activity_build.*
import ru.hungrymole.pob.build.di.BuildScope
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.components.setTooltip
import ru.hungrymole.pob.extensions.visible
import ru.hungrymole.pob.lua.Tooltip
import ru.hungrymole.pob.ui.rx.plusAssign
import javax.inject.Inject

@BuildScope
class TooltipManager @Inject constructor(
    private val act: BuildActivity,
    private val eventBus: EventBus
) {

    private val text = act.build_tooltip_holder

    init {
        act += eventBus
            .ofType<PhoneScreenTouchedEvent>()
            .subscribe {
                text.visible = false
            }
    }


    fun showToolTip(tooltip: Tooltip) {
        text.setTooltip(tooltip)
        text.bringToFront()
        text.visible = true
    }
}