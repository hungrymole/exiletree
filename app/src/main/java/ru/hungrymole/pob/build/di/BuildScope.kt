package ru.hungrymole.pob.build.di

import javax.inject.Scope

/**
 * Created by khusainov.d on 11.10.2017.
 */
@Scope
@Retention()
annotation class BuildScope