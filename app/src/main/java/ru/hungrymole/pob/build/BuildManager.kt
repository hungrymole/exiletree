package ru.hungrymole.pob.build

import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleObserver
import android.arch.lifecycle.OnLifecycleEvent
import android.content.Intent
import android.os.Bundle
import android.os.Environment
import io.reactivex.Maybe
import io.reactivex.Single
import io.reactivex.rxkotlin.ofType
import io.reactivex.rxkotlin.subscribeBy
import org.luaj.vm2.Globals
import ru.hungrymole.pob.build.di.BuildScope
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.dialogs.rxInputTextDialog
import ru.hungrymole.pob.extensions.readPermissions
import ru.hungrymole.pob.extensions.rx.io
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.extensions.toast
import ru.hungrymole.pob.lua.call
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.ui.rx.plusAssign
import timber.log.Timber
import java.io.File
import javax.inject.Inject

@BuildScope
class BuildManager @Inject constructor(
    private val act: BuildActivity,
    private val lua: Globals,
    eventBus: EventBus
) :LifecycleObserver {

    companion object {
        const val STATE = "build_State"
        const val LOAD_FILE = "build_file"
        const val UNNAMED_BUILD = "Unnamed Build.xml"
        private const val EMPTY_BUILD = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><PathOfBuilding></PathOfBuilding>"

        val buildsDir = Environment.getExternalStoragePublicDirectory("Builds").apply { mkdirs() }!!
    }

    private var buildFile: File? = null
    private val unsaved by lazy { File(act.filesDir, UNNAMED_BUILD) }

    var loaded = false

    init {
        act += eventBus
            .ofType<BuildLoadEvent>()
            .main()
            .subscribe { loaded = true }
    }

    fun initBuild(xml: String, name: String) {
        lua["initBuild"].call(name, xml)
    }

    fun buildXml(): String =
      Single
        .fromCallable {
            lua
              .getBy("saveBuild()")
              .checkjstring()
        }
        .luaSubscribe()
        .blockingGet()

    private fun buildName(): String =
      Single
        .fromCallable {
            lua
              .getBy("build.buildName")
              .optjstring("")
        }
        .luaSubscribe()
        .blockingGet()

    fun loadBuild(state: Bundle?,
                  intent: Intent) {
        val (xml, name) = resolveBuild(state, intent)
        initBuild(xml, name)
        loaded = true
    }

    private fun resolveBuild(state: Bundle?, intent: Intent): Pair<String, String> {
        val afterDeath = state?.getParcelable<PersistBuild>(STATE)
        val file = intent.extras?.getSerializable(LOAD_FILE) as? File

        return when {
            afterDeath != null -> afterDeath.xml to afterDeath.name//todo destructive delcaration not working
            file == null       -> EMPTY_BUILD to UNNAMED_BUILD
            else               -> {
                val xml = file.takeIf { it.exists() }?.readText() ?: EMPTY_BUILD
                val name = file.name.replace(".xml", "")
                buildFile = file
                xml to name
            }

        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    private fun onStop() {
        if (loaded && buildFile == unsaved) {
            act += Single
                .fromCallable { saveBuildToFile(unsaved) }
                .subscribeBy(Timber::e)
        }
    }

    fun onSaveInstanceState(outState: Bundle) {
        val name = buildName()
        val xml = buildXml()
        val state = PersistBuild(name, xml)
        outState.putParcelable(STATE, state)
    }

    fun saveBuild() {
        act += act
            .readPermissions()
            .flatMap {
                val buildFile = buildFile
                when (buildFile) {
                    null    -> Maybe.empty()
                    unsaved -> act.rxInputTextDialog("Build name").map { File(buildsDir, "$it.xml") }
                    else    -> Maybe.just(buildFile)
                }
            }
            .io()
            .doOnSuccess(::saveBuildToFile)
            .map {
                unsaved.delete()
                buildFile = it
            }
            .main()
            .subscribeBy(
                onError = Timber::e,
                onSuccess = { act.toast("Build saved") }
            )
    }

    fun saveBuildAs() {
        act += act
            .readPermissions()
            .flatMap { act.rxInputTextDialog("Build name") }
            .io()
            .map { File(buildsDir, "$it.xml") }
            .doOnSuccess(::saveBuildToFile)
            .main()
            .subscribeBy(onError = {
                Timber.e(it)
                act.toast("Failed to save build")
            }, onSuccess = {
                act.toast("Build saved")
            })
    }

    private fun saveBuildToFile(file: File) {
        val xml = buildXml()
        file.parentFile.mkdirs()
        file.writeText(xml)
    }
}