package ru.hungrymole.pob.build.toolbar.progress

import ru.hungrymole.pob.components.Event

class ProgressEvent(
    val progress: Int
) : Event()