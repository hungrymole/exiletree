package ru.hungrymole.pob.build

import android.content.ClipData
import android.content.Intent
import android.net.Uri
import android.util.Base64
import io.reactivex.rxkotlin.subscribeBy
import okhttp3.OkHttpClient
import org.luaj.vm2.Globals
import ru.hungrymole.pob.R
import ru.hungrymole.pob.build.di.BuildScope
import ru.hungrymole.pob.common.utf8String
import ru.hungrymole.pob.components.EventBus
import ru.hungrymole.pob.dialogs.progressDialog
import ru.hungrymole.pob.dialogs.radioButtonsDialog
import ru.hungrymole.pob.dialogs.rxInputTextDialog
import ru.hungrymole.pob.extensions.clipboardService
import ru.hungrymole.pob.extensions.rx.io
import ru.hungrymole.pob.extensions.rx.luaSubscribe
import ru.hungrymole.pob.extensions.rx.main
import ru.hungrymole.pob.extensions.toast
import ru.hungrymole.pob.lua.set
import ru.hungrymole.pob.network.*
import ru.hungrymole.pob.ui.rx.plusAssign
import timber.log.Timber
import java.io.ByteArrayOutputStream
import java.util.zip.DataFormatException
import java.util.zip.Deflater
import java.util.zip.Inflater
import javax.inject.Inject
import javax.inject.Named


/**
 * Created by Dinar Khusainov on 27.02.2018.
 */

@BuildScope
class PastebinImportManager @Inject constructor(
    private val act: BuildActivity,
    private val connection: NetworkMonitor,
    private val lua: Globals,
    private val eventBus: EventBus,
    private val buildManager: BuildManager,
    @param:Named(NetworkModule.OKHTTP_DEFAULT) private val client: OkHttpClient
) {

    companion object {
        private const val SHARE_CLIPBOARD = "Copy to clipboard"
        private const val SHARE_ANDROID = "Share"
    }

    val host = act.getString(R.string.host_pastebin)
    private val exportUrl = "https://$host/api/api_post.php"
    private val importUrl = "https://$host/raw/%s"

    fun askForImport() {
        act += act
            .rxInputTextDialog("Pastebin url to import")
            .map(Uri::parse)
            .subscribeBy(
                onError = Timber::e,
                onSuccess = ::import
            )
    }

    fun import(uri: Uri) {
        val code = uri.pathSegments.firstOrNull()
        val url = when {
            !connection.isConnected()        -> {
                act.toast("Can't import: no internet ")
                return
            }
            code == null || code.length != 8 -> {
                act.toast("Invalid pastebin url")
                return
            }
            else                             -> importUrl.format(code)
        }

        Timber.i("Importing pastebin: $url")
        val progress = act.progressDialog("Importing from pastebin")
        progress.show()
        act += client
            .get(url)
            .luaSubscribe()
            .map { it.body()!!.string() }
            .doOnSuccess(::processCode)
            .main()
            .doOnDispose(progress::dismiss)
            .doFinally(progress::dismiss)
            .subscribeBy(::importError) {
                act.toast("Import successful")
                eventBus.onNext(PastebinImportEvent)
                lua["build"]["buildFlag"] = true
            }
    }

    fun export() {
        if (!connection.isConnected()) {
            act.toast("Can't export: no internet ")
            return
        }
        val params by lazy {
            val code = packXml(buildManager.buildXml())
            listOf(
                "api_dev_key" to act.getString(R.string.api_key_pastebin),
                "api_paste_private" to 1, //unlisted
                "api_option" to "paste",
                "api_paste_code" to code,
                "api_paste_expire_date" to "N"
            )
        }

        val types = listOf(SHARE_ANDROID, SHARE_CLIPBOARD)
        val progress = act.progressDialog("Exporting to pastebin")
        act += act
            .radioButtonsDialog("Export type", types)
            .doOnSuccess { progress.show() }
            .io()
            .flatMapSingleElement { type ->
                client
                    .postBody(exportUrl, *params.toTypedArray())
                    .map { it.body()!!.string() to type }
            }
            .main()
            .doOnDispose(progress::dismiss)
            .doFinally(progress::dismiss)
            .subscribeBy(::exportError) { (url, type) ->
                when  {
                    Uri.decode(url) == null -> exportError(IllegalArgumentException(url))
                    type == SHARE_ANDROID   -> Intent().apply {
                        setType("text/plain")
                        action = Intent.ACTION_SEND
                        putExtra(Intent.EXTRA_TEXT, url)
                        Intent
                            .createChooser(this, "Share build")
                            .let(act::startActivity)
                    }
                    type == SHARE_CLIPBOARD -> {
                        val clip = ClipData.newRawUri("POB Build", Uri.parse(url))
                        act.clipboardService().primaryClip = clip
                        act.toast("Build url copied to clipboard")
                    }
                }
            }

    }

    private fun exportError(e: Throwable) {
        Timber.e(e)
        when (e) {
            is HttpException -> "Export failed: http code ${e.code}"
            else             -> "Export failed: ${e.message}"
        }.let(act::toast)
    }

    private fun importError(e: Throwable) {
        Timber.e(e)
        when (e) {
            is HttpException            -> "Import failed: http code ${e.code}"
            is DataFormatException      -> "Import failed. It is not a valid POB code."
            is IllegalArgumentException -> "Import failed. It is not a valid POB code."
            else                        -> "Import failed: ${e.message}"
        }.let(act::toast)
    }

    private fun processCode(code: String) {
        val xml = unpackCode(code)
        buildManager.initBuild(xml, "Imported build")
    }

    private fun unpackCode(code: String): String {
        val sanitized = code
            .replace("-", "+")
            .replace("_", "/")
        val decoded = Base64.decode(sanitized, Base64.DEFAULT)
        val xml = inflate(decoded).utf8String()
        return xml
    }

    private fun packXml(xml: String): String {
        val deflated = deflate(xml.toByteArray())
        val encoded = Base64
            .encode(deflated, Base64.DEFAULT)
            .utf8String()
        val desanitized = encoded
            .replace("+", "-")
            .replace("/", "_")
        return desanitized
    }

    private val buffer = ByteArray(DEFAULT_BUFFER_SIZE)
    private fun inflate(data: ByteArray): ByteArray {
        val inflater = Inflater()
        inflater.setInput(data)
        val out = ByteArrayOutputStream(data.size)
        while (!inflater.finished()) {
            val count = inflater.inflate(buffer)
            out.write(buffer, 0, count)
        }
        out.close()
        inflater.end()
        val output = out.toByteArray()
        return output
    }


    private fun deflate(data: ByteArray): ByteArray {
        val deflater = Deflater()
        deflater.setInput(data)
        val out = ByteArrayOutputStream(data.size)
        deflater.finish()
        while (!deflater.finished()) {
            val count = deflater.deflate(buffer)
            out.write(buffer, 0, count)
        }
        val output = out.toByteArray()
        out.close()
        deflater.end()
        return output
    }
}