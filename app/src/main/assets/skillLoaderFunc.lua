--
-- Created by IntelliJ IDEA.
-- User: khusainov.d
-- Date: 22.09.2017
-- Time: 13:44
-- To change this template use File | Settings | File Templates.
--

local function mod(modName, modType, modVal, flags, keywordFlags, ...)
    return {
        name = modName,
        type = modType,
        value = modVal,
        flags = flags or 0,
        keywordFlags = keywordFlags or 0,
        ...
    }
end
local function flag(modName, ...)
    return {
        name = modName,
        type = "FLAG",
        value = true,
        flags = 0,
        keywordFlags = 0,
        ...
    }
end
local function skill(dataKey, dataValue, ...)
    return {
        name = "SkillData",
        type = "LIST",
        value = { key = dataKey, value = dataValue },
        flags = 0,
        keywordFlags = 0,
        ...
    }
end

return { mod = mod, flag = flag, skill = skill }