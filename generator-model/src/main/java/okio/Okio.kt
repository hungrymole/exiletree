package okio

/**
 * Created by Dinar Khusainov on 21.03.2018.
 */

val ByteString.byteArray: ByteArray get() = data

fun ByteArray.toByteString(): ByteString = ByteString.of(this, 0, size)