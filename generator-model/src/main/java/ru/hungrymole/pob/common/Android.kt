package ru.hungrymole.pob.common

/**
 * Created by Dinar Khusainov on 23.01.2018.
 */

fun androidColor(s: String): Int {
    val l = java.lang.Long.parseLong(s, 16)
    return (l or 0x00000000ff000000).toInt()
}