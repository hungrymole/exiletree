package ru.hungrymole.pob.common

inline fun <T> List<T>.forEachByIndex(action: (T) -> Unit) {
  for (i in 0 until size) action(this[i])
}
inline fun <K, V> MutableMap<K, V>.getOrCreate(key: K, create: (K) -> V): V =
    get(key) ?: create(key).also { put(key, it) }