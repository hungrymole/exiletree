package ru.hungrymole.pob.common

import java.nio.charset.Charset

/**
 * Created by Dinar Khusainov on 18.01.2018.
 */
fun String.substring(after: String, until: String): String? {
    val start = indexOf(after)
    val end = indexOf(until, start + after.length)
    return if (start != -1 && end != -1) substring(start + after.length, end) else null
}

val DECIMAL_REGEX = "\\d*\\.\\d+|\\d+\\.\\d*".toRegex()
fun String.isDecimal() = matches(DECIMAL_REGEX)

fun ByteArray.utf8String(): String = toString(Charset.defaultCharset())