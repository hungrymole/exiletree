package ru.hungrymole.pob.common

import java.text.DecimalFormat

/**
 * Created by Dinar Khusainov on 18.01.2018.
 */
private val format = DecimalFormat("#.#")
val Float.formatted get() = (Math.round(this * 10.0) / 10.0).let(format::format)
val Double.formatted get() = (Math.round(this * 10.0) / 10.0).let(format::format)