package ru.hungrymole.pob.common

fun <R> `try`(block: () -> R): R? = try {
  block()
} catch (e: Throwable) {
  null
}