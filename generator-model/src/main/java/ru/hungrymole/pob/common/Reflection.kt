package ru.hungrymole.pob.common

/**
 * Created by Dinar Khusainov on 18.01.2018.
 */

fun <R : Any?> Any.getField(name: String) = try {
    this::class.java.getDeclaredField(name).let {
        it.isAccessible = true
        it.get(this) as? R
    }
} catch (e: Exception) {
    null
}

inline fun <reified T> Any.castOrNull(): T? = if (this is T) this else null

inline fun <T> Any.castOrError(): T = this as T