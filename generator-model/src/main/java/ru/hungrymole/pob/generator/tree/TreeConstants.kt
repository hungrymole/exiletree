package ru.hungrymole.pob.generator.tree

/**
 * Created by Dinar on 30.11.2017.
 */
//hardcode stuff here
object TreeConstants {
    const val RESIZE = 0.3835F
    const val ORBIT_RADII_4 = (493 * RESIZE).toInt()
    const val JEWEL_UNALLOC_ICON_ID = 1
    const val JEWEL_ALLOC_ICON_ID = 2
    const val JEWEL_ALLOC_COBALT_ICON_ID = 3
    const val JEWEL_ALLOC_CRIMSON_ICON_ID = 4
    const val JEWEL_ALLOC_VIRIDIAN_ICON_ID = 5
    const val JEWEL_ALLOC_PRISMATIC_ICON_ID = 13
    const val JEWEL_ALLOC_ABYSS_ICON_ID = 14
    const val ASC_START_ICON_ID = 6
    const val CLASS_UNALLOCATED_ICON_ID = 7
    const val GROUP_ORBIT_1_ICON_ID = 8
    const val GROUP_ORBIT_2_ICON_ID = 9
    const val GROUP_ORBIT_3_ICON_ID = 10
    const val ASC_UNALLOC_BUTTON_ICON_ID = 11
    const val ASC_ALLOC_BUTTON_ICON_ID = 12
    val classNodeIdToClassId = listOf(
            54447 to 3,
            47175 to 1,
            50986 to 4,
            61525 to 5,
            44683 to 6,
            58833 to 0,
            50459 to 2
    ).sortedBy { it.first }
}