package ru.hungrymole.pob.generator

import com.squareup.wire.Message
import com.squareup.wire.ProtoAdapter
import okio.Okio
import org.luaj.vm2.Globals
import org.luaj.vm2.LuaString
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.generator.lua.Loadables
import ru.hungrymole.pob.generator.lua.serialization.SerializedTable
import ru.hungrymole.pob.generator.tree.ParsedTree
import ru.hungrymole.pob.lua.toArrayTable
import java.io.File
import java.io.InputStream

/**
 * Created by Dinar Khusainov on 26.03.2018.
 */
sealed class Generated<M : Message<*,*>>(
    private val adapter: ProtoAdapter<M>,
    private val name: String
) {
    companion object {
        const val OUTPUT = "generated"
    }

    fun write(to: File, what: M) = File(to, name).writeBytes(what.encode())

    fun read(from: (String) -> InputStream): M {
        return from("$OUTPUT/$name")
            .let(Okio::source)
            .let(Okio::buffer)
            .let(adapter::decode)
    }
}

open class GeneratedSerializedTable(name: String) : Generated<SerializedTable>(SerializedTable.ADAPTER, name)

object GeneratedJavaTree : Generated<ParsedTree>(ParsedTree.ADAPTER, "javaTree")
object GeneratedLoaders : Generated<Loadables>(Loadables.ADAPTER, "loaders")

object GeneratedLuaTree : GeneratedSerializedTable("luaTree")
object GeneratedState : GeneratedSerializedTable( "state")

fun Globals.loadPobEnviroment(generator: Boolean) {

    val skipData = listOf(
      "ModItem",
      "ModFlask",
      "ModJewel",
      "ModMaster",
      "Essence",
      "Bases",
      "Minions",
      "Spectres",
      "Rares",
      "glove",
      "minion",
//      "other",
      "spectre"
    )

    val skipDataTable = skipData
      .takeUnless { generator }
      .orEmpty()
      .map(LuaString::valueOf)
      .toArrayTable()

    loadfile("Android/Enviroment.lua").call(LuaValue.valueOf(generator), skipDataTable)
}