package ru.hungrymole.pob.generator

import ru.hungrymole.pob.generator.lua.Loadable
import ru.hungrymole.pob.generator.tree.Node

/**
 * Created by Dinar Khusainov on 04.01.2018.
 */
typealias ProtoNode = Node
typealias ProtoLoadable = Loadable
typealias POBColors = Map<String, Int>