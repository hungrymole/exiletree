package ru.hungrymole.pob.generator.db

import org.intellij.lang.annotations.Language

/**
 * Created by Dinar Khusainov on 17.01.2018.
 */
object DB {

    const val filename = "pob.db"

    object Uniques : Table {
        override val tableName = "uniques"
        const val id = "unique_id"
        const val baseId = "base_id"
        const val raw = "raw"
        const val tooltipInfo = "tooltip_info"
        const val tooltipModsOffset = "tooltip_mods_offset"

        @Language("SQL")
        override fun createStatement() = """
                CREATE TABLE $tableName (
                            $id INTEGER PRIMARY KEY,
                            $baseId INTEGER,
                            $raw TEXT NOT NULL,
                            $tooltipInfo BLOB NOT NULL,
                            $tooltipModsOffset INTEGER NOT NULL,
                            FOREIGN KEY ($baseId) REFERENCES ${Bases.tableName}(${Bases.id})
                            )
                            """
    }

    object UnuqueFTSSearch : DB.Table {
        override val tableName = "uniques_search"
        const val uniqueId = "rowid"
        const val uniqueName = "name"
        const val uniqueMods = "mods"

        @Language("SQL")
        override fun createStatement() = """
                CREATE VIRTUAL TABLE $tableName USING fts4 (
                            $uniqueName TEXT NOT NULL,
                            $uniqueMods TEXT NOT NULL,
                            FOREIGN KEY ($uniqueId) REFERENCES ${DB.Uniques.tableName}(${DB.Uniques.id})
                            )"""
    }

    object UniquesLeagues : Table {
        override val tableName = "uniques_leagues"
        const val uniqueId = "unique_id"
        const val leagueId = "league_id"

        @Language("SQL")
        override fun createStatement() = """
                CREATE TABLE $tableName (
                $uniqueId INTEGER NOT NULL,
                $leagueId INTEGER NOT NULL,
                FOREIGN KEY ($uniqueId) REFERENCES ${Uniques.tableName}(${Uniques.id}),
                FOREIGN KEY ($leagueId) REFERENCES ${Leagues.tableName}(${Leagues.id})
              )"""
    }

    object UniquesTypes : Table {
        override val tableName = "uniques_types"
        const val uniqueId = "unique_id"
        const val itemTypeId = "item_type_id"

        @Language("SQL")
        override fun createStatement() = """
                CREATE TABLE $tableName (
                $uniqueId INTEGER NOT NULL,
                $itemTypeId INTEGER NOT NULL,
                FOREIGN KEY ($uniqueId) REFERENCES ${Uniques.tableName}(${Uniques.id}),
                FOREIGN KEY ($itemTypeId) REFERENCES ${ItemTypes.tableName}(${ItemTypes.id})
              )"""
    }

    object Bases : Table {
        override val tableName = "item_bases"
        const val id = "id"
        const val name = "name"
        const val label = "label"
        const val baseCategory = "category"

        @Language("SQL")
        override fun createStatement() = """
                CREATE TABLE $tableName (
                $id INTEGER NOT NULL,
                $name TEXT NOT NULL,
                $label TEXT NOT NULL,
                $baseCategory INTEGER NOT NULL,
                FOREIGN KEY ($baseCategory) REFERENCES ${BaseCategories.tableName}(${BaseCategories.name})
              )"""
    }

    abstract class ReferenceTable : DB.Table {
        val id = "id"
        val name = "name"

        @Language("SQL")
        override fun createStatement() = """
        CREATE TABLE $tableName (
        $id INTEGER PRIMARY KEY,
        $name TEXT NOT NULL
        )"""
    }

    object ItemTypes : ReferenceTable() {
        override val tableName = "ref_item_types"
    }

    object Leagues : ReferenceTable() {
        override val tableName = "ref_leagues"
    }

    object BaseCategories : ReferenceTable() {
        override val tableName = "ref_base_types"
    }

    fun tables() = listOf(
        BaseCategories,
        Bases,
        Uniques,
        UnuqueFTSSearch,
        ItemTypes,
        Leagues,
        UniquesLeagues,
        UniquesTypes
    )

    interface Table {
        val tableName: String
        fun createStatement(): String
    }
}