package ru.hungrymole.pob.lua

import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue

/**
 * Created by Dinar Khusainov on 31.12.2017.
 */

fun LuaValue.entries(): Map<LuaValue, LuaValue> {
    val r = LinkedHashMap<LuaValue, LuaValue>()
    onEach { key, value -> r[key] = value }
    return r
}
fun LuaValue.arrayValues(): List<LuaValue> {
    return when {
        istable() -> checktable().array?.filterNotNull().orEmpty()
        else -> emptyList()
    }
}

fun List<LuaValue>.toArrayTable(): LuaTable =
  LuaTable()
    .apply { array = toTypedArray() }

fun LuaValue.values(): List<LuaValue> {
    val table = checktable()
    return table.keys().map(table::get)
}

fun LuaValue.clear() {
    onEach { k, _ -> set(k, LuaValue.NIL) }
}

inline fun LuaValue.onEach(block: (LuaValue, LuaValue) -> Unit) {
    val t = checktable()
    val keys = t.keys()
    keys.forEach { block(it, t.get(it)) }
}

fun LuaValue.ints(): List<Int> = arrayValues().map { it.checkint() }
fun LuaValue.strings(): List<String> = arrayValues().map { it.checkjstring() }

fun Iterable<LuaValue>.toArrayTable() = LuaTable().apply {
    forEachIndexed { i, value ->
        rawset(i + 1, value)
    }
}

fun <K : LuaValue, V : LuaValue> Map<K, V>.toLuaTable(): LuaTable {
    val result =  LuaTable()
    entries.forEach { result.set(it.key, it.value) }
    return result
}

fun LuaTable.toMap(): Map<LuaValue, LuaValue> {
    val result = mutableMapOf<LuaValue, LuaValue>()
    onEach(result::set)
    return result
}

inline fun LuaTable.isLuaArrayOf(predicate: (LuaValue) -> Boolean): Boolean {
    if (hashEntries != 0) return false
    return hashEntries == 0 && array.all { it == null || predicate(it) }
}

inline fun LuaTable.isEmpty(): Boolean = hashEntries == 0 && array.isEmpty()

operator fun LuaValue.component1() = checktable().get(1)
operator fun LuaValue.component2() = checktable().get(2)
operator fun LuaValue.component3() = checktable().get(3)