package ru.hungrymole.pob.lua

/**
 * Created by Dinar Khusainov on 24.12.2017.
 */
@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.CLASS)
annotation class Table