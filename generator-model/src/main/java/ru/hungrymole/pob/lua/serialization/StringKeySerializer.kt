package ru.hungrymole.pob.lua.serialization

import org.luaj.vm2.*
import ru.hungrymole.pob.generator.lua.serialization.SerializedLuaTableValue
import ru.hungrymole.pob.generator.lua.serialization.SerializedTable

object StringKeySerializer {

  fun serialize(k: String,
                v: LuaValue,
                result: SerializedTable.Builder,
                serializeOther: (LuaValue) -> SerializedLuaTableValue) {
    when {
      v.isinttype() -> result.string_int[k] = v.checkint()
      v.isnumber()  -> result.string_double[k] = v.checkdouble()
      v.isstring()  -> result.string_string[k] = v.checkjstring()
      v.isboolean() -> result.string_boolean[k] = v.checkboolean()
      else          -> result.string_other[k] = serializeOther(v)
    }
  }

  fun deserialize(table: SerializedTable,
                  result: LuaTable,
                  deserializeOther: (SerializedLuaTableValue, (LuaValue) -> Unit) -> LuaValue) {
    table.string_int.forEach { (k, v) -> setOptimized(result, k, LuaInteger.valueOf(v)) }
    table.string_double.forEach { (k, v) -> setOptimized(result, k, LuaNumber.valueOf(v)) }
    table.string_string.forEach { (k, v) -> setOptimized(result, k, LuaString.valueOf(v)) }
    table.string_boolean.forEach { (k, v) -> setOptimized(result, k, LuaBoolean.valueOf(v)) }
    table.string_other.forEach { (k, v) ->
      deserializeOther(v) { ref ->
        setOptimized(result, k, ref)
      }
    }
  }

  private fun setOptimized(table: LuaTable,
                           key: String,
                           value: LuaValue) {
    table.rawset(LuaString.valueOf(key), value)
  }
}