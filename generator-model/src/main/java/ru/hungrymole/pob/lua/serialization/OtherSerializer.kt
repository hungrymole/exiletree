package ru.hungrymole.pob.lua.serialization

import org.luaj.vm2.LuaTable
import org.luaj.vm2.LuaValue
import ru.hungrymole.pob.common.forEachByIndex
import ru.hungrymole.pob.generator.lua.serialization.LuaComplexTableEntry
import ru.hungrymole.pob.generator.lua.serialization.SerializedLuaTableValue
import ru.hungrymole.pob.generator.lua.serialization.SerializedTable

object OtherSerializer {

  fun serialize(k: LuaValue,
                v: LuaValue,
                result: SerializedTable.Builder,
                serializeOther: (LuaValue) -> SerializedLuaTableValue) {
    LuaComplexTableEntry(
      serializeOther(k),
      serializeOther(v)
    ).let(result.others::add)
  }

  fun deserialize(table: SerializedTable,
                  result: LuaTable,
                  deserializeOther: (SerializedLuaTableValue, (LuaValue) -> Unit) -> LuaValue) {
    table.others.forEachByIndex {
      deserializeOther(it.key) { key ->
        result.rawset(key, LuaValue.ZERO)
        deserializeOther(it.value) { value ->
          result.set(key, value)
        }
      }
    }
  }
}