package ru.hungrymole.pob.lua

import org.luaj.vm2.*

/**
 * Created by khusainov.d on 19.07.2017.
 */

var luaLogger: (String) -> Unit = { println("Lua - $it") }
fun logLua(m: Any?) = luaLogger("$m")

fun LuaTable.getBy(path: String, log: Boolean = true): LuaValue {
    if (log) logLua(path)
    val parse = fun(v: LuaValue, s: String) = when {
        s.contains("()") -> v.get(s.dropLast(2)).checkfunction().call()
        s.contains("[") -> {
            val index = s.drop(1).dropLast(1).toInt()
            v[index]
        }
        else -> v.get(s)
    }

    val steps = path.split(".")
    var current = parse(this, steps.first())
    steps.drop(1).forEach { step ->
        current = parse(current, step)
        if (current.isnil() && log) logLua("$step - nil")
    }
    return current
}

operator fun LuaValue.set(k: String, v: Boolean) = set(k, LuaBoolean.valueOf(v))
fun LuaValue.call(boolean: Boolean) = call(LuaString.valueOf(boolean))
fun LuaValue.call(one: Int) = call(LuaString.valueOf(one))
fun LuaValue.call(one: Int, two: LuaValue) = call(LuaInteger.valueOf(one), two)
fun LuaValue.call(one: LuaValue, two: Int, three: LuaValue) = call(one, LuaInteger.valueOf(two), three)
fun LuaValue.call(one: LuaValue, two: Int) = call(one, LuaInteger.valueOf(two))
fun LuaValue.call(one: LuaValue, two: String) = call(one, LuaInteger.valueOf(two))
fun LuaValue.call(one: String, two: String) = call(LuaString.valueOf(one), LuaString.valueOf(two))
fun LuaValue.call(one: String, two: Int) = call(LuaString.valueOf(one), LuaInteger.valueOf(two))
fun LuaValue.call(one: String, two: Boolean) = call(LuaString.valueOf(one), LuaBoolean.valueOf(two))
fun LuaValue.call(one: String, two: LuaValue) = call(LuaString.valueOf(one), two)
fun LuaValue.call(one: String, two: LuaValue, three: LuaValue) = call(LuaString.valueOf(one), two, three)
fun LuaValue.call(one: LuaValue, two: String, three: Boolean) = call(one, LuaString.valueOf(two), LuaString.valueOf(three))
fun LuaValue.call(one: String, two: String, three: String) = call(LuaString.valueOf(one), LuaString.valueOf(two), LuaString.valueOf(three))
fun LuaValue.call(one: String, two: String, three: LuaValue) = call(LuaString.valueOf(one), LuaString.valueOf(two), three)
fun LuaValue.string(): String = if (isnil()) "nil" else checkjstring()

fun LuaValue.selfCall(funcName: String, vararg args: Any, log: Boolean = true): LuaValue {
    val logArgs = args.joinToString()
    val start = System.currentTimeMillis()
    val func = get(funcName).checkfunction()
    return try {
        when (args.size) {
            0    -> func.call(this)
            1    -> func.call(this, args[0].luaValue())
            2    -> func.call(this, args[0].luaValue(), args[1].luaValue())
            else -> func.invoke(LuaValue.varargsOf(arrayOf(this) + args.map(Any::luaValue))).arg1()
        }
    } finally {
        if (log) {
            logLua("SelfCall - $funcName($logArgs) - ${System.currentTimeMillis()- start} ms")
        }
    }
}

fun Any?.luaValue() = when (this) {
    is LuaValue -> this
    is String -> LuaValue.valueOf(this)
    is Int -> LuaValue.valueOf(this)
    is Boolean -> LuaValue.valueOf(this)
    null -> LuaValue.NIL
    else -> null!!
}
