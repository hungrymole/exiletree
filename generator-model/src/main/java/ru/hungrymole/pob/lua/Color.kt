package ru.hungrymole.pob.lua

/**
 * Created by Dinar Khusainov on 29.12.2017.
 */
@Retention(AnnotationRetention.SOURCE)
@Target(AnnotationTarget.VALUE_PARAMETER)
annotation class Color