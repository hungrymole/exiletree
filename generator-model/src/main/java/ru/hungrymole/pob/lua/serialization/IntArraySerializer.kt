package ru.hungrymole.pob.lua.serialization

import org.luaj.vm2.*
import ru.hungrymole.pob.generator.lua.serialization.SerializedLuaTableValue
import ru.hungrymole.pob.generator.lua.serialization.SerializedTable
import ru.hungrymole.pob.lua.isLuaArrayOf

object IntArraySerializer {

  fun serialize(table: LuaTable,
                result: SerializedTable.Builder,
                serializeOther: (LuaValue) -> SerializedLuaTableValue): Boolean = try {

    var isArray = false
    val array by lazy {
      isArray = true
      table.array.mapNotNull { it?.takeUnless(LuaValue::isnil) }
    }
    when {
      table.isLuaArrayOf(LuaValue::isinttype) -> array.map(LuaValue::checkint).let(result::int_arr_int)
      table.isLuaArrayOf(LuaValue::isnumber)  -> array.map(LuaValue::checkdouble).let(result::int_arr_double)
      table.isLuaArrayOf(LuaValue::isstring)  -> array.map(LuaValue::checkjstring).let(result::int_arr_string)
      table.isLuaArrayOf(LuaValue::isboolean) -> array.map(LuaValue::checkboolean).let(result::int_arr_boolean)
      table.hashEntries == 0                  -> array.map(serializeOther).let(result::int_arr_other)
    }

    isArray
  } catch (e: Throwable) {
    throw e
  }

  fun deserialize(table: SerializedTable,
                  result: LuaTable,
                  deserializeOther: (SerializedLuaTableValue, (LuaValue) -> Unit) -> LuaValue): Boolean {
    result.array = when {
      table.int_arr_int.isNotEmpty()     -> table.int_arr_int.map(LuaInteger::valueOf)
      table.int_arr_double.isNotEmpty()  -> table.int_arr_double.map(LuaNumber::valueOf)
      table.int_arr_string.isNotEmpty()  -> table.int_arr_string.map(LuaString::valueOf)
      table.int_arr_boolean.isNotEmpty() -> table.int_arr_boolean.map(LuaBoolean::valueOf)
      table.int_arr_other.isNotEmpty()   -> {
        result.array = arrayOfNulls(table.int_arr_other.size)
        table.int_arr_other.mapIndexed { i, v ->
          deserializeOther(v) { ref ->
            result.array[i] = ref
          }
        }
      }
      else                               -> return false
    }.toTypedArray()

    return true
  }
}