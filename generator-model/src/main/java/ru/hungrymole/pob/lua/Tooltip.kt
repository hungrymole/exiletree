package ru.hungrymole.pob.lua

import org.luaj.vm2.LuaValue
import org.luaj.vm2.LuajOptimizations
import ru.hungrymole.pob.common.DECIMAL_REGEX
import ru.hungrymole.pob.common.androidColor
import ru.hungrymole.pob.common.formatted
import ru.hungrymole.pob.common.getOrCreate
import ru.hungrymole.pob.generator.POBColors
import ru.hungrymole.pob.generator.lua.SpanInfo
import ru.hungrymole.pob.generator.lua.TooltipInfo
import java.util.regex.Pattern

/**
 * Created by khusainov.d on 25.09.2017.
 */
class Tooltip(
    val lines: List<TooltipLine>,
    val color: Int
) {
    companion object {
        val SEPARATOR = "\r\n"
        val COLOR_REGEX = "\\^x[ABCDEF0-9]{6}".toPattern()
        private val COLOR_REGEX_SIMPLE = "\\^[0-9]".toPattern()

        private val LINES = LuajOptimizations.toLuaString("lines")
        private val TEXT = LuajOptimizations.toLuaString("text")
        private val COLOR = LuajOptimizations.toLuaString("color")
        private val SIZE = LuajOptimizations.toLuaString("size")
        const val DEFAULT_SIZE = 16

        lateinit var colors: POBColors//todo...

        private val tooltipCache = mutableMapOf<String, Tooltip>()
        fun buildSingleLine(coloredLine: String): Tooltip = tooltipCache.getOrCreate(coloredLine, ::Tooltip)
    }

    var text = ""
    val separators = mutableListOf<Int>()
    val textColors = mutableListOf<SpanInfo>()
    val textSizes = mutableListOf<SpanInfo>()
    val highlights = mutableListOf<SpanInfo>()

  constructor(coloredLine: String) : this(listOf(TooltipLine(coloredLine, DEFAULT_SIZE)), 0)

  constructor(tooltip: LuaValue) : this(
    lines = tooltip[LINES]
      .arrayValues()
      .map {
        TooltipLine(
          it[TEXT].optjstring(""),
          it.rawget(SIZE).optint(DEFAULT_SIZE))
      },
    color = tooltip
              .rawget(COLOR)
              .optjstring(null)
              ?.let(colors::get)
            ?: 0
  )

    constructor(from: TooltipInfo) : this(emptyList(), from.borderColor) {
        text = from.text
        textColors += from.textColors
        textSizes += from.textSizes
        separators += from.separators
    }

    fun serialize(): TooltipInfo = TooltipInfo(
        text,
        color,
        textColors,
        textSizes,
        separators
    )

    init {
        val content = StringBuilder()
        lines.dropLastWhile { it.text.isEmpty() }.forEach {
            //shrink decimals to 1 point after dot
            val line = it.text.replace(DECIMAL_REGEX) { it.value.toFloat().formatted }
            val lineStart = content.length

            if (line.isEmpty() && content.takeLast(2) != SEPARATOR) {
                content.append(SEPARATOR)
                separators += content.lastIndexOf(SEPARATOR)
            }
            if (line.isEmpty()) return@forEach

            val colorStarts = COLOR_REGEX
              .findColors(line)
              .plus(COLOR_REGEX_SIMPLE.findColors(line))
              .sortedBy { it.first }

            if (colorStarts.isEmpty()) {
                content.append(line)
                textColors
                  .lastOrNull()
                  ?.value
                  ?.let { color -> SpanInfo(color, content.lastIndexOf(line), content.length) }
                  ?.let(textColors::add)
            }

            colorStarts.forEachIndexed { index, pair ->
                val next = colorStarts.getOrNull(index + 1)
                val (pos, str) = pair
                val prevLength = content.length
                val segmentEnd = if (next != null) next.first else line.length
                content.append(line, pos + str.length, segmentEnd)
                val color = colors[str] ?: parseDefaultCoor(str)
                textColors += SpanInfo(color, prevLength, content.length)
            }

            textSizes += SpanInfo(it.size, lineStart, content.length)
            content.append("\n")
        }
        text = content.trim().toString()
    }

    private fun parseDefaultCoor(luaColor: String): Int {
        return try {
            luaColor.drop(2).let(::androidColor)
        } catch (e: Throwable) {
            error("Unrecognized color $luaColor")
        }
    }

    private fun Pattern.findColors(line: String): List<Pair<Int, String>> {
        val matcher = matcher(line)
        val r = mutableListOf<Pair<Int, String>>()
        var lastPos = -1
        while (matcher.find()) {
            val colorStr = matcher.group(0)
            lastPos = line.indexOf(colorStr, ++lastPos)
            r += lastPos to colorStr
        }
        return r
    }

    class TooltipLine(val text: String,
                              val size: Int) {
        override fun toString(): String = text
    }
}