package ru.hungrymole.pob.lua.serialization

import okio.byteArray
import okio.toByteString
import org.luaj.vm2.*
import org.luaj.vm2.compiler.DumpState
import org.luaj.vm2.lib.LibFunction
import ru.hungrymole.pob.common.utf8String
import ru.hungrymole.pob.generator.lua.serialization.SeializedLuaClousure
import ru.hungrymole.pob.generator.lua.serialization.SerializedLuaTableValue
import ru.hungrymole.pob.generator.lua.serialization.SerializedTable
import ru.hungrymole.pob.lua.getBy
import ru.hungrymole.pob.lua.isEmpty
import ru.hungrymole.pob.lua.onEach
import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer

/**
 * Created by Dinar Khusainov on 20.03.2018.
 */
class LuaTableSerializer private constructor(
  private val globals: Globals? = null
) {

  companion object {
    private const val TABLE = 0
    private const val STRING = 1
    private const val INT = 2
    private const val BOOLEAN = 3
    private const val DOUBLE = 4
    private const val CLOUSURE = 5
    private const val REFERENCE = 6
    private const val GLOBALS = 7
    private const val LIB_FUNCTION = 8
    private const val NIL = 9
    @JvmStatic private val TRUE: Byte = 1
    @JvmStatic private val FALSE: Byte = 0
    @JvmStatic private val EMPTY = byteArrayOf()
    @JvmStatic private val libFuncPath = mapOf(
      "insert" to "table.insert",
      "remove" to "table.remove",
      "sort" to "table.sort",

      "floor" to "math.floor",
      "min" to "math.min",
      "max" to "math.max",
      "sqrt" to "math.sqrt",
      "modf" to "math.modf",
      "ceil" to "math.ceil",
      "abs" to "math.abs",

      "band" to "bit.band",
      "bor" to "bit.bor",
      "bnot" to "bit.bnot",

      "format" to "string.format",
      "gmatch" to "string.gmatch"
    )

    fun serializeGlobals(table: Globals): SerializedTable {
      table["table"] = LuaValue.NIL
      table["bit"] = LuaValue.NIL
      table["bit32"] = LuaValue.NIL
      table["math"] = LuaValue.NIL
      table["os"] = LuaValue.NIL
      table["io"] = LuaValue.NIL
      table["coroutine"] = LuaValue.NIL
      table["package"] = LuaValue.NIL
      table["string"] = LuaValue.NIL

      return serializeTable(table)
    }

    fun serializeTable(table: LuaTable): SerializedTable = try {
      LuaTableSerializer()
        .serializeTable(table)
    } catch (e: Throwable) {
      throw e
    }

    fun deserializeGlobals(t: SerializedTable, globals: Globals) {
      deserializeTable(t, globals)
        .onEach(globals::set)
    }

    fun deserializeTable(t: SerializedTable, globals: Globals): LuaTable =
      LuaTableSerializer(globals)
        .deserializeTable(t)
  }

  private var idCounter = 0
  private val references = mutableMapOf<Int, LuaValue>()
  private val serializeLuaValue = ::serializeValue
  private val deserializeLuaValue = ::deserializeValue

  private fun serializeTable(table: LuaTable): SerializedTable {
    val result = SerializedTable.Builder()

    if (table.isEmpty()) return result.build()

    if (IntArraySerializer.serialize(table, result, serializeLuaValue)) return result.build()

    table.onEach { k, v ->
      println("$k - $v")
      when {
        k.isinttype()                        -> IntKeySerializer.serialize(k.checkint(), v, result, serializeLuaValue)
        k.isnumber()                         -> error("double key $k")
        k.isboolean()                        -> error("bool key")
        k is Globals                         -> error("globals key")
//        v is LuaFunction && v !is LuaClosure -> error("Lib function referenced directly")
        v is LuaFunction && v !is LuaClosure -> return@onEach//lib functions (math/bit/etc)
        k is LuaFunction                     -> error("function key")
        k.isstring()                         -> StringKeySerializer.serialize(k.checkjstring(), v, result, serializeLuaValue)
        else                                 -> OtherSerializer.serialize(k, v, result, serializeLuaValue)
      }
    }

    table
      .getmetatable()
      ?.checktable()
      .also { println("META: $it") }
      ?.let(::serializeTable)
      ?.let(result::metatable)

    return result.build()
  }

  private fun deserializeTable(t: SerializedTable): LuaTable =
    LuaTable().also { deserializeIntoTable(it, t) }

  private fun deserializeIntoTable(out: LuaTable, t: SerializedTable) {
    if (!IntArraySerializer.deserialize(t, out, deserializeLuaValue)) {
      IntKeySerializer.deserialize(t, out, deserializeLuaValue)
      StringKeySerializer.deserialize(t, out, deserializeLuaValue)
      OtherSerializer.deserialize(t, out, deserializeLuaValue)
    }

    t.metatable?.also { meta ->
      val luaMeta = LuaTable()
      deserializeIntoTable(luaMeta, meta)
      out.setmetatable(luaMeta)
    }
  }

  private fun deserializeValue(v: SerializedLuaTableValue, aheadRef: (LuaValue) -> Unit): LuaValue {
    globals ?: return LuaValue.NIL
    val value = when (v.type) {
      NIL          -> LuaValue.NIL
      STRING       -> LuaString.valueOf(v.value.byteArray.utf8String())
      INT          -> LuaValue.valueOf(v.value.byteArray.toInt())
      DOUBLE       -> LuaValue.valueOf(v.value.byteArray.toDouble())
      BOOLEAN      -> {
        val bool = v.value.byteArray.first()
        if (bool == TRUE) LuaValue.TRUE else LuaValue.FALSE
      }
      GLOBALS      -> globals
      REFERENCE    -> {
        val refId = v.id
        val ref: LuaValue? = deserializeCache[refId]
        if (ref == null) {
          pendingRefs
            .getOrPut(refId, ::mutableListOf)
            .add(aheadRef)
        }
        ref
      }
      CLOUSURE     -> {
        val sc = v.value
          .byteArray
          .let(SeializedLuaClousure.ADAPTER::decode)
        val p = globals.undumper.undump(sc.prototype.byteArray.inputStream(), "")
        val clousure = LuaClosure(p, globals)
        clousure.upValues = arrayOfNulls<UpValue>(sc.upvalues.size)

        onInitialized(v.id, clousure)
        sc.upvalues.forEachIndexed { i, upv ->
          deserializeValue(upv) { clousure.upValues[i] = UpValue(arrayOf(it), 0) }

        }
        clousure
      }
      TABLE        -> {
        val table = v.value
          .byteArray
          .let(SerializedTable.ADAPTER::decode)
        val luaTable = LuaTable()
        onInitialized(v.id, luaTable)
        deserializeIntoTable(luaTable, table)
        luaTable
      }
      LIB_FUNCTION -> {
        val name = v.value.byteArray.utf8String()
        val path = libFuncPath[name] ?: name
        lfc
          .getOrPut(path) { globals.getBy(path, log = true) }
          .also { if (it == LuaValue.NIL) error("Lib funciton $name not found") }
      }
      else         -> error("INVALIED TYPE")
    }

    return value?.also(aheadRef) ?: LuaValue.NIL
  }

  private val lfc = mutableMapOf<String, LuaValue>()

  private val pendingRefs = mutableMapOf<Int, MutableList<(LuaValue) -> Unit>>()
  private val deserializeCache = mutableMapOf<Int, LuaValue>()
  private fun onInitialized(id: Int, value: LuaValue) {
    deserializeCache[id] = value
    pendingRefs
      .remove(id)
      ?.forEach { it(value) }
  }

  private fun serializeValue(v: LuaValue): SerializedLuaTableValue {
    val output = SerializedLuaTableValue.Builder()

    fun addReference(vl: LuaValue) {
      val id = idCounter++
      output.id = id
      references[id] = vl
    }

    if (references.containsValue(v)) {
      val refId = references
        .entries
        .first { it.value == v }
        .key
      return SerializedLuaTableValue(REFERENCE, null, refId)
    }

    val (type, value) = when (v) {
      is LuaNil      -> NIL to EMPTY
      is Globals     -> GLOBALS to EMPTY
      is LuaString   -> STRING to v.checkjstring().toByteArray()
      is LuaInteger  -> INT to v.v.toByteArray()
      is LuaDouble   -> DOUBLE to v.todouble().toByteArray()
      is LuaBoolean  -> BOOLEAN to if (v.v) byteArrayOf(TRUE) else byteArrayOf(FALSE)
      is LuaTable    -> {
        addReference(v)
        TABLE to serializeTable(v).encode()
      }
      is LuaClosure  -> {
        addReference(v)
        val upvalues = v
          .upValues
          .map {
            serializeValue(it.value)
          }

        val prototype = ByteArrayOutputStream()
          .use { DumpState.dump(v.p, it, false);it }
          .toByteArray()
          .toByteString()
        CLOUSURE to SeializedLuaClousure(prototype, upvalues).encode()
      }
      is LibFunction -> {
        var name = v.name()
        //todo hardcode for bitlib....
        if (name == "Bit32LibV") {
          name = v::class.java
            .superclass
            .superclass
            .getDeclaredField("name").let {
              it.isAccessible = true
              it.get(v) as String
            }
        }
        LIB_FUNCTION to name.toByteArray()
      }
      else           -> error("Invalid type $v ${v::class.java}")
    }
    output.type = type
    output.value = value.toByteString()
    return output.build()
  }

  private val intBuf = ByteBuffer.allocate(4)
  private fun Int.toByteArray(): ByteArray {
    intBuf.clear()
    return intBuf.putInt(this).array()
  }

  private fun ByteArray.toInt(): Int {
    intBuf.clear()
    intBuf.put(this).position(0)
    return intBuf.int
  }

  private val doubleBuf = ByteBuffer.allocate(8)
  private fun Double.toByteArray(): ByteArray {
    doubleBuf.clear()
    return doubleBuf.putDouble(this).array()
  }

  private fun ByteArray.toDouble(): Double {
    doubleBuf.clear()
    doubleBuf.put(this).position(0)
    return doubleBuf.double
  }
}