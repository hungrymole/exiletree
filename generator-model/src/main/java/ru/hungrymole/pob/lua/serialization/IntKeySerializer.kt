package ru.hungrymole.pob.lua.serialization

import org.luaj.vm2.*
import ru.hungrymole.pob.generator.lua.serialization.SerializedLuaTableValue
import ru.hungrymole.pob.generator.lua.serialization.SerializedTable

object IntKeySerializer {

  fun serialize(k: Int,
                v: LuaValue,
                result: SerializedTable.Builder,
                serializeOther: (LuaValue) -> SerializedLuaTableValue) {
    when {
      v.isinttype() -> result.int_int[k] = v.checkint()
      v.isnumber()  -> result.int_double[k] = v.checkdouble()
      v.isstring()  -> result.int_string[k] = v.checkjstring()
      v.isboolean() -> result.int_boolean[k] = v.checkboolean()
      else          -> result.int_other[k] = serializeOther(v)
    }
  }

  inline fun deserialize(table: SerializedTable,
                         result: LuaTable,
                         deserializeOther: (SerializedLuaTableValue, (LuaValue) -> Unit) -> LuaValue) {
    table.int_int.forEach { (k, v) -> setOptimized(result, k, LuaInteger.valueOf(v)) }
    table.int_double.forEach { (k, v) -> setOptimized(result, k, LuaNumber.valueOf(v)) }
    table.int_string.forEach { (k, v) -> setOptimized(result, k, LuaString.valueOf(v)) }
    table.int_boolean.forEach { (k, v) -> setOptimized(result, k, LuaBoolean.valueOf(v)) }
    table.int_other.forEach { (k, v) ->
      deserializeOther(v) { ref ->
        setOptimized(result, k, ref)
      }
    }
  }

  inline fun setOptimized(table: LuaTable,
                           key: Int,
                           value: LuaValue) {
    if (!table.arrayset(key, value)) {
      table.rawset(LuaInteger.valueOf(key), value)
    }
  }
}