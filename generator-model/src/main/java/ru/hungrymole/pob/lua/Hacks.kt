package ru.hungrymole.pob.lua

import org.luaj.vm2.Globals
import ru.hungrymole.pob.common.androidColor
import ru.hungrymole.pob.generator.POBColors

/**
 * Created by Dinar Khusainov on 29.03.2018.
 */

fun Globals.pobColors(): POBColors {
    return get("colorCodes")
        .entries()
        .map { it.value.checkjstring() }
        .associate { it to it.drop(2).let(::androidColor) }
        .toMutableMap()
        .also { colors ->
//            colors["^xFFFF30"] = androidColor("5F0096") //yellow doesn't work on white background
            colors["^7"] = androidColor("FFFFFF")
            colors["^8"] = androidColor("B3B3B3")
            colors["^9"] = androidColor("676767")
            colors["^1"] = androidColor("FF0202")
            Tooltip.colors = colors
        }
}